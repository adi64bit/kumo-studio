<?php $footer = khayr_option('footer', 'options');?>
<footer class="footer d-flex flex-column justify-content-center add-graphic-4">
    <div class="footer__wrapper">
        <div class="footer__top">

            <div class="footer__top-logo--wrapper">
                <a href="<?php echo home_url();?>" class="text-hide cursor-link d-block footer__top-logo">
                    <img data-src="<?php echo $footer[0]['logo']; ?>" alt="Kumo Studio Footer Logo" class="footer__top-logo--img lazyload">
                </a>
            </div>

            <div class="footer__top-text">
                <p>KUMO Studio</p>
            </div>

            <div class="footer__top-address">
                <p><?php echo $footer[0]['address']; ?></p>
            </div>

            <div class="footer__top-social-media">
                <?php 
                    $socials = khayr_option('social_media', 'options');
                    if (is_array($socials) && isset($socials[0])) {
                        echo '<ul>';
                        foreach ($socials[0] as $key => $value) {
                            echo '<li><a href="'.$value.'" target="_blank" class="cursor-link"><i class="icon icon-'.$key.'"></i></a></li>';
                        }
                        echo '</ul>';
                    }
                ?>
            </div>

            <div class="footer__top-menu">
                <?php 
                    wp_nav_menu( array(
                        'menu_class'        => 'footer__top-menu-navigation',
                        'menu_id'           => 'footer-nav',
                        'theme_location'    => 'footer_menu',
                    ) );
                ?>
            </div>

        </div>
        <div class="footer__bottom">
            <div class="footer__bottom-copyright">
                <p><?php echo do_shortcode($footer[0]['copyright']); ?></p>
            </div>
        </div>
    </div>
</footer>

<?php 
    $graphic = khayr_option('graphic_elements', 'options');
    if($graphic){
        echo '<div class="graphic__elements" style="display:none;">';
        foreach ($graphic as $key => $value) {
            ?>
            <div class="graphic__element graphic__element-<?php echo $key;?>" data-key="<?php echo $key;?>" data-img="<?php echo $value['element'];?>" data-pos-x="<?php echo $value['pos_x'];?>" data-pos-y="<?php echo $value['pos_y'];?>">
                <?php 
                    $temp = wp_get_attachment_metadata($value['element_id']);
                    if ($temp && isset($temp['file']) && strpos($temp['file'], '.svg') !== false) {
                        $uploadDir = wp_get_upload_dir();
                        echo file_get_contents($uploadDir['basedir'].''.$temp['file']);
                    }
                ?>
            </div>
            <?php
        }
        echo '</div>';
    }
?>