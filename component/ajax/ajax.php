<?php
    if (!isset($args)) {
        $args = array();
    }

    $ajax = shortcode_atts(array(
        'type'      => 'blog',
        'class'     => '',
        'data'      => false,
        'posts'     => false,
        'loadmore'  => false,
        'pagination'=> false
    ), $args)
?>
<section id="section_<?php echo $ajax['type'];?>" class="section section-ajax section-ajax--<?php echo $ajax['type'];?> <?php echo $class;?>">
    <div class="container khayr-ajax">
        
        <?php 
            if ($ajax['data']) {
                if ($ajax['data']['params']['featured']) {
                    $post = get_post($ajax['data']['params']['featured']);
                    ?>
                    <div class="khayr-ajax__featured">
                        <?php 
                            echo get_template_part( 'template-parts/blog-listing', null, $post );
                        ?>
                    </div>
                    <?php
                }

                if ($ajax['data']['params']) {
                    ?>
                    <form class="khayr-ajax__data">
                        <?php 
                            foreach ($ajax['data']['params'] as $key => $param) {
                                echo '<input type="hidden" name="'.$key.'" value="'.$param.'">';
                            }
                        ?>
                    </form>
                    <?php
                }

                ?>
                <div class="khayr-ajax__filter-trigger-mobile">
                    <a href="#" class="khayr-ajax__filter-trigger"><span class="icon icon-filter"></span></a>
                </div>
                <div class="khayr-ajax__filter">
                    <?php 
                        $category = $ajax['data']['filter']['categories'];
                        if ($category) {
                            ?>
                            <div class="khayr-ajax__filter-group">
                                <select name="category" data-ajax="category" data-ajax-type="select" class="unform form-control choices-style">
                                    <option value=""><?php echo $category['label'];?></option>
                                    <?php 
                                        if ($category['items']) {
                                            foreach ($category['items'] as $key => $value) {
                                                echo '<option value="'.$value['value'].'" '.( $ajax['data']['params']['category'] == $value['value'] ? 'selected' : '' ).'>'.$value['label'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }

                        

                        $year = $ajax['data']['filter']['year'];
                        if ($year) {
                            ?>
                            <div class="khayr-ajax__filter-group">
                                <select name="year" data-ajax="year" data-ajax-type="select" class="unform form-control choices-style">
                                    <option value=""><?php echo $year['label'];?></option>
                                    <?php 
                                        if ($year['items']) {
                                            foreach ($year['items'] as $key => $value) {
                                                echo '<option value="'.$value['value'].'" '.( $ajax['data']['params']['category'] == $value['value'] ? 'selected' : '' ).'>'.$value['label'].'</option>';
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <?php
            }
        ?>

        <div class="khayr-ajax__lists row">
            <?php 
                if ($ajax['posts']) {
                    foreach ($ajax['posts'] as $key => $post) {
                        ?>
                        <div class="khayr-ajax__list col">
                            <?php echo get_template_part( 'template-parts/blog-listing', null, $post );?>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="khayr-ajax__list col no-article">It seems we can't find what you're looking for.</div>
                    <?php
                }
            ?>
        </div>

        <div class="khayr-ajax__placeholder">
            <?php 
            for ($i=0; $i < 3; $i++) { 
                ?>
                <div class="khayr-ajax__placeholder--item">
                    <div class="khayr-ajax__placeholder--item-1"></div>
                    <div class="khayr-ajax__placeholder--item-2"></div>
                    <div class="khayr-ajax__placeholder--item-3"></div>
                </div>
                <?php
            }
            ?>
            </div>

        <div class="khayr-ajax__load-more <?php echo $ajax['loadmore'] ? '' : 'hide';?>">
            <?php 
                if ($ajax['loadmore']) {
                    echo $ajax['loadmore'];
                }
            ?>
        </div>

        <div class="khayr-ajax__pagination <?php echo $ajax['pagination'] ? '' : 'hide';?>">
            <?php 
                if ($ajax['pagination']) {
                    echo $ajax['pagination'];
                }
            ?>
        </div>
    </div>
</section>
