<?php

class KhayrAjax
{
    protected $post_type;
    protected $params;

    public function __construct( $params = null )
    {
        $this->params = $params;

        add_action('wp_ajax_khayr_get_posts', [$this, 'get_posts']);
        add_action('wp_ajax_nopriv_khayr_get_posts', [$this, 'get_posts']);
    }

    public function get_posts()
    {   
        $response = array();
        $is_ajax = self::is_ajax();
        $postData = $_POST;

        if( ! $is_ajax ) {
                $postData = $this->params;
        }

        $this->post_type = sanitize_text_field( $postData['post_type'] );

        $args = $this->get_args($postData);
        
        $response['postData'] = $postData;
        
        $query = new WP_Query($args);
        $response['args'] = $args;
        $response['posts'] = $query->have_posts() ? $query->get_posts() : false;

        $paged = isset($postData['paging']) ? absint( $postData['paging'] ) : 1;

        if (isset($postData['pagination']) && $postData['pagination'] == 'true') {
            $response['pagination'] = $this->get_pagination($query, $paged);
        }

        if (isset($postData['loadmore']) && $postData['loadmore'] == 'true') {
            $response['loadmore'] = $this->get_loadmore($query, $paged);
        }

        if (isset($postData['infinite_scroll']) && $postData['infinite_scroll'] == 'true') {
            $response['infinite_scroll'] = $this->get_infinite_scroll($query, $paged);
        }

        wp_reset_postdata();

        if( $is_ajax ) {
            if( is_array($response['posts']) ) {
                $response['posts'] = $this->get_template($response['posts']);
            } else {
                $response['posts'] = false;
            }
            unset($response['args']);
            unset($response['postData']);
            echo wp_send_json( $response );
            wp_die();
        } else {
            return $response;
        }
    }

    public function get_args($postData)
    {
        $is_ajax = self::is_ajax();
        $paged = isset($postData['paging']) ? absint( $postData['paging'] ) : 1;
        $posts_per_page = isset($postData['posts_per_page']) ? intval( $postData['posts_per_page'] ) : '';
        $category = isset($postData['category']) ? sanitize_text_field( $postData['category'] ) : '';
        $post_tag = isset($postData['post_tag']) ? sanitize_text_field( $postData['post_tag'] ) : '';
        $year = isset($postData['year']) ? intval( $postData['year'] ) : '';
        $term = isset($postData['tag']) ? sanitize_text_field( $postData['tag'] ) : '';
        $sort_by = isset($postData['sort_by']) ? sanitize_text_field( $postData['sort_by'] ) : '';
        $keyword = isset($postData['keyword']) ? sanitize_text_field( $postData['keyword'] ) : '';
        $featured = isset($postData['featured']) ? intval( $postData['featured'] ) : '';
        $post_type = $this->post_type;
        $taxonomy = $this->get_taxonomy($this->post_type);

        $args = [
                'post_type'         => $post_type,
                'post_status'       => 'publish',
                'posts_per_page'    => $posts_per_page,
                'paged'             => $paged
        ];

        if ($featured !== '') {
            //$args['post__not_in'] = array($featured);
        }

        $final_args = $args;

        $final_args = array_merge( $final_args,
            $this->get_sort_by_args($sort_by)
        );

        if ($keyword !== '') {
                $final_args = array_merge( $final_args,
                $this->get_search_keyword_args($keyword)
                );
        }

        if ($category !== '') {
            $final_args = array_merge( $final_args,
                $this->get_category_args($category, 'category')
            );
        }

        if ($post_tag !== '') {
            $final_args['tag'] = $post_tag;
        }

        if ($year !== '') {
            $final_args['date_query'] = array(
                array('year' => $year)
            );
        }

        // if ($taxonomy !== '') {
        //         $final_args = array_merge( $final_args,
        //         $this->get_category_args($term, $taxonomy)
        //         );
        // }

        return $final_args;
    }

    public function get_date_args( $month, $year ) // not used
    {
        $args = array();

        if( $month != '' ) {
            $args['monthnum'] = $month;
        }

        if( $year != '' ) {
            $args['year'] = $year;
        }

        return $args;
    }

    public function get_taxonomy()
    {
        $tax = 'category';

        switch($this->post_type) {
            case 'post':
            $tax = 'category';
        }

        return $tax;
    }

    public function get_category_args($term, $taxonomy)
    {
        $category_args = array();

        if( $term != '' && $this->post_type == 'post' ) {
            $category_args['category_name'] = $term;
        } elseif ($term != '' && $taxonomy == 'category') {
            $category_args['category_name'] = $term;
        }

        if( $term != '' && $term != 'all' && $this->post_type != 'post' && $taxonomy != 'category') {
            $category_args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms'    => $term 
                )
            );
        }

        return $category_args;
    }

    public function get_sort_by_args($sort_by)
    {
        $order_args = array();

        switch($sort_by) {
            case 'a-z':
                $order_args['orderby'] = 'title';
                $order_args['order'] = 'ASC';
            break;
            case 'z-a':
                $order_args['orderby'] = 'title';
                $order_args['order'] = 'DESC';
            break;
            case 'date-latest':
                $order_args['order'] = 'DESC';
            break;
            case 'date-oldest':
                $order_args['order'] = 'ASC';
            break;
        }

        return $order_args;
    }

    public function get_search_keyword_args($keyword)
    {
        $search_args = array();

        if( $keyword != '' ) {
                $search_args['s'] = $keyword;
                $search_args['relevanssi'] = true;
        }

        return $search_args;
    }

    public function get_published_year()
    {
        $data = array();

        $args = array(
            'post_type'         => $this->params['post_type'],
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'order'             => 'DESC'
        );

        $query = new WP_Query($args);

        if( $query->have_posts() ) {
            while( $query->have_posts() ) {
                $query->the_post();

                $data[] = get_the_date('Y');
            }
        }

        wp_reset_postdata();

        return array_unique($data);
    }

    public function get_template($posts, $status = '') 
    {
        ob_start();

        foreach ($posts as $post) {
            echo '<div class="khayr-ajax__list col">';
            echo get_template_part( 'template-parts/blog-listing', null, $post );
            echo '</div>';
        }

        return ob_get_clean();
    }

    public function get_not_found_template($message = '')
    {
        ob_start();
        echo '<div class="khayr-ajax__list col not-found">';
        echo 'It seems we can\'t find what you\'re looking for.';
        echo '</div>';
        return ob_get_clean();
    }

    public function get_loadmore($wp_query, $active) {
        ob_start();
        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
                return ob_get_clean();
        $paged = $active;
        $max   = intval( $wp_query->max_num_pages );
        /**	Add current page to the array */
        if ( $paged >= 1 )
                $links[] = $paged;
        /**	Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
                $links[] = $paged - 1;
                $links[] = $paged - 2;
        }
        if ( ( $paged + 2 ) <= $max ) {
                $links[] = $paged + 2;
                $links[] = $paged + 1;
        }
        
        /**	Next Post Link */
        if ( $paged != $max ){
                ?>
                <div class="ajax__loadmore-button">
                    <button class="button button-primary" data-page="<?php echo $paged + 1;?>" data-ajax="loadmore">
                            Load More
                    </button>
                </div>
                <?php
        }
        return ob_get_clean();
    }

    public function get_infinite_scroll($wp_query, $active) {
        ob_start();
        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
                return ob_get_clean();
        $paged = $active;
        $max   = intval( $wp_query->max_num_pages );
        /**	Add current page to the array */
        if ( $paged >= 1 )
                $links[] = $paged;
        /**	Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
                $links[] = $paged - 1;
                $links[] = $paged - 2;
        }
        if ( ( $paged + 2 ) <= $max ) {
                $links[] = $paged + 2;
                $links[] = $paged + 1;
        }
        
        /**	Next Post Link */
        if ( $paged != $max ){
                ?>
                <div class="ajax__infinite_scroll" data-page="<?php echo $paged + 1;?>" data-ajax="infinite_scroll">
                </div>
                <?php
        }
        return ob_get_clean();
    }

    public function get_pagination($wp_query, $active) {
        ob_start();
        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
                return ob_get_clean();
        $paged = $active;
        $max   = intval( $wp_query->max_num_pages );
        /**	Add current page to the array */
        if ( $paged >= 1 )
                $links[] = $paged;
        /**	Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
                $links[] = $paged - 1;
                $links[] = $paged - 2;
        }
        if ( ( $paged + 2 ) <= $max ) {
                $links[] = $paged + 2;
                $links[] = $paged + 1;
        }
        echo '<div class="navigation"><ul class="list-inline">' . "\n";
        /**	Previous Post Link */
        if ( $paged != 1 ){
                printf('<li class="previous-btn"><a href="javascript:void(0)" class="button-pagination prev cursor-link" data-page="%s" data-ajax="pagination"><i class="icon icon-panah"></i></a></li>',($paged - 1) );
        } else {
            printf('<li class="previous-btn disabled"><a href="javascript:void(0)" class="button-pagination prev" data-page="%s" data-ajax="pagination"><i class="icon icon-panah"></i></a></li>',$paged );
        }
                
        /**	Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
                $class = 1 == $paged ? 'active' : 'cursor-link';

                printf( '<li><a href="javascript:void(0)" class="button-pagination link-text %s" data-page="%s" data-ajax="pagination">%s</a></li>' . "\n", $class, 1, 1 );

                if ( ! in_array( 2, $links ) )
                printf('<li><a href="javascript:void(0)" class="button-pagination link-text " data-page="%s" style="pointer-events:none"><span class="pagination-dots">...</span></a></li>',($paged + 1) );
        }
        /**	Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
                $class = $paged == $link ? 'active' : 'cursor-link';
                printf( '<li><a href="javascript:void(0)" class="button-pagination link-text %s" data-page="%s" data-ajax="pagination">%s</a></li>' . "\n", $class, $link, $link );
        }
        /**	Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
                if ( ! in_array( $max - 1, $links ) )
                printf('<li><a href="javascript:void(0)" class="button-pagination link-text" data-page="%s" style="pointer-events:none"><span class="pagination-dots">...</span></a></li>',($paged + 1) );

                $class = $paged == $max ? 'active' : '';
                printf( '<li><a href="javascript:void(0)" class="button-pagination link-text cursor-link %s" data-page="%s" data-ajax="pagination">%s</a></li>' . "\n", $class, $max, $max );
        }
        /**	Next Post Link */
        if ( $paged != $max ){
                printf('<li class="next-btn"><a href="javascript:void(0)" class="button-pagination cursor-link next" data-page="%s" data-ajax="pagination"><i class="icon icon-panah"></i></a></li>',($paged + 1) );
        } else {
            printf('<li class="next-btn disabled"><a href="javascript:void(0)" class="button-pagination next" data-page="%s" data-ajax="pagination"><i class="icon icon-panah"></i></a></li>', $paged);
        }
                
        echo '</ul></div>' . "\n";
        return ob_get_clean();
    }

    public static function is_ajax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}

new KhayrAjax();