class KhayrAjax {
    constructor() {
        //variable init
        this.elm = '.khayr-ajax';
        this.elmFilter = '.khayr-ajax__filter';
        this.elmPagination = '.khayr-ajax__pagination';
        this.elmLoadmore = '.khayr-ajax__load-more';
        this.elmInfinite = '.khayr-ajax__infinite_scroll';
        this.elmPlaceholder = '.khayr-ajax__placeholder';
        this.elmLists = '.khayr-ajax__lists';
        this.elmData = '.khayr-ajax__data';

        this.ajaxComplete = new Event('khayr-ajax-complete');

        this.data = [];
    }

    setup() {
        let elm = document.querySelectorAll(this.elm);
        if (!elm) {
            return;
        }

        elm.forEach(element => {
            if (!element.classList.contains('initialize')) {
                element.classList.add('initialize');
                this.filter(element);
                this.pagination(element);
                // this.loadmore(element);
                // this.infinite_scroll(element);

                let filterTrigger = element.querySelector('.khayr-ajax__filter-trigger'),
                    filter = element.querySelector('.khayr-ajax__filter');
                if (filterTrigger && filter && window.innerWidth < 991) {
                    filterTrigger.addEventListener('click', (e) => {
                        e.preventDefault();
                        let elm = e.target,
                            icon = elm.querySelector('.icon');
                        if (elm.classList.contains('active')) {
                            elm.classList.remove('active');
                            icon.classList.remove('icon-close');
                            icon.classList.add('icon-filter');
                            filter.style.overflow = 'hidden';
                            filter.style.height = '0px';
                        } else {
                            elm.classList.add('active');
                            icon.classList.add('icon-close');
                            icon.classList.remove('icon-filter');
                            let child = filter.querySelectorAll('.khayr-ajax__filter-group'), height = 0;
                            if (child) {
                                child.forEach(element => {
                                    height = height + element.offsetHeight;
                                });
                            }
                            filter.style.height = height+'px';
                            setTimeout(() => {
                                filter.style.overflow = 'visible';
                            }, 500);
                        }
                        return false;
                    });
                }
            }
        });
    }

    filter(wrapper) {
        let filter = wrapper.querySelector(this.elmFilter);

        if (!filter) {
            return;
        }

        let filterType = filter.querySelectorAll('[data-ajax-type]');

        filterType.forEach(elm => {
            switch (elm.getAttribute('data-ajax-type')) {
                case 'list':
                    this.list_filter(elm, wrapper);
                    break;
                case 'select':
                    this.select_filter(elm, wrapper);
                    break;
                case 'input':
                    this.input_filter(elm, wrapper);
                    break;
                case 'reset':
                    this.reset_filter(elm, wrapper);
                    break;
            }
        });
    }

    reset_filter(elm, wrapper) {
        elm.addEventListener('click', e => {
            e.preventDefault();

            let filter = wrapper.querySelector(this.elmFilter);

            if (filter) {
                let filterType = filter.querySelectorAll('[data-ajax-type]');
                const formData = wrapper.querySelector(this.elmData);
                let queryParams = new URLSearchParams(window.location.search);

                filterType.forEach(elm => {
                    switch (elm.getAttribute('data-ajax-type')) {
                        case 'select':
                            let index = elm.dataset.choicesIndex;
                            if (typeof window.selectChoice[index] !== 'undefined') {
                                window.selectChoice[index].setChoiceByValue('');
                            }
                            this.update_params(elm, formData, queryParams);
                            break;
                        case 'input':
                            elm.value = '';
                            break;
                    }
                });
            }

            this.filter_process(e, wrapper);
        });
    }

    input_filter(elm, wrapper) {
        elm.addEventListener('keydown', e => {
            if (e.key == 'Enter') {
                this.filter_process(e, wrapper);
            }
        });
    }

    list_filter(elm, wrapper) {
        elm.addEventListener('click', e => {
            e.preventDefault();
            let $this = e.target;

            if ($this.getAttribute('data-ajax') == 'type') {
                $this
                    .closest(this.elmFilter)
                    .querySelectorAll('[data-ajax="type"]')
                    .forEach(elm => {
                        elm.classList.remove('active');
                    });

                $this.classList.add('active');
            }

            this.filter_process(e, wrapper);
        });
    }

    loadmore(wrapper) {
        document.body.addEventListener('click', e => {
            let elm = e.target;
            if (
                elm &&
                elm.hasAttribute('data-ajax') &&
                elm.getAttribute('data-ajax') == 'loadmore' &&
                elm.closest(this.elm) == wrapper
            ) {
                e.preventDefault();
                this.filter_process(e, wrapper);
            }
        });
    }

    infinite_scroll(wrapper) {
        let placeholder = wrapper.querySelector(this.elmPlaceholder),
            infinite = wrapper.querySelector(this.elmInfinite);
        if (placeholder && infinite) {
            inViewDefault('.khayr-ajax__placeholder').on('enter', el => {
                if (
                    wrapper.classList.contains('load-infinite') &&
                    !wrapper.classList.contains('load-infinite-running') &&
                    !wrapper.classList.contains('is-loading')
                ) {
                    wrapper.classList.add('load-infinite-running');
                    let dataAjax = infinite.querySelector('.ajax__infinite_scroll');
                    if (dataAjax) {
                        this.filter_process({
                                target: dataAjax
                            },
                            wrapper
                        );
                    } else {
                        wrapper.classList.remove('load-infinite-running');
                    }
                }
            });
        }
    }

    select_filter(elm, wrapper) {
        elm.addEventListener('change', e => {
            this.filter_process(e, wrapper);
        });
    }

    pagination(wrapper) {
        document.body.addEventListener('click', e => {
            let elm = e.target;
            if (
                elm &&
                elm.hasAttribute('data-ajax') &&
                elm.getAttribute('data-ajax') == 'pagination' &&
                elm.closest(this.elm) == wrapper
            ) {
                e.preventDefault();
                this.filter_process(e, wrapper);
            }
        });
    }

    filter_process(e, wrapper) {
        let elm = e.target;
        const ajaxWrapper = wrapper;
        const formData = ajaxWrapper.querySelector(this.elmData);

        if (document.body.classList.contains('filter-modal-opened')) {
            document.body.classList.remove('filter-modal-opened');
        }

        if (
            elm.getAttribute('data-ajax') == 'loadmore' ||
            elm.getAttribute('data-ajax') == 'infinite_scroll'
        ) {
            ajaxWrapper.classList.add('loadmore-loading');
        } else {
            ajaxWrapper.classList.add('type-loading');
        }

        ajaxWrapper.classList.add('is-loading');

        this.before_execution(elm, formData);
        this.run(ajaxWrapper, formData, elm);
    }

    before_execution(el, formData) {
        let queryParams = new URLSearchParams(window.location.search);

        /*
         * Reset params in the form
         * Reset params in the URL
         */
        this.reset_params(formData, queryParams);

        /*
         * Update params in the form
         * Update params in the URL
         * Set history
         */
        this.update_params(el, formData, queryParams);
    }

    run(wrapper, formData, el) {
        // eslint-disable-next-line no-undef
        const url = Khayr.ajax_url || false;
        const templateWrapper = wrapper.querySelector(this.elmLists);
        const loadmoreWrapper = wrapper.querySelector(this.elmLoadmore);
        const paginationWrapper = wrapper.querySelector(this.elmPagination);
        const infiniteWrapper = wrapper.querySelector(this.elmInfinite);

        /**
         * Scroll to khayr-ajax
         */
        if (
            el.getAttribute('data-ajax') !== 'loadmore' &&
            el.getAttribute('data-ajax') !== 'infinite_scroll'
        ) {
            if (typeof window.scrollElmTo !== 'undefined') {
                window.scrollElmTo(this.elm, 500);
            }
        }

        let data = new FormData(formData);
        data.append('action', 'khayr_get_posts');

        if (
            el.getAttribute('data-ajax') == 'loadmore' ||
            el.getAttribute('data-ajax') == 'infinite_scroll'
        ) {
            data.append('more_item', true);
        }

        let fetchData = {
            method: 'POST',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'same-origin',
            body: data
        };

        fetch(url, fetchData)
            .then(response => {
                return response.json();
            })
            .then(data => {
                if (document.body.contains(infiniteWrapper)) {
                    infiniteWrapper.innerHTML = '';
                    infiniteWrapper.classList.add('hide');
                    wrapper.classList.remove('load-infinite');
                    wrapper.classList.remove('load-infinite-running');
                }

                if (document.body.contains(loadmoreWrapper)) {
                    loadmoreWrapper.innerHTML = '';
                    loadmoreWrapper.classList.add('hide');
                }

                if (document.body.contains(paginationWrapper)) {
                    paginationWrapper.innerHTML = '';
                    paginationWrapper.classList.add('hide');
                }

                if (data.posts != '') {
                    if (
                        el.getAttribute('data-ajax') == 'loadmore' ||
                        el.getAttribute('data-ajax') == 'infinite_scroll'
                    ) {
                        templateWrapper.insertAdjacentHTML('beforeend', data.posts);
                    } else {
                        templateWrapper.innerHTML = data.posts;
                    }
                } else {
                    templateWrapper.innerHTML =
                        '<div class="col xs-12 sm-12 md-12 lg-12"><span class="type-text">No Article Found</span></div>';
                }

                if (
                    document.body.contains(infiniteWrapper) &&
                    typeof data.infinite_scroll !== 'undefined' &&
                    data.infinite_scroll != ''
                ) {
                    infiniteWrapper.innerHTML = data.infinite_scroll;
                    infiniteWrapper.classList.remove('hide');
                    wrapper.classList.add('load-infinite');
                }

                if (
                    document.body.contains(loadmoreWrapper) &&
                    typeof data.loadmore !== 'undefined' &&
                    data.loadmore != ''
                ) {
                    loadmoreWrapper.innerHTML = data.loadmore;
                    loadmoreWrapper.classList.remove('hide');
                }

                if (
                    document.body.contains(paginationWrapper) &&
                    typeof data.pagination !== 'undefined' &&
                    data.pagination != ''
                ) {
                    paginationWrapper.innerHTML = data.pagination;
                    paginationWrapper.classList.remove('hide');
                }

                document.dispatchEvent(this.ajaxComplete);

                wrapper.classList.remove('is-loading');
                wrapper.classList.remove('loadmore-loading');
                wrapper.classList.remove('type-loading');
            })
            .catch(err => {
                // eslint-disable-next-line no-console
                console.log(err);
            });
    }

    update_params(el, formData, queryParams) {
        switch (el.getAttribute('data-ajax')) {
            case 'year':
                if (el.hasAttribute('data-slug')) {
                    formData.querySelector('input[name="year"]').value = el.getAttribute(
                        'data-slug'
                    );
                    if (el.getAttribute('data-slug') !== 'all' || el.getAttribute('data-slug') !== '') {
                        queryParams.set('year', el.getAttribute('data-slug'));
                    } else {
                        queryParams.set('year', '');
                    }
                } else {
                    formData.querySelector('input[name="year"]').value = el.value;
                    if (el.value !== 'all' || el.value !== '') {
                        queryParams.set('year', el.value);
                    } else {
                        queryParams.set('year', '');
                    }
                }
                break;
            case 'post_tag':
                if (el.hasAttribute('data-slug')) {
                    formData.querySelector('input[name="post_tag"]').value = el.getAttribute(
                        'data-slug'
                    );
                    if (el.getAttribute('data-slug') !== 'all' || el.getAttribute('data-slug') !== '') {
                        queryParams.set('post_tag', el.getAttribute('data-slug'));
                    } else {
                        queryParams.set('post_tag', '');
                    }
                } else {
                    formData.querySelector('input[name="post_tag"]').value = el.value;
                    if (el.value !== 'all' || el.value !== '') {
                        queryParams.set('post_tag', el.value);
                    } else {
                        queryParams.set('post_tag', '');
                    }
                }
                break;
            case 'category':
                if (el.hasAttribute('data-slug')) {
                    formData.querySelector('input[name="category"]').value = el.getAttribute(
                        'data-slug'
                    );
                    if (el.getAttribute('data-slug') !== 'all' || el.getAttribute('data-slug') !== '') {
                        queryParams.set('category', el.getAttribute('data-slug'));
                    } else {
                        queryParams.set('category', '');
                    }
                } else {
                    formData.querySelector('input[name="category"]').value = el.value;
                    if (el.value !== 'all' || el.value !== '') {
                        queryParams.set('category', el.value);
                    } else {
                        queryParams.set('category', '');
                    }
                }
                break;
            case 'sort_by':
                formData.querySelector('input[name="sort_by"]').value = el.value;
                queryParams.set('sort_by', el.value);
                break;
            case 'pagination':
                formData.querySelector('input[name="paging"]').value = el.getAttribute(
                    'data-page'
                );
                queryParams.set('paging', el.getAttribute('data-page'));
                break;
            case 'loadmore':
                formData.querySelector('input[name="paging"]').value = el.getAttribute(
                    'data-page'
                );
                // queryParams.set('paging', el.getAttribute('data-page'));
                break;
            case 'infinite_scroll':
                formData.querySelector('input[name="paging"]').value = el.getAttribute(
                    'data-page'
                );
                // queryParams.set('paging', el.getAttribute('data-page'));
                break;
            case 'search':
                formData.querySelector('input[name="keyword"]').value = el.value;
                queryParams.set('keyword', el.value);
                break;
        }

        if (queryParams.toString() != '') {
            history.pushState(null, null, '?' + queryParams.toString());
        }
    }

    reset_params(formData, queryParams) {
        formData.querySelector('input[name="paging"]').value = 1;
        queryParams.delete('paging');
        // queryParams.delete('cat');
        // queryParams.delete('sort-by');
        // queryParams.delete('keyword');
    }

}

let khayrajax = new KhayrAjax();
khayrajax.setup();