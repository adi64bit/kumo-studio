<?php $header = khayr_option('header', 'options'); ?>
<header class="header">
    <div class="container">
        <div class="header__wrapper">

            <div class="header__logo-wrapper">
                <a href="<?php echo home_url();?>" class="cursor-link d-block header__logo">
                    <img src="<?php echo $header[0]['logo']; ?>" alt="Kumo Studio Logo" class="header__logo-main">
                </a>
            </div>

            <div class="header__controls">
                <div class="header__controls-button">
                    <ul>
                        <li class="">
                            <button class="header__controls-button--menu cursor-link" type="button">
                                <span class="header__controls-button--menu-box">
                                    <span class="header__controls-button--menu-inner"></span>
                                </span>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="header__menus">
                <div class="container">
                    <?php 
                        wp_nav_menu( array(
                            'menu_class'        => 'header__menus-menu menu-navigation',
                            'menu_id'           => 'main-nav',
                            'theme_location'    => 'main_menu',
                        ) );
                    ?>
                </div>
            </div>

            <div class="header__menus mini">
                <div class="container">
                    <?php 
                        wp_nav_menu( array(
                            'menu_class'        => 'header__menus-menu menu-navigation',
                            'menu_id'           => 'main-nav',
                            'theme_location'    => 'main_menu',
                        ) );
                    ?>
                </div>
            </div>

        </div>
    </div>
</header>