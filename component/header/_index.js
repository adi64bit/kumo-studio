(function (Khayr) {
    Khayr = Khayr || [];

    // header
    var KhayrHeader = function KhayrHeader() {

        // init
        header = document.querySelector('.header');
        if (header) {
            let body = document.body;
            const header_scroll_effect = () => {
                let scrollBarPosition =
                    window.scrollY || window.pageYOffset || document.body.scrollTop;
                if (scrollBarPosition >= 80) {
                    header.classList.add('sticky-header');
                    body.classList.add('sticky-header');
                } else {
                    header.classList.remove('sticky-header');
                    body.classList.remove('sticky-header');
                }
            }

            document.addEventListener('scroll', () => {
                header_scroll_effect();
            });

            let menu = header.querySelector('.header__controls-button--menu');
            if (menu) {
                menu.addEventListener('click', (e) => {
                    e.preventDefault();
                    if (header.classList.contains('menu-open')) {
                        header.classList.remove('menu-open');
                    } else {
                        header.classList.add('menu-open');
                    }
                    return false;
                });
            }

            // let links = header.querySelectorAll('.header__menus li:not(.small-link) a[href^="#"], .header__menus li:not(.small-link) a[href^="/#"]');
            // if (links) {
            //     links.forEach(element => {
            //         element.addEventListener('click', (e) => {
            //             e.preventDefault();
            //             let href = e.target.getAttribute('href').split('#')[1],
            //                 target = document.querySelector('#' + href);
            //                 console.log(target);
            //             if (target) {
            //                 jQuery('html,body').animate({
            //                     scrollTop: target.offsetTop + 'px'
            //                 }, 600);
            //             } else {
            //                 //window.location.href = '/' + href;
            //             }
            //             if (header.classList.contains('menu-open')) {
            //                 header.classList.remove('menu-open');
            //             }

            //             return false;
            //         });
            //     });
            // }
        }
    };

    Khayr.header = Khayr.header || new KhayrHeader();
})(Khayr);