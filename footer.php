<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Khayr
 */

?>
	</div>
	<?php get_template_part( 'component/footer/footer' ); ?>
	</div>
</div><!-- #page -->

<div class="scroll-to-top">
	<a href="javascript:void(0);" class="scroll-to-top-trigger cursor-link"><span class="icon icon-panah"></span></a>
</div>

<div class="square-cursor" data-khayr-grow="cursor-animation">
	<div class="square-cursor__inner"></div>
</div>

<?php wp_footer(); ?>
</body>
</html>
