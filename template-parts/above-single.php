<?php 
$delay = 0;
?>
<div class="single-blog__top">
    <div class="single-blog__top--breadcrumb mobile" <?php echo khayr_scroll($delay.'s'); ?>>
        <?php 
            if ( function_exists('yoast_breadcrumb') ) {
                yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
            }
        ?>
    </div>
    <div class="single-blog__top--image-wrap">
        <div class="single-blog__top--image" <?php echo khayr_scroll($delay.'s'); ?>>
            <?php 
                $delay = 0;
                $id     = get_the_ID();
                $title  = get_the_title($id);
                if (has_post_thumbnail($id)) {
                    ?>
                    <div class="content-img__item">
                        <div class="content-img__item-imgwrap">
                            <?php echo khayr_get_image(get_post_thumbnail_id($id), 'full', array(
                                    'class' => array('content-img__item-img')
                                )); ?>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
    <div class="single-blog__top--content">
        <div class="single-blog__top--breadcrumb" <?php echo khayr_scroll($delay.'s'); ?>>
            <?php 
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
                }
            ?>
        </div>
        <div class="single-blog__top--heading">
            <?php 
                $delay = $delay + 0.2;
            ?>
            <h1 <?php echo khayr_scroll($delay.'s'); ?> data-text-stagger><?php echo $title;?></h1>
            <div class="single-blog__top--heading-wrap">
                <?php 
                    $cat = get_the_category($id);
                    if($cat){
                        $delay = $delay + 0.2;
                        echo '<ul class="single-blog__top--heading-cat">';
                        foreach ($cat as $key => $value) {
                            echo '<li '.khayr_scroll($delay.'s').'><a href="'.get_term_link($value->term_id).'">'.$value->name.'</a></li>';
                            $delay = $delay + 0.2;
                        }
                        echo '</ul>';
                    }
                    $date = get_the_date('d M', $id);
                    echo '<span class="single-blog__top--heading-date" '.khayr_scroll($delay.'s').'>'.$date.'</span>';
                ?>
            </div>
        </div>
        <div class="single-blog__top--social">
            <?php 
                
                $link   = get_permalink($id);
                $socials = [
                    [
                        'name'  => 'Facebook',
                        'link'  => 'https://www.facebook.com/sharer/sharer.php?u='.$link,
                        'icon'  => 'facebook'
                    ],
                    [
                        'name'  => 'Twitter',
                        'link'  => 'https://twitter.com/share?text='.urlencode(html_entity_decode($title . " - ". get_bloginfo('name'), ENT_COMPAT, 'UTF-8'))."&amp;url=" . get_bloginfo('url') . "?p=" . $id,
                        'icon'  => 'twitter'
                    ],
                    [
                        'name'  => 'Linkedin',
                        'link'  => 'https://www.linkedin.com/shareArticle?mini=true&url=' . $link .  '&amp;title=' . urlencode(html_entity_decode($title . " - ". get_bloginfo('name'), ENT_COMPAT, 'UTF-8')),
                        'icon'  => 'linkedin'
                    ]
                ];
                echo '<ul>';
                $delay = $delay + 0.2;
                foreach ($socials as $key => $value) {
                    echo '<li '.khayr_scroll($delay.'s').'><a href="'.$value['link'].'" target="_blank"><i class="icon icon-'.$value['icon'].'"></i></a></li>';
                    $delay = $delay + 0.2;
                }
                echo '</ul>';
            ?>
        </div>
    </div>
</div>