<div class="single-blog__bottom">
    <div class="single-blog__bottom--wrap">
        <div class="single-blog__bottom--cat">
            <?php 
                $cat = get_the_category(get_the_ID());
                if($cat){
                    echo '<ul>';
                    echo '<li><strong>Categories:</strong></li>';
                    foreach ($cat as $key => $value) {
                        echo '<li><a href="'.get_term_link($value->term_id).'">'.$value->name.' ('.$value->count.')</a></li>';
                    }
                    echo '</ul>';
                }
            ?>
        </div>
        <div class="single-blog__bottom--tag">
            <?php 
                $tag = get_the_tags(get_the_ID());
                if($tag){
                    echo '<ul>';
                    echo '<li><strong>Tags:</strong></li>';
                    foreach ($tag as $key => $value) {
                        echo '<li><a href="'.get_term_link($value->term_id).'">'.$value->name.'</a></li>';
                    }
                    echo '</ul>';
                }
            ?>
        </div>
        <div class="single-blog__bottom--social">
            <?php 
                $id     = get_the_ID();
                $title  = get_the_title($id);
                $link   = get_permalink($id);
                $socials = [
                    [
                        'name'  => 'Facebook',
                        'link'  => 'https://www.facebook.com/sharer/sharer.php?u='.$link,
                        'icon'  => 'facebook'
                    ],
                    [
                        'name'  => 'Twitter',
                        'link'  => 'https://twitter.com/share?text='.urlencode(html_entity_decode($title . " - ". get_bloginfo('name'), ENT_COMPAT, 'UTF-8'))."&amp;url=" . get_bloginfo('url') . "?p=" . $id,
                        'icon'  => 'twitter'
                    ],
                    [
                        'name'  => 'Linkedin',
                        'link'  => 'https://www.linkedin.com/shareArticle?mini=true&url=' . $link .  '&amp;title=' . urlencode(html_entity_decode($title . " - ". get_bloginfo('name'), ENT_COMPAT, 'UTF-8')),
                        'icon'  => 'linkedin'
                    ]
                ];
                echo '<ul>';
                foreach ($socials as $key => $value) {
                    echo '<li><a href="'.$value['link'].'" target="_blank"><i class="icon icon-'.$value['icon'].'"></i></a></li>';
                }
                echo '</ul>';
            ?>
        </div>
        <div class="single-blog__bottom--navigation">
            <?php 
                the_post_navigation(
                    array(
                        'prev_text' => '<span class="nav-subtitle"><i class="icon icon-panah"></i>' . esc_html__( 'Older blog', 'kumo' ) . '</span> <span class="nav-title">%title</span>',
                        'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Newer blog', 'kumo' ) . '<i class="icon icon-panah"></i></span> <span class="nav-title">%title</span>',
                    )
                );
            ?>
        </div>
    </div>
</div>
<?php 
$single_blog = khayr_option('single_blog', 'options');
$hide_default_cta = get_post_meta(get_the_ID(), 'hide_default_cta', true );
if ($hide_default_cta == 'on') {
    
} else {
    if (isset($single_blog[0]['show_cta']) && $single_blog[0]['show_cta'] == 'on') {
        $contact = [];
        if (isset($single_blog[0]['cta_link_text_1']) && $single_blog[0]['cta_link_text_1'] && isset($single_blog[0]['cta_link_url_1']) && $single_blog[0]['cta_link_url_1']) {
            $contact[] = [
                'title' => $single_blog[0]['cta_link_text_1'],
                'link'  => $single_blog[0]['cta_link_url_1'],
                'icon'  => isset($single_blog[0]['cta_link_icon_1']) ? $single_blog[0]['cta_link_icon_1'] : ''
            ];
        }
        if (isset($single_blog[0]['cta_link_text_2']) && $single_blog[0]['cta_link_text_2'] && isset($single_blog[0]['cta_link_url_2']) && $single_blog[0]['cta_link_url_2']) {
            $contact[] = [
                'title' => $single_blog[0]['cta_link_text_2'],
                'link'  => $single_blog[0]['cta_link_url_2'],
                'icon'  => isset($single_blog[0]['cta_link_icon_2']) ? $single_blog[0]['cta_link_icon_2'] : ''
            ];
        }
        $instance = array(
            'heading' => isset($single_blog[0]['cta_text']) ? $single_blog[0]['cta_text'] : '',
            'subheading' => isset($single_blog[0]['cta_sub_text']) ? $single_blog[0]['cta_sub_text'] : '',
            'contact' => $contact,
            'contact_image' => isset($single_blog[0]['cta_image']) && $single_blog[0]['cta_image'] ? attachment_url_to_postid($single_blog[0]['cta_image']) : '',
            'contact_form' => isset($single_blog[0]['cta_form']) ? $single_blog[0]['cta_form'] : '',
            'show_contact_form' => isset($single_blog[0]['show_newsletter']) && $single_blog[0]['show_newsletter'] == 'on' ? true : false,
            'contact_form_position' => 'center'
        );
        require_once get_template_directory() . '/inc/widgets/kbw_contact/tpl/default.php';
    }
}
?>