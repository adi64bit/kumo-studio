<?php 
    $categories = wp_get_post_categories( $args->ID, array( 'fields' => 'all' ) );
    $content = $args->post_excerpt;
    if ($content && $content != '') {
        # code...
    } else {
        ob_start();
        if ( class_exists( 'SiteOrigin_Panels' ) && get_post_meta( $args->ID, 'panels_data', true ) ) {
            echo SiteOrigin_Panels::renderer()->render( $args->ID );
        } else {
            $post_content = wpautop(get_post( $args->ID )->post_content);
            $modified_content = apply_filters( 'the_content', $post_content );
            echo $modified_content;
        }
        $content = ob_get_clean();
        $content = wp_trim_words($content, 25, '...');
    }
    $items = array(
        'category' => $categories,
        'title' => get_the_title( $args->ID ),
        'description'   => $content,
        'date'  => get_the_date('d M Y', $args->ID),
        'link'  => get_the_permalink($args->ID),
        'image' => has_post_thumbnail($args->ID) ? khayr_get_image(get_post_thumbnail_id($args->ID), 'full', array(
            'class' => array('content-img__item-img')
        )) : false
    );
?>
<div class="module-latest-blog__item" <?php echo khayr_scroll(); ?>>
    <div class="module-latest-blog__item--image">
        <?php 
            if ($items['image']) {
                ?>
                    <div class="content-img__item">
                        <div class="content-img__item-imgwrap">
                            <?php echo $items['image']; ?>
                        </div>
                    </div>
                <?php
            }
        ?>
    </div>
    <div class="module-latest-blog__item--content">
        <?php 
            if ($items['category']) {
                echo '<ul class="module-latest-blog__item-category">';
                foreach ($items['category'] as $k => $v) {
                    echo '<li><a href="'.get_term_link($v->term_id).'" class="cursor-link">'.$v->name.'</a></li>';
                }
                echo '</ul>';
            }
            if($items['title']){
                echo '<h3 class="module-latest-blog__item--title">'.$items['title'].'</h3>';
            }
            if($items['date']){
                echo '<span class="module-latest-blog__item--date-inner">'.$items['date'].'</span>';
            }
            if($items['description']){
                echo '<p class="module-latest-blog__item--description">'.$items['description'].'</p>';
            }
            if ($items['link']) {
                ?>
                    <div class="module module-button" data-khayr-grow="module-button">
                        <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                            <a href="<?php echo $items['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text">Read Blog</span><span class="icon icon-panah"></span></a>
                        </div>
                    </div>
                <?php
            }
        ?>
    </div>
</div>