<?php 
    if (!isset($args)) {
        $args = array();
    }
?>

<div class="archive-title">
    <h1 <?php echo khayr_scroll();?> data-text-stagger><?php echo get_the_archive_title();?></h1>
</div>
