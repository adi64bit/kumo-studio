<?php

extract(shortcode_atts(array(
    'image_id'      => false,
    'image_size'    => 'large',
    'show_caption'  => false,
    'caption'       => '',
    'alt'           => '',
    'class'         => '',
    'srcset'        => false,
    'style'         => 'default',
    'image_ratio'   => true
), $atts));

if (!$image_id) {
    return '';
}

$lazyload = false;
if (function_exists('khayr_option')) {
    if (khayr_option('images_lazyload') == 'lazyload') {
        $lazyload = true;
    }
}

?>
<div class="khayr-module module-khayr-image khayr-image <?php echo esc_attr($class);?>">
    <div class="khayr-image__wrapper">
        <?php 
            if ($image_ratio && function_exists('khayr_get_image_ratio')) {
                echo khayr_get_image_ratio($image_id);
            }

            if (function_exists('khayr_get_image')) {
                $alt_text = $alt !== '' ? esc_attr($alt) : false;
                echo khayr_get_image($image_id, $image_size, array(
                    'alt'       => $alt_text,
                    'class'     => 'khayr-image__img',
                    'lazyload'  => $lazyload,
                    'srcset'    => $srcset
                ));
            }
        ?>
    </div>

    <?php
    if ($show_caption) {
        $caption_text = $caption !== '' ? $caption : wp_get_attachment_caption($image_id);
        ?>
        <div class="khayr-image__caption">
            <p><?php echo esc_attr($caption_text);?></p>
        </div>
        <?php
    }
    ?>
</div>