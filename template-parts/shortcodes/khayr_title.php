<?php
extract(shortcode_atts(array(
    'title_tag'       => 'h2',
    'title'           => '',
    'subtitle_text'   => '',
    'content_wysiwyg' => ''
), $atts));
?>
<div class="khayr-module module-title khayr-section-title">
    <?php 
        if ($title !== '') {
            echo '<'.esc_attr($title_tag).' class="khayr-section-title__heading">'.esc_attr($title).'</'.esc_attr($title_tag).'>';
        }

        if ($subtitle_text !== '') {
            echo '<span class="khayr-section-title__subheading">'.esc_attr($subtitle_text).'</span>';
        }

        if ($content_wysiwyg !== '') {
            echo '<div class="khayr-section-title__content">'.wpautop($content_wysiwyg).'</div>';
        }
    ?>
</div>