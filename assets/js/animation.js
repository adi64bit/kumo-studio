(function (Khayr) {
    Khayr = Khayr || [];

    // animation
    var KhayrAnimation = function KhayrAnimation() {
        const salSelector = '[data-sal], [data-text-stagger], .graphic-element-svg';
        const salGlobal = sal({
            selector: salSelector,
            once: true,
        });
        const textStagger = () => {
            let text = '[data-text-stagger]',
                elms = document.querySelectorAll(text),
                salText = false;
            if (elms) {
                elms.forEach(element => {
                    let alpha = Splitting({
                        target: element,
                        by: 'chars'
                    });

                    let animation = anime({
                        targets: alpha[0].chars,
                        translateY: ['110%', '0%'],
                        opacity: [0, 1],
                        duration: 600,
                        autoplay: false,
                        delay: anime.stagger(50)
                    });

                    element.addEventListener('sal:in', () => {
                        if (!element.classList.contains('init')) {
                            element.classList.add('init');
                            animation.play();
                        }
                    });
                });
            }
        };

        const graphicElm = () => {
            let graphics = document.querySelector('.graphic__elements');
            if (graphics) {
                let graphic = graphics.querySelectorAll('.graphic__element');
                if (graphic) {
                    let svgsal = false;
                    let svgAnim = '.graphic-element-svg';
                    graphic.forEach(element => {
                        let key = element.dataset.key,
                            imgGraphic = element.dataset.img,
                            pos_x = element.dataset.posX,
                            pos_y = element.dataset.posY,
                            svg = element.querySelector('svg');

                        let elms = document.querySelectorAll('.add-graphic-' + key);
                        if (elms) {
                            elms.forEach(elm => {
                                let div = document.createElement('div');
                                div.classList.add('graphic-element-' + key);
                                if (svg) {
                                    svgsal = true;
                                    let newsvg = svg.cloneNode(true);
                                    newsvg.style.left = pos_x + '%';
                                    newsvg.style.top = pos_y + '%';
                                    div.appendChild(newsvg);
                                    div.classList.add('graphic-element-svg');

                                    let paths = div.querySelectorAll('path');
                                    let fill = [];
                                    let tl = anime.timeline({
                                        easing: 'easeInOutSine',
                                        autoplay: false
                                    });

                                    tl.add({
                                        targets: paths,
                                        strokeDashoffset: [anime.setDashoffset, 0],
                                        duration: 2000,
                                        delay: function (el, i) {
                                            return i * 250
                                        },
                                        begin: function (anim) {
                                            newsvg.classList.add('initialize');
                                            if (anim.animatables) {
                                                anim.animatables.forEach((element, index) => {
                                                    fill[index] = element.target.getAttribute('fill');
                                                    element.target.setAttribute('fill', 'rgba(0,0,0,0)');
                                                    element.target.setAttribute('stroke', fill[index]);
                                                    element.target.setAttribute('stroke-width', '2px');
                                                });
                                            }
                                        },
                                        complete: function (anim) {
                                            if (anim.animatables) {
                                                anim.animatables.forEach((element, index) => {
                                                    let childAnim = anime({
                                                        targets: element.target,
                                                        duration: 1000,
                                                        fill: ['rgba(0,0,0,0)', '' + fill[index] + ''],
                                                        strokeWidth: ['2px', '0px'],
                                                        complete: () => {
                                                            element.target.setAttribute('stroke', 'rgba(0,0,0,0)');
                                                        }
                                                    });
                                                });
                                            }
                                            newsvg.classList.add('complete');
                                        },
                                        direction: 'forwards'
                                    });

                                    div.addEventListener('sal:in', () => {
                                        if (!div.classList.contains('initialize')) {
                                            div.classList.add('initialize');
                                            tl.play();
                                        }
                                    });
                                } else {
                                    let img = document.createElement('img');
                                    img.src = imgGraphic;
                                    img.style.left = pos_x + '%';
                                    img.style.top = pos_y + '%';
                                    div.appendChild(img);
                                }
                                elm.insertBefore(div, elm.firstChild);
                            });
                        }
                    });
                    if (svgsal) {
                        salGlobal.update();
                    }
                }
            }
        };

        const laxFunc = () => {
            lax.init()

            // Add a driver that we use to control our animations
            lax.addDriver('scrollY', function () {
                return window.scrollY
            })

            // Add animation bindings to elements
            lax.addElements('.lax-elm-up', {
                scrollY: {
                    translateY: [
                        ["elInY", "elCenterY", "elOutY"],
                        [150, 0, -50],
                    ]
                }
            });

            // Add animation bindings to elements
            lax.addElements('.lax-elm-up-0', {
                scrollY: {
                    translateY: [
                        ["elInY", "elCenterY", "elOutY"],
                        [0, 0, -110],
                    ]
                }
            });

            lax.addElements('.lax-elm-down', {
                scrollY: {
                    translateY: [
                        ["elInY", "elCenterY", "elOutY"],
                        [-50, 0, 150],
                    ]
                }
            });
        }


        // init
        document.addEventListener('khayr-ajax-complete', () => {
            salGlobal.update();
        });
        window.addEventListener('load', () => {
            if (window.innerWidth > 991) {
                laxFunc();
                graphicElm();
            }
        });
        textStagger();
    };

    Khayr.animation = Khayr.animation || new KhayrAnimation();
})(Khayr);