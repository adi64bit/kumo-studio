(function (Khayr) {
    Khayr = Khayr || [];

    var KhayrCursor = function KhayrCursor(element) {
        this.cursor = element;
        this.cursorInner = element.querySelector(".square-cursor__inner");
        this.cursorObjectBox = this.cursorInner.getBoundingClientRect();
        this.cursorBox = this.cursor.getBoundingClientRect();
        this.cursorIsStuck = false;
        this.clientX = -100;
        this.clientY = -100;
        this.showCursor = false;

        this.cursorOriginals = {
            width: this.cursorInner.offsetWidth,
            height: this.cursorInner.offsetHeight
        };

        document.addEventListener("mousemove", e => {
            this.clientX = e.clientX;
            this.clientY = e.clientY;
        });

        const render = () => {
            if (!this.isStuck) {
                anime({
                    targets: this.cursor,
                    translateX: this.clientX - this.cursorBox.width / 2,
                    translateY: this.clientY - this.cursorBox.width / 2
                });
            }
            requestAnimationFrame(render);
        };
        requestAnimationFrame(render);

        let swiper = document.querySelectorAll('.swiper');
        if (swiper) {
            swiper.forEach(element => {
                element.addEventListener('mouseenter', () => {
                    this.cursor.classList.add('slide');
                    if (element.classList.contains('swiper-vertical')) {
                        this.cursor.classList.add('rotate');
                    }
                });
                element.addEventListener('mouseleave', () => {
                    this.cursor.classList.remove('slide');
                    if (element.classList.contains('swiper-vertical')) {
                        this.cursor.classList.remove('rotate');
                    }
                });
            });
        }

        let faq = document.querySelectorAll('.module-faq-accordion:not(.always-open) .module-faq-accordion__content-item');
        if (faq) {
            faq.forEach(element => {
                element.addEventListener('mouseover', () => {
                    if (element.classList.contains('im-faq-accordion--active')) {
                        this.cursor.classList.remove('faq-open');
                        this.cursor.classList.add('faq-close');
                    } else {
                        this.cursor.classList.remove('faq-close');
                        this.cursor.classList.add('faq-open');
                    }
                });
                element.addEventListener('mouseleave', () => {
                    this.cursor.classList.remove('faq-open');
                    this.cursor.classList.remove('faq-close');
                });
            });
        }

        let link = document.querySelectorAll('.cursor-link'),
            anim1 = null,
            anim2 = null;
        if (link) {
            link.forEach(element => {
                element.addEventListener('mouseenter', () => {
                    let width = element.offsetWidth + 60,
                        height = element.offsetHeight + 30;
                    if (anim2) {
                        anim2.pause();
                        anim2.remove(this.cursorInner);
                        anim2 = null;
                    }
                    anim1 = anime({
                        targets: this.cursorInner,
                        duration: 1000,
                        width: ['30px', width + 'px'],
                        height: ['30px', height + 'px'],
                        left: ['35px', (100 - width) / 2 + 'px'],
                        top: ['35px', (100 - height) / 2 + 'px'],
                        borderRadius: ['50%', '0%'],
                        // rotate: ['-45deg', '0deg']
                        // rotate: ['0deg']
                    });
                });

                element.addEventListener('mouseleave', () => {
                    let width = element.offsetWidth + 60,
                        height = element.offsetHeight + 30;
                    if (anim1) {
                        anim1.pause();
                        anim1.remove(this.cursorInner);
                        anim1 = null;
                    }
                    anim2 = anime({
                        targets: this.cursorInner,
                        duration: 1000,
                        width: [width + 'px', '30px'],
                        height: [height + 'px', '30px'],
                        top: [(100 - height) / 2 + 'px', '35px'],
                        left: [(100 - width) / 2 + 'px', '35px'],
                        borderRadius: ['0%', '50%'],
                        // rotate: ['-45deg', '0%'],
                        // rotate: ['0%']
                    });
                });
            });
        }
    };

    Khayr.exec.add('cursor-animation', function (elm) {
        return new KhayrCursor(elm);
    }, 'grow');

})(Khayr);

document.addEventListener('DOMContentLoaded', function () {
    window.scrollAnchorTo = function (item, speed) {
        let header_offset =
            jQuery('.header').css('position') == 'sticky' ||
            jQuery('.header').css('position') == 'fixed' ?
            parseInt(jQuery('.header').innerHeight()) :
            0;
        let header = document.querySelector('.header');
        let wpadmin = jQuery('#wpadminbar').length ? parseInt(jQuery('#wpadminbar').innerHeight()) : 0;
        header_offset = (header_offset * 1) + wpadmin;
        let item_offset = 0;
        let clicked_item = jQuery('[href="' + item + '"]');
        if (speed == undefined) {
            speed = 800;
        }
        if (jQuery(item).length > 0) {
            item_offset = jQuery(item).offset() !== null ? jQuery(item).offset().top : 0;
            if (item_offset >= 0) {
                jQuery('html,body').animate({
                        scrollTop: jQuery(item).offset().top - header_offset
                    },
                    speed,
                    function () {
                        if (clicked_item.length) {
                            if (item.substring(0, 1) === '#') {
                                window.location.hash = item;
                                window.scrollTo(0, jQuery(item).offset().top - header_offset);
                            } else {
                                window.history.pushState(
                                    null,
                                    clicked_item.attr('title'),
                                    clicked_item.attr('href')
                                );
                            }
                        }
                    }
                );

                if (header && header.classList.contains('menu-open')) {
                    header.classList.remove('menu-open');
                }
            }
        }
    };

    jQuery(document).on('click', 'a[href^="#"]', function(e) {
        e.preventDefault();
        const hash = jQuery(this).attr('href');
        const $speed = jQuery(this).data('speed') ? jQuery(this).data('speed') : 800;
        let module_contact = jQuery('.module-contact');
        if(hash == '#get-in-touch' && module_contact.length > 0) {
            window.scrollAnchorTo('.module-contact', $speed);
        } else {
            window.scrollAnchorTo(hash, $speed);
        }
    });

    jQuery(document).on('click', 'a[href*="/#"]', function(e) {
        e.preventDefault();
        const hash = jQuery(this).attr('href').split('#')[1];
        const $speed = jQuery(this).data('speed') ? jQuery(this).data('speed') : 800;
        let clicked_item = jQuery('#' + hash);
        let module_contact = jQuery('.module-contact');
        if (clicked_item.length > 0) {
            window.scrollAnchorTo('#' + hash, $speed);
        } else if(hash == 'get-in-touch' && module_contact.length > 0) {
            window.scrollAnchorTo('.module-contact', $speed);
        } else {
            window.location.href = jQuery(this).attr('href');
        }
        return false;
    });

    if (window.location.hash) {
        let window_hash = window.location.hash;
        setTimeout(() => {
            let module_contact = jQuery('.module-contact');
            if (window_hash == '#get-in-touch' && module_contact.length > 0) {
                window.scrollAnchorTo('.module-contact', 800);
            } else {
                window.scrollAnchorTo(window_hash, 800);
            }
        }, 300);
    }
});