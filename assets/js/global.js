(function (Khayr) {
    Khayr = Khayr || [];

    // Events
    class KhayrEvents {
        constructor() {

        }

        add(eventName, callback) {
            if (typeof Khayr.evt[eventName] == 'undefined') {
                Khayr.evt[eventName] = [];
            }

            Khayr.evt[eventName].push(callback);
        }

        trigger(eventName) {
            if (typeof Khayr.evt[eventName] !== 'undefined' && Khayr.evt[eventName].length > 0) {
                var args = arguments;
                Khayr.evt[eventName].forEach(func => {
                    func([].slice.call(args, 1));
                });
            }
        }
    }

    Khayr.list = Khayr.list || new KhayrEvents();

    // Executor
    class KhayrExecutor {
        constructor() {
            var Self = this;

            Khayr.list.add('khayr-init', function () {
                Self.run();
            });

            Khayr.list.add('khayr-diet-reset', function () {
                Self.reset();
            });
        }

        run() {
            var growElements = document.querySelectorAll('[data-khayr-grow]');
            if (growElements) {
                growElements.forEach(element => {
                    if (typeof element.grow == 'undefined') {
                        var growName = element.dataset.khayrGrow;
                        element.grow = 'executed';
                        if (growName && typeof Khayr.grow[growName] !== 'undefined') {
                            Khayr.grow[growName](element);
                        }
                    }
                });
            }

            var dietElements = document.querySelectorAll('[data-khayr-diet]');
            if (dietElements) {
                dietElements.forEach(element => {
                    if (typeof element.diet == 'undefined') {
                        var dietName = element.dataset.khayrDiet;
                        element.diet = 'executed';
                        if (dietName && typeof Khayr.diet[dietName] !== 'undefined') {
                            Khayr.diet[dietName](element);
                        }
                    }
                });
            }
        }

        add(name, callback, type) {
            if (type == 'diet') {
                if (typeof Khayr.diet[name] == 'undefined') {
                    Khayr.diet[name] = callback;
                }
            } else {
                if (typeof Khayr.grow[name] == 'undefined') {
                    Khayr.grow[name] = callback;
                }
            }
        }

        reset() {
            Khayr.diet = [];
        }
    }

    Khayr.exec = Khayr.exec || new KhayrExecutor();

    document.addEventListener('DOMContentLoaded', () => {
        setTimeout(() => {
            Khayr.list.trigger('khayr-init');
        }, 100);

        let buttonAnchor = document.querySelector('.scroll-to-top-trigger');
            if(buttonAnchor){
                buttonAnchor.addEventListener('click', (e) => {
                    e.preventDefault();
                    jQuery('html, body').animate({
                        scrollTop : '0px'
                    });
                    return false;
                });
            }

            document.querySelectorAll('a').forEach(link => {
                if (link.querySelector('strong')) {
                    link.classList.add('has-strong');
                }
            });
    });
})(Khayr);