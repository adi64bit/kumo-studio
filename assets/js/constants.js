/**
 * Easy selector helper function
 */
const select = (el, all = false) => {
    el = el.trim();
    if (all) {
        return [...document.querySelectorAll(el)];
    } else {
        return document.querySelector(el);
    }
};

/**
 * Easy selector helper function
 */
const selectChild = (parent, el, all = false) => {
    if (!parent) {
        return false;
    }
    el = el.trim();
    if (all) {
        return [...parent.querySelectorAll(el)];
    } else {
        return parent.querySelector(el);
    }
};

/**
 * Easy selector with callback helper function
 */
const selectCallback = (parent, el, callback, all = false) => {
    if (!parent) {
        return false;
    }
    el = el.trim();
    if (all) {
        let elm = [...parent.querySelectorAll(el)];
        if (elm) {
            elm.forEach(callback);
        }
    } else {
        let elm = parent.querySelector(el);
        if (elm) {
            callback(elm);
        }
    }
};

/**
 * Easy event listener function
 */
const onKum = (type, el, listener, all = false) => {
    let elm = isElement(el) || Array.isArray(el) ? el : select(el, all);
    if (elm) {
        if (all) {
            elm.forEach(e => e.addEventListener(type, listener));
        } else {
            elm.addEventListener(type, listener);
        }
    }
};

let isElement = $obj => {
    try {
        return $obj.constructor.__proto__.prototype.constructor.name ? true : false;
    } catch (e) {
        return false;
    }
};

function IMDebounce(func, wait, immediate) {
    let timeout;
    return function() {
        let context = this,
            args = arguments;
        let later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

function IMThrottle(callback, limit) {
    var waiting = false;
    return function() {
        if (!waiting) {
            callback.apply(this, arguments);
            waiting = true;
            setTimeout(function() {
                waiting = false;
            }, limit);
        }
    };
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    let expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
    let name = cname + '=';
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return '';
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
