(function (Khayr) {
    Khayr = Khayr || [];

    // contactForm
    var KhayrContact = function KhayrContact() {

        const checkInvalid = (root) => {
            var elm = root.querySelectorAll('.form-group .wpcf7-form-control-wrap');
            if (elm) {
                elm.forEach(element => {
                    let valid = element.querySelectorAll('.wpcf7-not-valid-tip');
                    if (valid) {
                        element.classList.add('invalid');
                    } else {
                        element.classList.remove('invalid');
                    }

                    let inputs = element.querySelectorAll('input, select, textarea');
                    if (inputs) {
                        inputs.forEach(input => {
                            if (input.value == '') {
                                input.closest('.form-group').classList.remove('focused');
                            }
                        });
                    }
                });

                elm.each(function () {
                    var a = $(this);
                    if (a.querySelector('.wpcf7-not-valid-tip').length > 0) {
                        a.addClass('invalid');
                    } else {
                        a.removeClass('invalid');
                    }
                    if (a.querySelector('input, select, textarea').val() === '') {
                        a.querySelector('input, select, textarea').parents('.form-group').removeClass('focused');
                    }
                });
            }

            let invalid = root.querySelectorAll('.form-group .wpcf7-form-control-wrap.invalid');
            if (invalid) {
                invalid[0].querySelector('input, select, textarea').focus();
            }
        }

        const placeholder = (elm) => {
            if (elm) {
                let inputs = elm.querySelectorAll('.form-group input:not([type="submit"]), .form-group select, .form-group textarea');
                if (inputs) {
                    inputs.forEach(input => {
                        let parent = input.closest('.form-group');
                        if (parent) {
                            if (input.value !== '') {
                                parent.classList.add('focused');
                            }
                        }

                        input.addEventListener('focus', () => {
                            let parent = input.closest('.form-group');
                            if (parent) {
                                parent.classList.add('focused');
                            }
                        });
                        input.addEventListener('blur', () => {
                            let parent = input.closest('.form-group');
                            if (parent) {
                                if (input.value == '') {
                                    parent.classList.remove('focused');
                                }
                            }
                        });
                    });
                }
            }
        };

        const contactForm7 = (elm) => {
            var wpcf7Elm = elm.querySelector('.wpcf7');
            if (!wpcf7Elm) {
                return;
            }
            wpcf7Elm.addEventListener('wpcf7invalid', function (event) {
                checkInvalid(wpcf7Elm);
            }, false);
            wpcf7Elm.addEventListener('wpcf7spam', function (event) {
                checkInvalid(wpcf7Elm);
            }, false);
            wpcf7Elm.addEventListener('wpcf7mailsent', function (event) {
                checkInvalid(wpcf7Elm);
            }, false);
            wpcf7Elm.addEventListener('wpcf7mailfailed', function (event) {
                checkInvalid(wpcf7Elm);
            }, false);
            wpcf7Elm.addEventListener('wpcf7submit', function (event) {
                checkInvalid(wpcf7Elm);
            }, false);
        };

        // init
        let elms = document.querySelectorAll('.module-contact, .module-contact-newsletter');
        if (elms) {
            elms.forEach(element => {
                contactForm7(element);
                placeholder(element);
            });
        }

    };

    Khayr.contactForm = Khayr.contactForm || new KhayrContact();
})(Khayr);

class customSelect {
    constructor() {
        this.elements = [];
        this.count = 0;
    }

    init() {
        this.initChoices();
        this.reinitChoicesAfterGFSubmit();
    }

    getInstances() {
        return window.choices;
    }

    reinitChoicesAfterGFSubmit() {
        const selectOptions = document.querySelectorAll('select.form-control.jb-choices');

        if (!document.body.contains(selectOptions[0])) {
            return;
        }

        jQuery(document).on('gform_post_render', () => {
            const choicesSelect = document.querySelectorAll('.ginput_container_select');

            choicesSelect.forEach(select => {
                if (!select.firstElementChild.classList.contains('choices')) {
                    this.initChoices();
                }
            });
        });
    }

    initChoices() {
        const selectOptions = document.querySelectorAll('select.form-control.choices-style');

        if (!document.body.contains(selectOptions[0])) {
            return;
        }

        selectOptions.forEach(el => {
            el.dataset.choiceKey = this.count;
            let choice = new Choices(el, {
                searchEnabled: false,
                itemSelectText: '',
                shouldSort: false,
                placeholder: true,
                callbackOnInit: () => {
                    const onChoiceInit = new CustomEvent('on-choices-init', {
                        bubbles: true
                    });

                    setTimeout(() => {
                        el.dispatchEvent(onChoiceInit);
                    }, 1);
                }
            });

            this.elements[this.count] = choice;

            choice.passedElement.element.addEventListener('choice', event => {
                const onChoiceSelect = new CustomEvent('on-choices-select', {
                    bubbles: true,
                    detail: {
                        value: event.detail.choice.value
                    }
                });

                setTimeout(() => {
                    el.dispatchEvent(onChoiceSelect);
                }, 1);
            });

            this.count++;
        });

        window.choices = this.elements;
    }
}

let customDropdown = new customSelect();
customDropdown.init();
