<?php
/**
 * Khayr functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Khayr
 */


/**
*	---------------------------------------------------------------------------------------
*	Include Autoloader & Define Class
*	---------------------------------------------------------------------------------------
*/

/**
*	---------------------------------------------------------------------------------------
*	Define constants
*	---------------------------------------------------------------------------------------
*/
define( 'KHAYR_BASE_NAME', 'Khayr' );
define( 'KHAYR_BASE_SLUG', 'kumo' );
define( 'KHAYR_BASE_VERSION', '1.0' );
// define( 'KHAYR_FRAMEWORK', get_template_directory() . '/src/Khayr/' );
// define( 'KHAYR_WIDGETS', get_template_directory() . '/inc/widgets/' );
define( 'KHAYR_BASE', get_template_directory() );
define( 'KHAYR_BASE_INC', get_template_directory() . '/inc/' );
define( 'KHAYR_BASE_CPT', get_template_directory() . '/inc/cpt/' );
define( 'KHAYR_BASE_METABOX', get_template_directory() . '/inc/metabox/' );
define( 'KHAYR_BASE_WIDGET', get_template_directory() . '/inc/widgets/' );
define( 'KHAYR_BASE_OPTIONS', get_template_directory() . '/inc/options/' );
define( 'KHAYR_BASE_CLASS', get_template_directory() . '/inc/class/' );
define( 'KHAYR_BASE_FUNCTION', get_template_directory() . '/inc/functions/' );
define( 'KHAYR_BASE_LIBS', get_template_directory() . '/inc/libs/' );
define( 'KHAYR_BASE_COMPONENT', get_template_directory() . '/component/' );
define( 'KHAYR_BASE_TEMPLATE_PARTS', get_template_directory() . '/template-parts/' );
define( 'KHAYR_BASE_TEMPLATE', get_template_directory() . '/templates/' );
define( 'KHAYR_BASE_ICON', get_template_directory_uri() . '/assets/icon/' );
define( 'KHAYR_BASE_IMAGE', get_template_directory_uri() . '/assets/img/' );

// include helper required file
require_once( KHAYR_BASE_FUNCTION . 'helpers-functions.php');


/**
*	----------------------------------------------------------------------------------------
*	Set up theme default and register framework
*	----------------------------------------------------------------------------------------
*/
if ( ! function_exists( 'khayr_setup' ) ) :
	function khayr_setup() {
		/* add title tag support */
		add_theme_support( 'title-tag' );

		/* Load child theme languages */
		load_theme_textdomain( 'kumo', get_stylesheet_directory() . '/languages' );

		/* load theme languages */
		load_theme_textdomain( 'kumo', get_template_directory() . '/languages' );

		/* Add default posts and comments RSS feed links to head */
		add_theme_support( 'automatic-feed-links' );

		/* Add support for post thumbnails. */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption'
			)
		);

		/* remove gallery style css */
		add_filter( 'use_default_gallery_style', '__return_false' );

		/* Support for elementor header and footer */
		if ( class_exists( 'Header_Footer_Elementor' ) ) {
			add_theme_support( 'header-footer-elementor' );
		}
		
		// Register CPT
		require_once( KHAYR_BASE_CPT . 'pt-review.php');
		require_once( KHAYR_BASE_CPT . 'pt-services.php');
		require_once( KHAYR_BASE_CPT . 'pt-testimonial.php');
		require_once( KHAYR_BASE_CPT . 'pt-template.php');

		// Register MB
		require_once( KHAYR_BASE_METABOX . 'mb-post.php');
		require_once( KHAYR_BASE_METABOX . 'mb-services.php');
		require_once( KHAYR_BASE_METABOX . 'mb-testimonial.php');
		require_once( KHAYR_BASE_METABOX . 'mb-review.php');

		// Register Options
		require_once( KHAYR_BASE_OPTIONS . 'opt-general.php');

		/* Framework Setup */
		if (class_exists('KhayrCore')) {
			new KhayrCore();
		}

		// additional themes require
		require_once( KHAYR_BASE_CLASS . 'class-plugins-setup.php');
		require_once( KHAYR_BASE_CLASS . 'class-menus.php');
		require_once( KHAYR_BASE_CLASS . 'class-so-widget.php');
		require_once( KHAYR_BASE_CLASS . 'class-assets-loader.php');
		require_once( KHAYR_BASE_COMPONENT . 'ajax/class-ajax.php');
		
	}
endif;
add_action( 'after_setup_theme', 'khayr_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function khayr_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'khayr_content_width', 1170 );
}
add_action( 'after_setup_theme', 'khayr_content_width', 0 );


function wpmm_enqueue_css_files($styles) {
    $styles['file'] = 'https://kumostudio.co/wp-content/themes/kumo-studio/wp-maintenance-mode/fonts/stylesheet.css';
    $styles['file2'] = 'https://kumostudio.co/wp-content/themes/kumo-studio/wp-maintenance-mode/dist/main.css';

    return $styles;
}

add_filter('wpmm_styles', 'wpmm_enqueue_css_files', 10, 1);

function wpmm_enqueue_javascript_files($scripts) {
    $scripts['file'] = 'https://kumostudio.co/wp-content/themes/kumo-studio/wp-maintenance-mode/dist/manifest.js';
    $scripts['file2'] = 'https://kumostudio.co/wp-content/themes/kumo-studio/wp-maintenance-mode/dist/vendor.js';
	$scripts['file3'] = 'https://kumostudio.co/wp-content/themes/kumo-studio/wp-maintenance-mode/dist/app.js';

    return $scripts;
}

add_filter('wpmm_scripts', 'wpmm_enqueue_javascript_files', 10, 1);