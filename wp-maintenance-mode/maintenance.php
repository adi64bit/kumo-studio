<?php
/**
 * Maintenance mode page
 *
 * This template can be overridden by copying it to one of these paths:
 * - /wp-content/themes/{your_child_theme}/wp-maintenance-mode/maintenance.php
 * - /wp-content/themes/{your_theme}/wp-maintenance-mode/maintenance.php
 * - /wp-content/themes/{your_child_theme}/wp-maintenance-mode.php [deprecated]
 * - /wp-content/themes/{your_theme}/wp-maintenance-mode.php [deprecated]
 * - /wp-content/wp-maintenance-mode.php
 *
 * It can also be overridden by changing the default path. See `wpmm_maintenance_template` hook:
 * https://github.com/WP-Maintenance-Mode/Snippet-Library/blob/master/change-template-path.php
 *
 * @version 2.4.0
 */

defined( 'ABSPATH' ) || exit;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo esc_html( $title ); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="author" content="<?php echo esc_attr( $author ); ?>" />
		<meta name="description" content="<?php echo esc_attr( $description ); ?>" />
		<meta name="keywords" content="<?php echo esc_attr( $keywords ); ?>" />
		<meta name="robots" content="<?php echo esc_attr( $robots ); ?>" />
		<?php
		do_action( 'wm_head' ); // this hook will be removed in the next versions
		do_action( 'wpmm_head' );
		?>
	</head>

    <body class="<?php echo $body_classes ? esc_attr( $body_classes ) : ''; ?>">
    <div class="page">
        <div class="page__wrapper">
            <div class="page__text">
                <span class="page__text-span">
                    <span class="text-orange" style="opacity: 0;">Digital Marketing</span>
                    <span class="text-white" style="opacity: 0;">Creative Studio</span>
                </span>
            </div>
            <div class="page__logo">
                <div class="page__logo-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="445" height="249.744"
                        viewBox="0 0 445 249.744" style="opacity: 0;">
                        <g transform="translate(-403.334 -202.502)">
                            <path
                                d="M1097.667,401.333q-26.585-27.4-26.584-64.061t26.936-63.475q26.932-26.816,64.646-26.818t64.413,26.7q26.7,26.7,26.7,63.592t-26.467,64.177q-26.473,27.291-63.241,27.287h-3.513Q1124.249,428.737,1097.667,401.333Zm148.382-63.592q0-34.547-24.476-59.259t-59.026-24.712q-34.551,0-59.142,24.712t-24.593,59.259q0,34.551,24.593,59.376t59.025,24.828q34.433,0,59.026-24.828T1246.05,337.741Z"
                                transform="translate(-405.446 -27.005)" fill="#c5c7c3" />
                            <path
                                d="M957.7,400.275h9.359V202.5H954.009l-95.87,196.279L762.554,202.5H748.94V415.232h8.509V213.564L857.289,419.2,957.7,213.564Z"
                                transform="translate(-209.846 0)" fill="#326a8d" />
                            <path
                                d="M414.108,366.881l80.339-70.035h-9.369l-74.25,64.413V296.846h-7.495v138.6h7.729v-60.6l93.69,97.672h10.306Z"
                                transform="translate(0 -57.284)" fill="#326a8d" />
                            <path
                                d="M663.956,388.139V509.029q0,20.146-12.765,33.142t-31.738,13q-18.972,0-31.854-13.117t-12.883-33.025V383.2l-7.729,6.763v119.3q0,22.723,14.873,37.71t36.423,14.991h2.342a48.881,48.881,0,0,0,36.188-15.107q14.872-15.108,14.873-38.062V404.085Z"
                                transform="translate(-99.367 -109.716)" fill="#c5c7c3" />
                            <path
                                d="M1350.555,273.679q-26.7-26.7-64.413-26.7a99.612,99.612,0,0,0-11.7.677v6.851a90.2,90.2,0,0,1,11.581-.736q34.547,0,59.026,24.712t24.476,59.259q0,34.551-24.593,59.376t-59.026,24.828a89.286,89.286,0,0,1-11.464-.729v7.02a91.465,91.465,0,0,0,9.592.5h3.514q36.769,0,63.241-27.287t26.467-64.177Q1377.256,300.382,1350.555,273.679Z"
                                transform="translate(-528.923 -27.005)" fill="#c5c7c3" />
                        </g>
                    </svg>
                </div>

                <div class="page__logo-contact">
                    <ul class="page__logo-contact-lists">
                        <li class="page__logo-contact-list email cursor-link"
                            style="opacity: 0;">
                            <a href="mailto:doumo@kumostudio.co">
                                <span class="icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="448" height="448" viewBox="0 0 448 448">
                                        <path d="M416 376v-192c-5.25 6-11 11.5-17.25 16.5-35.75 27.5-71.75 55.5-106.5 84.5-18.75 15.75-42 35-68 35h-0.5c-26 0-49.25-19.25-68-35-34.75-29-70.75-57-106.5-84.5-6.25-5-12-10.5-17.25-16.5v192c0 4.25 3.75 8 8 8h368c4.25 0 8-3.75 8-8zM416 113.25c0-6.25 1.5-17.25-8-17.25h-368c-4.25 0-8 3.75-8 8 0 28.5 14.25 53.25 36.75 71 33.5 26.25 67 52.75 100.25 79.25 13.25 10.75 37.25 33.75 54.75 33.75h0.5c17.5 0 41.5-23 54.75-33.75 33.25-26.5 66.75-53 100.25-79.25 16.25-12.75 36.75-40.5 36.75-61.75zM448 104v272c0 22-18 40-40 40h-368c-22 0-40-18-40-40v-272c0-22 18-40 40-40h368c22 0 40 18 40 40z" fill="#f48841"></path>
                                    </svg>    
                                </span>doumo@kumostudio.co</a>
                        </li>
                        <li class="page__logo-contact-list wa cursor-link"
                            style="opacity: 0;">
                            <a href="https://wa.me/628998699777" target="_blank"><span class="icon"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="25.936"
                                        height="26" viewBox="0 0 25.936 26">
                                        <g transform="translate(0)">
                                            <path
                                                d="M4753.98,724.514l1.864-6.955a12.874,12.874,0,1,1,5.127,5.082Zm7.338-4.479.44.262a10.453,10.453,0,1,0-3.575-3.543l.27.444-1.04,3.883Z"
                                                transform="translate(-4753.98 -698.514)"
                                                fill="#f48841" />
                                            <path
                                                d="M4774.335,715.658a2.158,2.158,0,0,0-1.84-.414c-.478.2-.782.943-1.091,1.325a.457.457,0,0,1-.591.129,8.336,8.336,0,0,1-4.159-3.561.511.511,0,0,1,.065-.7,2.8,2.8,0,0,0,.754-1.226,2.666,2.666,0,0,0-.338-1.45,3.433,3.433,0,0,0-1.07-1.615,1.468,1.468,0,0,0-1.594.237,3.239,3.239,0,0,0-1.122,2.564,3.61,3.61,0,0,0,.1.813,6.783,6.783,0,0,0,.78,1.8,14.318,14.318,0,0,0,.815,1.239,12.582,12.582,0,0,0,3.535,3.282,10.682,10.682,0,0,0,2.2,1.048,4.9,4.9,0,0,0,2.565.4,3.1,3.1,0,0,0,2.326-1.73,1.516,1.516,0,0,0,.109-.883C4775.651,716.314,4774.827,715.952,4774.335,715.658Z"
                                                transform="translate(-4756.465 -701.031)"
                                                fill="#f48841" fill-rule="evenodd" />
                                        </g>
                                    </svg></span>+62 899 8699 777</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="square-cursor">
        <div class="square-cursor__inner"></div>
    </div>
    <script type='text/javascript'>
			var wpmm_vars = {"ajax_url": "<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>"};
		</script>

		<?php
		do_action( 'wm_footer' ); // this hook will be removed in the next versions
		do_action( 'wpmm_footer' );
		?>
	</body>
</html>
