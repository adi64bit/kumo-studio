<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Khayr
 */

get_header();
$terms = get_queried_object();

$data = [
	'params' => [
		'post_type'          => 'post', 
		'paging'             => isset($_GET['paging']) ? absint($_GET['paging']) : 1,
		'posts_per_page'     => isset($_GET['show']) ? $_GET['show'] : 5,
		'category'           => $terms->taxonomy == 'category' ? $terms->slug : '',
		'post_tag'           => $terms->taxonomy == 'post_tag' ? $terms->slug : '',
		'year'               => isset($_GET['year']) ? $_GET['year'] : '',
		'pagination'         => 'true'
	],
	'filter' => []
];


$response = new KhayrAjax( $data['params'] );
$year = $response->get_published_year();

if ($post_tag) {
	foreach ($year as $value) {
		$data['filter']['year']['items'][] = [
			'label' => $value,
			'value' => $value
		];
	}
}

$response = $response->get_posts();

$ajax = [
	'type'              => 'blog',
	'class'             => '',
	'data'              => $data,
	'posts'             => $response['posts'],
	'pagination'        => isset($response['pagination']) ? $response['pagination']: false,
	'loadmore'          => isset($response['loadmore']) ? $response['loadmore']: false,
	'infinite_scroll'   => isset($response['infinite_scroll']) ? $response['infinite_scroll']: false,
	'dump'              => $response
];

get_template_part( 'template-parts/archive-title', null, $terms );
get_template_part( 'component/ajax/ajax', null, $ajax );
get_footer();
