<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Khayr
 */

get_header();

$data = [
	'params' => [
		'post_type'          => 'post', 
		'paging'             => isset($_GET['paging']) ? absint($_GET['paging']) : 1,
		'posts_per_page'     => isset($_GET['show']) ? $_GET['show'] : 5,
		'category'           => isset($_GET['category']) ? $_GET['category'] : '',
		'post_tag'           => isset($_GET['post_tag']) ? $_GET['post_tag'] : '',
		'year'               => isset($_GET['year']) ? $_GET['year'] : '',
		'pagination'         => 'true'
	],
	'filter' => [
		'categories'	=> [
			'label' => 'Category',
			'items' => []
		],
		// 'post_tag'	=> [
		// 	'label' => 'Tag',
		// 	'items' => []
		// ],
		'year'	=> [
			'label' => 'Year',
			'items' => []
		]
	]
];

$featured = Khayr_get_featured_post();

if($featured){
	$data['params']['featured'] = $featured->ID;
}

$category = get_terms( [
	'taxonomy'   => 'category',
	'hide_empty' => true,
	'exclude'    => array(1)
] );

if ($category) {
	foreach ($category as $key => $value) {
		$data['filter']['categories']['items'][] = [
			'label' => $value->name,
			'value' => $value->slug
		];
	}
}

// $post_tag = get_terms( [
// 	'taxonomy'   => 'post_tag',
// 	'hide_empty' => true,
// 	'exclude'    => array(1)
// ] );

// if ($post_tag) {
// 	foreach ($post_tag as $key => $value) {
// 		$data['filter']['post_tag']['items'][] = [
// 			'label' => $value->name,
// 			'value' => $value->slug
// 		];
// 	}
// }

$response = new KhayrAjax( $data['params'] );
$year = $response->get_published_year();

if ($year) {
	foreach ($year as $value) {
		$data['filter']['year']['items'][] = [
			'label' => $value,
			'value' => $value
		];
	}
}

$response = $response->get_posts();

$ajax = [
	'type'              => 'blog',
	'class'             => '',
	'data'              => $data,
	'posts'             => $response['posts'],
	'pagination'        => isset($response['pagination']) ? $response['pagination']: false,
	'loadmore'          => isset($response['loadmore']) ? $response['loadmore']: false,
	'infinite_scroll'   => isset($response['infinite_scroll']) ? $response['infinite_scroll']: false,
	'dump'              => $response
];

get_template_part( 'component/ajax/ajax', null, $ajax );
get_footer();
