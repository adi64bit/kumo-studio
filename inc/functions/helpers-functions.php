<?php

if( !function_exists('khayr_is_ajax_request') ) {
    function khayr_is_ajax_request() {
        if ( ! empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) {
            return true;
        }
        return false;
    }
}
    
/* --------------------------------------------------------------------------
* Get Image Ratio
---------------------------------------------------------------------------*/
if (!function_exists('khayr_get_image_ratio')) {
    function khayr_get_image_ratio($image_id, $type = 'image'){

        if ($type == 'square') {
            $ratio = 100;
        } elseif ($type == 'landscape') {
            $ratio = 70;
        } elseif ($type == 'portrait') {
            $ratio = 130;
        } else {
            $image = wp_get_attachment_image_src( $image_id, 'full' );
            $ratio = $image[2] / $image[1] * 100;
        }
        
        return '<i style="padding-bottom: '. $ratio .'%; display: block"></i>';
    }
}
    
/* --------------------------------------------------------------------------
* Get Image
---------------------------------------------------------------------------*/
if (!function_exists('khayr_get_image')) {
    function khayr_get_image($id = false, $size = 'full', $attr = array()) {
        if (!$id) {
            return;
        }
        
        $attr = shortcode_atts(array(
            'type' => 'html',
            'alt' => false,
            'srcset' => false,
            'lazyload' => true,
            'class' => false,
            'attr' => array(),
            'after' => '',
            'before' => '',
            'placeholder' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='
        ), $attr, false);
        
        $image = wp_get_attachment_image_src($id, $size);
        if(!$image){
            return;
        }
        
        if($attr['type'] == 'html' && function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()){
            $attr['lazyload'] = false;
        }
        
        if($attr['type'] == 'html' && function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {
            $attr['lazyload'] = false;
        }
        
        if(!is_array($attr['attr'])){
            $attr['attr'] = array();
        }
        
        $attr['attr']['class'] = 'image-'.$size.' ';
        $srcset = $attr['srcset'] ? wp_get_attachment_image_srcset($id, $size) : false;
        if($attr['lazyload']){
            $attr['attr']['class'] .= 'lazyload ';
            if($srcset){
                $attr['attr']['data-sizes'] = 'auto';
                $attr['attr']['data-srcset'] = $srcset;
                $attr['attr']['data-size'] = $image[1].'x'.$image[2];
                $attr['attr']['src'] = $attr['placeholder'];
            } else {
                $attr['attr']['data-src'] = $image[0];
                $attr['attr']['data-size'] = $image[1].'x'.$image[2];
                $attr['attr']['src'] = $attr['placeholder'];
            }
        } else {
            if($srcset){
                $attr['attr']['sizes'] = wp_get_attachment_image_sizes($id, $size);
                $attr['attr']['srcset'] = $srcset;
                $attr['attr']['data-size'] = $image[1].'x'.$image[2];
                $attr['attr']['src'] = $image[0];
            } else {
                $attr['attr']['data-size'] = $image[1].'x'.$image[2];
                $attr['attr']['src'] = $image[0];
            }
        }
        
        
        if($attr['class'] && is_array($attr['class'])){
            foreach ($attr['class'] as $key => $value) {
                $attr['attr']['class'] .= $value.' ';
            }
        }
        
        $attr['attr']['alt'] = $attr['alt'] ? $attr['alt'] : get_post_meta($id, '_wp_attachment_image_alt', true);
        
        $attribute = '';
        if($attr['attr'] && is_array($attr['attr'])){
            foreach ($attr['attr'] as $key => $value) {
                $attribute .= $key.'="'.htmlentities($value).'" ';
            }
        }
        
        if($attr['type'] == 'html'){
            return $attr['before'].'<img '.$attribute.'>'.$attr['after'];
        }
        
        $result = array(
            'id'  => $id,
            'image' => $image,
            'attr' => $attr['attr'],
        );
        if ($srcset) {
            $result['srcset'] = $srcset;
        }
        return $result;
    }
}

/* --------------------------------------------------------------------------
* Get Options
---------------------------------------------------------------------------*/
if ( ! function_exists( 'khayr_option' ) ) {
	function khayr_option( $id, $fallback = false, $param = false ) {
		if ( isset( $_GET['khayr_'.$id] ) ) {
			if ( '-1' == $_GET['khayr_'.$id] ) {
				return false;
			} else {
				return $_GET['khayr_'.$id];
			}
		} else {
			global $khayr_options;
			if ( $fallback == false ) $fallback = '';
			$output = ( isset($khayr_options[$id]) && $khayr_options[$id] !== '' ) ? $khayr_options[$id] : $fallback;
			if ( !empty($khayr_options[$id]) && $param ) {
				$output = $khayr_options[$id][$param];
			}
		}
		return $output;
	}
}

if (!function_exists('khayr_is_external_content')) {
    function khayr_is_external_content( $url ) {
        $is_external = khayr_is_absolute($url) && !khayr_is_internal_content($url);
        return $is_external;
    }
}

if (!function_exists('khayr_is_internal_content')) {
    function khayr_is_internal_content( $url ) {
        // using content_url() instead of site_url or home_url is IMPORTANT
        // otherwise you run into errors with sites that:
        // 1. use WPML plugin
        // 2. or redefine content directory
        $is_content_url = strstr($url, content_url());

        // this case covers when the upload directory has been redefined
        $upload_dir = wp_upload_dir();
        $is_upload_url = strstr($url, $upload_dir['baseurl']);

        return $is_content_url || $is_upload_url;
    }
}

if (!function_exists('khayr_is_absolute')) {
    function khayr_is_absolute( $path ) {
        return (boolean) (strstr($path, 'http'));
    }
}


/* --------------------------------------------------------------------------
* Register Metabox
---------------------------------------------------------------------------*/
if ( ! function_exists( 'khayr_register_metabox' ) ) {
	function khayr_register_metabox( $id, $config, $options = array() ) {
		$default = array(
            'id'            => $id,
            'title'         => 'Test Metabox',
            'object_types'  => array( 'page' )
        );

        $config = array_merge($default, $config);        

        add_filter('khayr_metabox_register', function($conf) use ( $config, $options ){
            $conf[$config['id']] = array(
                'config'    => $config,
                'options'   => $options
            );
            return $conf;
        });
	}
}

if ( ! function_exists( 'khayr_register_options' ) ) {
	function khayr_register_options( $id, $config, $options = array() ) {
		$default = array(
            'id'            => $id,
            'title'         => 'Test Metabox',
            'object_types'  => array( 'options-page' )
        );

        $config = array_merge($default, $config);        

        add_filter('khayr_options_register', function($conf) use ( $config, $options ){
            $conf[$config['id']] = array(
                'config'    => $config,
                'options'   => $options
            );
            return $conf;
        });
	}
}

/* --------------------------------------------------------------------------
* Get Metabox Options
---------------------------------------------------------------------------*/
function khayr_option($key = '', $group = 'options', $default = false ) {
	if ( function_exists( 'cmb2_get_option' ) ) {
		// Use cmb2_get_option as it passes through some key filters.
		return cmb2_get_option( 'khayr_'.$group, $key, $default );
	}

	// Fallback to get_option if CMB2 is not loaded yet.
	$opts = get_option( 'khayr_'.$group, $default );

	$val = $default;

	if ( 'all' == $key ) {
		$val = $opts;
	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
		$val = $opts[ $key ];
	}

	return $val;
}

/* --------------------------------------------------------------------------
* Register CPT
---------------------------------------------------------------------------*/
if ( ! function_exists( 'khayr_register_cpt' ) ) {
	function khayr_register_cpt($config) {
		$default = array(
            'postType'      => false,
            'postName'     => 'Post Name',
            'pluralName'   => 'Posts Name',
            'singularName'  => 'Post Name',
            'active' => true,
            'hierarchical' => false,
            'public' => true,
        );

        $config = array_merge($default, $config);        

        add_filter('khayr_cpt_register', function($conf) use ( $config){
            $conf[] = $config;
            return $conf;
        });
	}
}

if(!function_exists('khayr_scroll')){
    function khayr_scroll($delay = '0s', $duration = '0.6s', $animation = 'slide-up'){
        $animate = sprintf('data-sal="%s" style="--sal-duration: %s; --sal-delay: %s; --sal-easing: cubic-bezier(0.45,0.05,0.55,0.95);"', $animation, $duration, $delay);
        return $animate;
    }
}

function Khayr_get_featured_post(){
    $response = array();

    $args = [
        'post_type'         => 'post',
        'post_status'       => 'publish',
        'posts_per_page'    => -1,
        'paged'             => 1
    ];

    $query_latest = new WP_Query($args);
    $latest = $query_latest->have_posts() ? $query_latest->get_posts()[0] : false;

    $args['meta_query'] = array(
        array(
            'key' => 'set_as_featured',
            'value' => 'on',
            'compare' => '=='
        )
    );

    $query = new WP_Query($args);

    return $query->have_posts() ? $query->get_posts()[0] : $latest;
}

function current_year_shortcode() {
    $current_year = date('Y');
    return $current_year;
}
add_shortcode('current_year', 'current_year_shortcode');
add_shortcode('year', 'current_year_shortcode');

if (!function_exists('khayr_get_contact_form_7')){
    function khayr_get_contact_form_7(){
        $args = array(
            'post_type' => 'wpcf7_contact_form',
            'posts_per_page' => -1
        );
        $query = new WP_Query($args);
        $forms = array();
        if($query->have_posts()){
            foreach ($query->get_posts() as $key => $value) {
                $forms[$value->ID] = $value->post_title;
            }
        }
        return $forms;
    }
}