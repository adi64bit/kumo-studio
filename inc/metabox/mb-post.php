<?php
if (function_exists('khayr_register_metabox')) {
    khayr_register_metabox(
        'khayr_post',
        array(
            'title'         => esc_html__( 'Post Option', 'kumo' ),
            'object_types'  => array('post'),
            'show_on_cb'    => 'khayr_show_if_front_page',
            'context'       => 'normal',
            'priority'      => 'high',
        ),
        array(
            array(
                'name' => esc_html__( 'Set as Featured Blog', 'kumo' ),
                'desc' => esc_html__( 'set as featured blog', 'kumo' ),
                'id'   => 'set_as_featured',
                'type' => 'checkbox',
            ),
            // hide default CTA (below before footer)
            array(
                'name' => esc_html__( 'Hide Default CTA', 'kumo' ),
                'desc' => esc_html__( 'hide default CTA', 'kumo' ),
                'id'   => 'hide_default_cta',
                'type' => 'checkbox',
            ),
        )
    );
}
