<?php
if (function_exists('khayr_register_metabox')) {
    khayr_register_metabox(
        'khayr_testimonial',
        array(
            'title'         => esc_html__( 'Testimonial', 'kumo' ),
            'object_types'  => array('testimonial'),
            'show_on_cb'    => 'khayr_show_if_front_page',
            'context'       => 'normal',
            'priority'      => 'high',
        ),
        array(
            array(
                'name' => esc_html__( 'Hashtag / Label', 'kumo' ),
                'desc' => esc_html__( 'hashtag/label above the title', 'kumo' ),
                'id'   => 'label',
                'type' => 'text',
            ),
            array(
                'name' => esc_html__( 'Description', 'kumo' ),
                'desc' => esc_html__( 'short desription of the testimonials', 'kumo' ),
                'id'   => 'hashtag',
                'type' => 'textarea',
            )
        )
    );
}
