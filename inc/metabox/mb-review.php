<?php
if (function_exists('khayr_register_metabox')) {
    khayr_register_metabox(
        'khayr_reviw',
        array(
            'title'         => esc_html__( 'review', 'kumo' ),
            'object_types'  => array('review'),
            'show_on_cb'    => 'khayr_show_if_front_page',
            'context'       => 'normal',
            'priority'      => 'high',
        ),
        array(
            array(
                'name' => esc_html__( 'Label', 'kumo' ),
                'desc' => esc_html__( 'Label', 'kumo' ),
                'id'   => 'label',
                'type' => 'text',
            ),
            // source
            array(
                'name' => esc_html__( 'Source', 'kumo' ),
                'desc' => esc_html__( 'Source', 'kumo' ),
                'id'   => 'source',
                'type' => 'text',
            ),
            array(
                'name' => esc_html__( 'Description', 'kumo' ),
                'desc' => esc_html__( 'short desription of the reviews', 'kumo' ),
                'id'   => 'hashtag',
                'type' => 'textarea',
            ),
        )
    );
    khayr_register_metabox(
        'khayr_review_category',
        array(
            'title'         => esc_html__( 'review', 'kumo' ),
            'object_types'     => array( 'term' ), // Tells CMB2 to use term_meta vs post_meta
            'taxonomies'       => array( 'review_category' ),
            'show_on_cb'    => 'khayr_show_if_front_page',
            'context'       => 'normal',
            'priority'      => 'high',
        ),
        array(
            // color
            array(
                'name' => esc_html__( 'Color', 'kumo' ),
                'desc' => esc_html__( 'Color', 'kumo' ),
                'id'   => 'color',
                'type' => 'colorpicker',
            ),
        )
    );
}
