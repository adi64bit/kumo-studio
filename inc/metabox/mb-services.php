<?php
if (function_exists('khayr_register_metabox')) {
    khayr_register_metabox(
        'khayr_service',
        array(
            'title'         => esc_html__( 'Service', 'kumo' ),
            'object_types'  => array('service'),
            'show_on_cb'    => 'khayr_show_if_front_page',
            'context'       => 'normal',
            'priority'      => 'high',
        ),
        array(
            array(
                'name' => esc_html__( 'Hashtag / Label', 'kumo' ),
                'desc' => esc_html__( 'hashtag/label above the title', 'kumo' ),
                'id'   => 'label',
                'type' => 'text',
            ),
            array(
                'name' => esc_html__( 'Description', 'kumo' ),
                'desc' => esc_html__( 'short desription of the services', 'kumo' ),
                'id'   => 'hashtag',
                'type' => 'textarea',
            ),
            // link
            array(
                'name' => esc_html__( 'Link', 'kumo' ),
                'desc' => esc_html__( 'link to the service page', 'kumo' ),
                'id'   => 'link',
                'type' => 'text',
            ),
            // link text default read more
            array(
                'name' => esc_html__( 'Link Text', 'kumo' ),
                'desc' => esc_html__( 'text of the link', 'kumo' ),
                'id'   => 'link_text',
                'type' => 'text',
            ),
        )
    );
}
