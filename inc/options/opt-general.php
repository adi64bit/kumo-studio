<?php
if (function_exists('khayr_register_options')) {

    khayr_register_options(
        'khayr_options',
        array(
            'title'         => esc_html__( 'General', 'kumo' ),
            'object_types'  => array( 'options-page' ),
            'menu_title'    => esc_html__( 'Theme Options', 'kumo' ),
            'option_key'    => 'khayr_options',
            'tab_group'     => 'khayr_options',
            'tab_title'     => 'General',
        ),
        array(
            // Header
            array(
                'id'          => 'header',
                'type'        => 'group',
                'repeatable'  => false,
                'options'     => array(
                    'group_title'       => esc_html__( 'Header', 'khayr' ), // since version 1.1.4, {#} gets replaced by row number
                ),
                'fields'      => array(
                    array(
                        'name'    => esc_html__( 'Logo', 'khayr' ),
                        'desc'    => esc_html__( 'Upload an image to set as a logo.', 'khayr' ),
                        'id'      => 'logo',
                        'type'    => 'file',
                        'options' => array(
                            'url' => false
                        ),
                        'text'    => array(
                            'add_upload_file_text' => esc_html__( 'Add Logo', 'khayr' )
                        ),
                        'query_args' => array(
                            'type' => array(
                                'image/gif',
                                'image/jpeg',
                                'image/png',
                            )
                        ),
                        'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
                    )
                )
            ),

            // Footer
            array(
                'id'          => 'footer',
                'type'        => 'group',
                'repeatable'  => false,
                'options'     => array(
                    'group_title'       => esc_html__( 'Footer', 'khayr' ), // since version 1.1.4, {#} gets replaced by row number
                ),
                'fields'      => array(
                    array(
                        'name'    => esc_html__( 'Logo', 'khayr' ),
                        'desc'    => esc_html__( 'Upload an image to set as a logo.', 'khayr' ),
                        'id'      => 'logo',
                        'type'    => 'file',
                        'options' => array(
                            'url' => false
                        ),
                        'text'    => array(
                            'add_upload_file_text' => esc_html__( 'Add Logo', 'khayr' )
                        ),
                        'query_args' => array(
                            'type' => array(
                                'image/gif',
                                'image/jpeg',
                                'image/png',
                            )
                        ),
                        'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
                    ),
                    array(
                        'name' => esc_html__( 'Address', 'khayr' ),
                        'desc' => esc_html__( 'website address', 'khayr' ),
                        'default' => 'Jl. Trenggana Gang IV No. 2, Penatih, Denpasar Timur, Bali 80238 Indonesia',
                        'id' => 'address',
                        'type' => 'textarea'
                    ),
                    array(
                        'name' => esc_html__( 'Copyright', 'khayr' ),
                        'desc' => esc_html__( 'copyright content.', 'khayr' ),
                        'default' => '©[year] KUMO Studio. All Rights Reserved.',
                        'id' => 'copyright',
                        'type' => 'text'
                    )
                )
            ),

            // Footer
            array(
                'id'          => 'single_blog',
                'type'        => 'group',
                'repeatable'  => false,
                'options'     => array(
                    'group_title'       => esc_html__( 'Single Blog', 'khayr' ), // since version 1.1.4, {#} gets replaced by row number
                ),
                'fields'      => array(
                    // true/false field
                    array(
                        'name' => esc_html__( 'Show CTA', 'khayr' ),
                        'id'   => 'show_cta',
                        'type' => 'checkbox',
                        'desc' => esc_html__( 'Show CTA', 'khayr' ),
                        'default' => true
                    ),
                    // if show_cta true show fields (text, text link 1, text link 2)
                    array(
                        'name' => esc_html__( 'CTA Text', 'khayr' ),
                        'id'   => 'cta_text',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Text', 'khayr' ),
                        'default' => 'Ready to bring your goals to life?',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Sub Text', 'khayr' ),
                        'id'   => 'cta_sub_text',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Sub Text', 'khayr' ),
                        'default' => '',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link Icon 1', 'khayr' ),
                        'id'   => 'cta_link_icon_1',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link Icon 1', 'khayr' ),
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link Text 1', 'khayr' ),
                        'id'   => 'cta_link_text_1',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link Text 1', 'khayr' ),
                        'default' => 'doumo@kumostudio.co',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link URL 1', 'khayr' ),
                        'id'   => 'cta_link_url_1',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link URL 1', 'khayr' ),
                        'default' => 'mailto:doumo@kumostudio.co',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link Icon 2', 'khayr' ),
                        'id'   => 'cta_link_icon_2',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link Icon 2', 'khayr' ),
                        'default' => 'wa',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link Text 2', 'khayr' ),
                        'id'   => 'cta_link_text_2',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link Text 2', 'khayr' ),
                        'default' => '+62 899 8699 777',
                    ),
                    array(
                        'name' => esc_html__( 'CTA Link URL 2', 'khayr' ),
                        'id'   => 'cta_link_url_2',
                        'type' => 'text',
                        'desc' => esc_html__( 'CTA Link URL 2', 'khayr' ),
                        'default' => 'https://wa.me/628998699777',
                    ),
                    array(
                        'name' => esc_html__( 'Show Newsletter', 'khayr' ),
                        'id'   => 'show_newsletter',
                        'type' => 'checkbox',
                        'desc' => esc_html__( 'Show Newsletter', 'khayr' ),
                        'default' => true
                    ),
                    // select field data from contact form 7
                    array(
                        'name' => esc_html__( 'Newsletter Form', 'khayr' ),
                        'id'   => 'cta_form',
                        'type' => 'text',
                        'desc' => esc_html__( 'Newsletter Form', 'khayr' ),
                    ),
                    // contact image
                    array(
                        'name'    => esc_html__( 'CTA Image', 'khayr' ),
                        'desc'    => esc_html__( 'Upload an image to set as a contact image.', 'khayr' ),
                        'id'      => 'cta_image',
                        'type'    => 'file',
                        'options' => array(
                            'url' => false
                        ),
                        'text'    => array(
                            'add_upload_file_text' => esc_html__( 'Add Image', 'khayr' )
                        ),
                        'query_args' => array(
                            'type' => array(
                                'image/gif',
                                'image/jpeg',
                                'image/png',
                            )
                        ),
                        'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
                    ),
                )
            ),

            // Social Media
            array(
                'id'          => 'social_media',
                'type'        => 'group',
                'repeatable'  => false,
                'options'     => array(
                    'group_title'       => esc_html__( 'Social Media', 'khayr' ), // since version 1.1.4, {#} gets replaced by row number
                ),
                'fields'      => array(
                    array(
                        'name' => esc_html__( 'Whatsapp URL', 'khayr' ),
                        'id'   => 'whatsapp',
                        'type' => 'text_url'
                    ),
                    array(
                        'name' => esc_html__( 'Email URL', 'khayr' ),
                        'id'   => 'email',
                        'type' => 'text_url'
                    ),
                    array(
                        'name' => esc_html__( 'Facebook URL', 'khayr' ),
                        'id'   => 'facebook',
                        'type' => 'text_url'
                    ),
                    array(
                        'name' => esc_html__( 'Instagram URL', 'khayr' ),
                        'id'   => 'instagram',
                        'type' => 'text_url'
                    )
                )
            ),

            // Graphic Elements
            array(
                'id'          => 'graphic_elements',
                'type'        => 'group',
                'repeatable'  => true,
                'options'     => array(
                    'group_title'       => esc_html__( 'Graphic Elements', 'khayr' ), // since version 1.1.4, {#} gets replaced by row number
                ),
                'fields'      => array(
                    array(
                        'name'    => esc_html__( 'Element', 'khayr' ),
                        'desc'    => esc_html__( 'Upload an image to set as a element.', 'khayr' ),
                        'id'      => 'element',
                        'type'    => 'file',
                        'options' => array(
                            'url' => false
                        ),
                        'text'    => array(
                            'add_upload_file_text' => esc_html__( 'Add Logo', 'khayr' )
                        ),
                        'query_args' => array(
                            'type' => array(
                                'image/gif',
                                'image/jpeg',
                                'image/png',
                                'image/svg+xml'
                            )
                        ),
                        'preview_size' => 'thumbnail', // Image size to use when previewing in the admin.
                    ),
                    array(
                        'name' => esc_html__( 'Position X', 'khayr' ),
                        'desc' => esc_html__( 'position x.', 'khayr' ),
                        'default' => 50,
                        'id' => 'pos_x',
                        'type' => 'text'
                    ),
                    array(
                        'name' => esc_html__( 'Position Y', 'khayr' ),
                        'desc' => esc_html__( 'position y.', 'khayr' ),
                        'default' => 50,
                        'id' => 'pos_y',
                        'type' => 'text'
                    )
                )
            ),
        )
    );
}

