<div class="module module-contact-newsletter <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " 
    data-khayr-grow="module-contact-newsletter" 
    style="
        background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;
        --mcn-width:<?php echo isset($instance['width']) && $instance['width'] ? $instance['width'] : 100; ?><?php echo isset($instance['width_unit']) && $instance['width_unit'] ? $instance['width_unit'] : '%'; ?>; 
        --mcn-mobile-width:<?php echo isset($instance['mobile_width']) && $instance['mobile_width'] ? $instance['mobile_width'] : 100; ?><?php echo isset($instance['mobile_width_unit']) && $instance['mobile_width_unit'] ? $instance['mobile_width_unit'] : '%'; ?>;
        ">
    <div class="container">
    <div class="module-contact-newsletter__wrapper">
        <div class="module-contact-newsletter__top">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
                if ($instance['description']) {
                    echo '<p class="" '.khayr_scroll().'>';
                    echo $instance['description'];
                    echo '</p>';
                }
            ?>
        </div>
        <div class="module-contact-newsletter__bottom">
            <?php 
                if ($instance['contact_form']) {
                    echo do_shortcode($instance['contact_form']);
                }
            ?>
        </div>
    </div>
    </div>
</div>