<?php
/*
Widget Name: Kumo - Contact Newsletter
Description: kumo contact newsletter widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_contact_newsletter_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_contact_newsletter',
            __( 'Kumo - Contact Newsletter', 'khayr' ),
            [
                'description' => __( 'kumo contact newsletter widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => ''
                ],
                'description'   => [
                    'type'      => 'textarea',
                    'label'     => __( 'Description', 'khayr' ),
                    'default'   => ''
                ],
                'contact_form'  => [
                    'type'      => 'text',
                    'label'     => __( 'Contact Newsletter Form (shortcode)', 'khayr' ),
                    'default'   => ''
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
                'width'   => [
                    'type'      => 'number',
                    'label'     => __( 'Width (%)', 'khayr' ),
                    'default'   => 100
                ],
                'width_unit'   => [
                    'type'      => 'select',
                    'label'     => __( 'Width Unit', 'khayr' ),
                    'default'   => '%',
                    'options'   => [
                        '%' => '%',
                        'px' => 'px'
                    ]
                ],
                'mobile_width'   => [
                    'type'      => 'number',
                    'label'     => __( 'Width (%)', 'khayr' ),
                    'default'   => 100
                ],
                'mobile_width_unit'   => [
                    'type'      => 'select',
                    'label'     => __( 'Width Unit', 'khayr' ),
                    'default'   => '%',
                    'options'   => [
                        '%' => '%',
                        'px' => 'px'
                    ]
                ],

            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_contact_newsletter',
                'src'             => '/inc/widgets/kbw_contact_newsletter/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_contact_newsletter',
                'src'             => '/inc/widgets/kbw_contact_newsletter/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_contact_newsletter', __FILE__, 'kbw_contact_newsletter_widget' );