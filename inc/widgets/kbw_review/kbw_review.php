<?php
/*
Widget Name: Kumo - Review
Description: kumo review widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_review_widget extends SiteOrigin_Widget  {

    function __construct() {

        parent::__construct(
            'kbw_review',
            __( 'Kumo - Review', 'khayr' ),
            [
                'description' => __( 'kumo review widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Review'
                ],
                // option hide category
                'hide_category' => [
                    'type'    => 'checkbox',
                    'label'   => __( 'Hide Category', 'khayr' ),
                    'default' => false
                ],

            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_review',
                'src'             => '/inc/widgets/kbw_review/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_review',
                'src'             => '/inc/widgets/kbw_review/_style.css',
            ];
            return $css;
        });
    }
}

siteorigin_widget_register( 'kbw_review', __FILE__, 'kbw_review_widget' );