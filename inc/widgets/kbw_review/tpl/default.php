<div class="module module-review <?php echo $instance['hide_category'] ? 'hide-category' : '';?>" data-khayr-grow="module-review">
    <div class="module-review__wrapper">
        <div class="container">
            <div class="module-review__heading">
                <h2 class="module-review__title" <?php echo khayr_scroll();?> data-text-stagger><?php echo $instance['heading']; ?></h2>
            </div>
            <div class="module-review__tag-lists">
                <!-- get data from review category terms -->
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'review_category',
                    'hide_empty' => false,
                ) );
                ?>
                <?php if ( !empty($terms) ) : ?>
                    <ul class="module-review__tag-list">
                        <?php foreach ( $terms as $term ) : ?>
                            <li class="module-review__tag-item">
                                <a href="#<?php echo $term->slug;?>" data-slug="<?php echo $term->slug;?>" class="module-review__tag-link cursor-link"><?php echo $term->name; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <!-- swiper review items -->
            <div class="module-review__slides-wrap">
                <div class="module-review__count-item">
                    <span class="count"></span>/<span class="total"></span>
                </div>
                <div class="module-review__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                            $post_ids = [];
                            ?>
                        <?php if ( !empty($terms) ) : ?>
                            <?php foreach ( $terms as $term ) : ?>
                                <?php
                                $count = 0;
                                $cst_args = array(
                                    'post_type' => 'review',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'review_category',
                                            'field' => 'slug',
                                            'terms' => $term->slug,
                                        ),
                                    ),
                                );
                                $cst_query = new WP_Query($cst_args);
                                if ( $cst_query->have_posts() ) :
                                    foreach ( $cst_query->get_posts() as $post ) :
                                        $post_ids[] = $post->ID;
                                        $count++;
                                        $title = get_the_title($post->ID);
                                        $description = get_post_meta( $post->ID, 'hashtag', true );
                                        $label = get_post_meta( $post->ID, 'label', true );
                                        $color = get_term_meta( $term->term_id, 'color', true );
                                        $color = $color ? $color : 'transparent';
                                        $source = get_post_meta( $post->ID, 'source', true );
                                        ?>
                                        <div class="swiper-slide" data-cat-color="<?php echo $color;?>" data-cat-index="<?php echo $count; ?>" data-cat-total="<?php echo $term->count;?>" data-cat-name="<?php echo $term->slug; ?>">
                                            <div class="module-review__item">
                                                <div class="module-review__item-content">
                                                    <div class="module-review__item-description">
                                                        <p><?php echo $description; ?></p>
                                                    </div>
                                                    <p class="module-review__item-title"><?php echo $title; ?></p>
                                                    <?php echo $label && $label !== '' ? '<p class="module-review__item-label">'.$label.'</p>' : ''; ?>
                                                    <?php echo $source && $source !== '' ? '<p class="module-review__item-source">'.$source.'</p>' : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php 
                            if ( $instance['hide_category'] ) :
                                $cst_args = array(
                                    'post_type' => 'review',
                                    'posts_per_page' => -1,
                                    'post__not_in' => $post_ids,
                                );
                                $cst_query = new WP_Query($cst_args);
                                if ( $cst_query->have_posts() ) :
                                    foreach ( $cst_query->get_posts() as $post ) :
                                        $title = get_the_title($post->ID);
                                        $description = get_post_meta( $post->ID, 'hashtag', true );
                                        $label = get_post_meta( $post->ID, 'label', true );
                                        $source = get_post_meta( $post->ID, 'source', true );
                                        ?>
                                        <div class="swiper-slide" data-cat-color="transparent" data-cat-index="1" data-cat-total="1" data-cat-name="default">
                                            <div class="module-review__item">
                                                <div class="module-review__item-content">
                                                    <div class="module-review__item-description">
                                                        <p><?php echo $description; ?></p>
                                                    </div>
                                                    <p class="module-review__item-title"><?php echo $title; ?></p>
                                                    <?php echo $label && $label !== '' ? '<p class="module-review__item-label">'.$label.'</p>' : ''; ?>
                                                    <?php echo $source && $source !== '' ? '<p class="module-review__item-source">'.$source.'</p>' : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php 
                            endif;
                        ?>
                        </div>
                    
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</div>