(function (Khayr) {

    var module_review = function module_review(element) {
        const Self = this;
        this.swiper = this.swiper || [];
        let container = element.querySelector('.container');
        let slider = element.querySelector('.module-review__slides'),
            length = this.swiper.length;
        if (slider) {
            let idx = length++;
            this.swiper[idx] = new Swiper(slider, {
                slidesPerView: 1,
                loop: false,
                autoHeight: true,
                pagination: {
                    el: element.querySelector('.swiper-pagination')
                },
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                },
            });

            let changeAfter = (swiper) => {
                // get current slide element
                let currentSlide = swiper.slides[swiper.activeIndex];
                if (currentSlide) {
                    let getnumber = currentSlide.getAttribute('data-cat-index'), 
                        getcount = currentSlide.getAttribute('data-cat-total'),
                        getslug = currentSlide.getAttribute('data-cat-name'),
                        color = currentSlide.getAttribute('data-cat-color');
                    if (getnumber && getcount) {
                        let countElem = element.querySelector('.module-review__count-item .count'),
                            totalElem = element.querySelector('.module-review__count-item .total');
                        countElem.innerHTML = getnumber;
                        totalElem.innerHTML = getcount;
                    }
                    if (getslug) {
                        let tagElms = element.querySelectorAll('.module-review__tag-link');
                        tagElms.forEach(function (tagElm) {
                            let slug = tagElm.getAttribute('data-slug');
                            if (slug == getslug) {
                                tagElm.classList.add('active');
                            } else {
                                tagElm.classList.remove('active');
                            }
                        });
                    }
                    let wrap = element.querySelector('.module-review__slides-wrap');
                    if (color) {
                        wrap.style.backgroundColor = color;
                    } else {
                        wrap.style.backgroundColor = '';
                    }
                }
            }

            this.swiper[idx].on('slideChange', function () {
                changeAfter(this);
            });

            // on first init
            changeAfter(this.swiper[idx]);

            let tagElms = element.querySelectorAll('.module-review__tag-link');
            tagElms.forEach(function (tagElm) {
                tagElm.addEventListener('click', function (e) {
                    e.preventDefault();
                    let slug = tagElm.getAttribute('data-slug');
                    let slide = slider.querySelector('[data-cat-name="' + slug + '"][data-cat-index="1"]');
                    if (slide) {
                        let arialabel = slide.getAttribute('aria-label');
                        let slideindex = parseInt(arialabel.split('/')[0]) - 1;
                        Self.swiper[idx].slideTo(slideindex);
                    }
                });
            });
        }
    };

    Khayr.exec.add('module-review', function (elm) {
        return new module_review(elm);
    }, 'grow');
})(Khayr);