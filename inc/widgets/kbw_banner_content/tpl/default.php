<div class="module module-banner-content" data-khayr-grow="module-banner-content">
    <div class="container">
        <div class="module-banner-content__row">
            <div class="module-banner-content__col content">
                <div class="module-banner-content__wrapper">
                    <?php echo $instance['subheading'] && $instance['subheading'] !== '' ? '<span class="module-banner-content__subheading">'.$instance['subheading'].'</span>' : ''; ?>
                    <?php echo $instance['heading'] && $instance['heading'] !== '' ? '<h2 class="module-banner-content__heading">'.$instance['heading'].'</h2>' : ''; ?>
                    <?php 
                        if ($instance['content'] && $instance['content'] !== '') {
                            echo '<div class="module-banner-content__content">'.wpautop($instance['content']).'</div>';
                        }
                    ?>
                </div>
            </div>
            <div class="module-banner-content__col image">
                <div class="module-banner-content__image-wrapper">
                <?php 
                    if ($instance['image']) {
                        echo '<i style="display:block;padding-bottom:143%;position:relative;"></i>';
                        ?>
                            <div class="content-img__item">
                                <div class="content-img__item-imgwrap">
                                    <?php echo khayr_get_image($instance['image'], 'full', 
                                            array(
                                                'class' => array('content-img__item-img')
                                            )
                                        ); ?>
                                </div>
                            </div>
                        <?php
                    }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>