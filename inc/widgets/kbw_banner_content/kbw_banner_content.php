<?php
/*
Widget Name: Kumo - Banner Content
Description: kumo banner content widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_banner_content_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_banner_content',
            __( 'Kumo - Banner Content', 'khayr' ),
            [
                'description' => __( 'Banner Widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'subheading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Sub Heading', 'khayr' ),
                    'default'   => 'Halo, saya Utari.'
                ],
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Konsultan digital marketing yang kamu cari'
                ],
                'content'   => [
                    'type'      => 'textarea',
                    'label'     => __( 'Text Content', 'khayr' ),
                    'default'   => 'Didukung dengan pengalaman kerja selama bertahun-tahun di bidang digital marketing dan sertifikasi keahlitan terkait, seperti SEO, media sosial, data analyst, dll, saya membantu bisnis maupun tim-mu meraih sukses di dunia digital marketing yang disesuaikan dengan tujuan bisnismu.'
                ],
                'image'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image', 'khayr' )
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_banner_content',
                'src'             => '/inc/widgets/kbw_banner_content/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_banner_content',
                'src'             => '/inc/widgets/kbw_banner_content/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_banner_content', __FILE__, 'kbw_banner_content_widget' );