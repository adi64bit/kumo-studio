<div class="module module-text-overview-seo 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> 
    <?php echo isset($instance['text_align']) && $instance['text_align'] ? 'text-align-'.$instance['text_align'] : 'text-align-center'; ?>" 
    data-khayr-grow="module-text-overview-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-text-overview-seo__wrapper">
            <?php 
                if ($instance['heading']) {
                    echo '<div class="module-text-overview-seo__heading">';
                        echo '<h2 class="module-text-overview-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    echo '</div>';
                }
                
            ?>
            <div class="module-text-overview-seo__content">
                <?php 
                    if ($instance['content']) {
                        foreach ($instance['content'] as $key => $value) {
                            ?>
                                <div class="module-text-overview-seo__content-item article-content">
                                    <?php 
                                        if ($value['text']) {
                                            echo $value['text'];
                                        }
                                    ?>
                                </div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</div>