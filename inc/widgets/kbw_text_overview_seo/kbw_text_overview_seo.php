<?php
/*
Widget Name: Kumo - Text Overview (SEO)
Description: kumo Text Overview widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_text_overview_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_text_overview_seo',
            __( 'Kumo - Text Overview (SEO)', 'khayr' ),
            [
                'description' => __( 'Text Overview (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Overview Heading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'content'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Contents', 'khayr' ),
                    'item_name' => __( 'Content item', 'khayr' ),
                    'fields'    => [
                        'text'   => [
                            'type'      => 'tinymce',
                            'label'     => __( 'Text', 'khayr' ),
                            'default'   => ''
                        ],
                    ]
                ],
                // text alignment
                'text_align'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Alignment', 'khayr' ),
                    'default'   => 'center',
                    'options'   => [
                        'left'   => 'Left',
                        'center' => 'Center',
                        'right'  => 'Right'
                    ]
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                // text color white or black
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_text_overview_seo',
                'src'             => '/inc/widgets/kbw_text_overview_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_text_overview_seo',
                'src'             => '/inc/widgets/kbw_text_overview_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_text_overview_seo', __FILE__, 'kbw_text_overview_seo_widget' );