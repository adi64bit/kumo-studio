(function (Khayr) {

    var module_certification = function module_certification(element) {

        this.swiper = this.swiper || [];
        let container = element.querySelector('.container');
        let slider = element.querySelector('.module-certification__slides'),
            length = this.swiper.length;
        if (slider) {
            let index = length++;
            this.swiper[index] = new Swiper(slider, {
                slidesPerView: 4,
                loop: false,
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                breakpoints: {
                    320: {
                        slidesPerView: 2,
                    },
                    500: {
                        slidesPerView: 2,
                    },
                    1280: {
                        slidesPerView: 4,
                    },
                }
            });
        }
    };

    Khayr.exec.add('module-certification', function (elm) {
        return new module_certification(elm);
    }, 'grow');
})(Khayr);