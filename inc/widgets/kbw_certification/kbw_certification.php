<?php
/*
Widget Name: Kumo - Certification
Description: kumo certification widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_certification_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_certification',
            __( 'Kumo - Certification', 'khayr' ),
            [
                'description' => __( 'kumo certification widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Sertifikasi'
                ],
                'custom'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Custom Item', 'khayr' ),
                    'item_name' => __( 'item', 'khayr' ),
                    'fields'    => [
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                        'link'   => [
                            'type'      => 'link',
                            'label'     => __( 'Link', 'khayr' )
                        ],
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_certification',
                'src'             => '/inc/widgets/kbw_certification/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_certification',
                'src'             => '/inc/widgets/kbw_certification/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_certification', __FILE__, 'kbw_certification_widget' );