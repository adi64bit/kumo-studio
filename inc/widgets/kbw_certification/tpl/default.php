<div class="module module-certification" data-khayr-grow="module-certification">
    <div class="module-certification__wrapper">
        <div class="container">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="module-certification__heading" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
            ?>
        </div>
        <?php 
            $items = array();
            foreach ($instance['custom'] as $key => $value) {
                $items[] = array(
                    'image' => $value['image'] ? khayr_get_image($value['image'], 'full', array(
                        'class' => array('content-img__item-img')
                    )) : false,
                    'link'  => $value['link'] ? sow_esc_url($value['link']) : false
                );
            }

            if($items){
                ?>
                <div class="module-certification__slides-wrapper">
                    <div class="module-certification__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                                foreach ($items as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-certification__slide">
                                            <div class="module-certification__slide--wrap">
                                                <?php 
                                                    if ($value['link']) {
                                                        ?>
                                                            <a href="<?php echo $value['link'];?>" class="module-certification__slide--link">
                                                        <?php
                                                    }
                                                ?>
                                                <div class="module-certification__slide--image">
                                                    <?php 
                                                        if ($value['image']) {
                                                            ?>
                                                                <?php echo $value['image']; ?>
                                                            <?php
                                                        }
                                                    ?>
                                                </div>
                                                <?php 
                                                    if ($value['link']) {
                                                        ?>
                                                            </a>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>