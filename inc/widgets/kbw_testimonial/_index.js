(function (Khayr) {

    var module_testimonial = function module_testimonial(element) {

        this.swiper = this.swiper || [];
        let container = element.querySelector('.container');
        let slider = element.querySelector('.module-testimonial__slides'),
            length = this.swiper.length;
        if (slider) {
            let index = length++;
            this.swiper[index] = new Swiper(slider, {
                slidesPerView: 1,
                direction: 'horizontal',
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                breakpoints: {
                    320: {
                        slidesPerView: 1,
                        direction: 'horizontal',
                        autoHeight: true
                    },
                    769: {
                        slidesPerView: 2,
                        direction: 'vertical',
                        autoHeight: false
                    }
                }
            });
        }
    };

    Khayr.exec.add('module-testimonial', function (elm) {
        return new module_testimonial(elm);
    }, 'grow');
})(Khayr);