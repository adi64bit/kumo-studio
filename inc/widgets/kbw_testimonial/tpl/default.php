<div class="module module-testimonial" data-khayr-grow="module-testimonial">
    <div class="module-testimonial__wrapper">
        <div class="container">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="module-testimonial__heading" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
            ?>
        </div>
        <?php 
            $items = false;
            if ($instance['sources'] == 'testimonial') {
                $argsWidget = array(
                    'post_type'     => 'testimonial',
                    'post_status'   => 'publish',
                    'posts_per_page'=> -1
                );
                $query = new WP_Query($argsWidget);
                if($query->have_posts()){
                    $items = array();
                    foreach ($query->get_posts() as $key => $value) {
                        $items[] = array(
                            'label' => get_post_meta( $value->ID, 'label', true ),
                            'title' => get_the_title( $value->ID ),
                            'description'   => get_post_meta( $value->ID, 'hashtag', true ),
                            'image' => has_post_thumbnail($value->ID) ? khayr_get_image(get_post_thumbnail_id($value->ID), 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => false
                        );
                    }
                }
            } elseif ($instance['sources'] == 'custom') {
                if($instance['custom']){
                    $items = array();
                    foreach ($instance['custom'] as $key => $value) {
                        $items[] = array(
                            'label' => $value['label'],
                            'title' => $value['title'],
                            'description'   => $value['description'],
                            'image' => $value['image'] ? khayr_get_image($value['image'], 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => $value['link'] ? sow_esc_url($value['link']) : false
                        );
                    }
                }
            }

            if($items){
                ?>
                <div class="module-testimonial__slides-wrapper">
                    <div class="module-testimonial__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                                foreach ($items as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-testimonial__slide">
                                            <div class="module-testimonial__slide--wrap" <?php echo khayr_scroll(); ?>>
                                                <div class="module-testimonial__slide--image">
                                                    <?php 
                                                        if ($value['image']) {
                                                            ?>
                                                            <?php echo $value['image']; ?>
                                                                <!-- <div class="content-img__item no-animation">
                                                                    <div class="content-img__item-imgwrap">
                                                                        <?php echo $value['image']; ?>
                                                                    </div>
                                                                </div> -->
                                                            <?php
                                                        }
                                                    ?>
                                                    
                                                </div>
                                                <div class="module-testimonial__slide--content">
                                                <?php 
                                                    if ($value['label']) {
                                                        echo '<label class="module-testimonial__slide--label">'.$value['label'].'</label>';
                                                    }
                                                    if($value['title']){
                                                        echo '<h3 class="module-testimonial__slide--title">'.$value['title'].'</h3>';
                                                    }
                                                    if($value['description']){
                                                        echo '<p class="module-testimonial__slide--description">'.$value['description'].'</p>';
                                                    }
                                                    if ($value['link']) {
                                                        ?>
                                                            <div class="module module-button" data-khayr-grow="module-button">
                                                                <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                                                                    <a href="<?php echo $value['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text">Read More</span><span class="icon icon-panah"></span></a>
                                                                </div>
                                                            </div>
                                                        <?php
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>