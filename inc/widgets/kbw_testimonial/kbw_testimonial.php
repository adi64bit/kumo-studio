<?php
/*
Widget Name: Kumo - Testimonial
Description: kumo testimonial widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_testimonial_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_testimonial',
            __( 'Kumo - Testimonial', 'khayr' ),
            array(
                'description' => __( 'kumo testimonial widget.', 'khayr' )
            ),
            array(
            ),
            array(
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'SERVICES'
                ],
                'sources' => [
                    'type' => 'select',
                    'label' => __( 'Select Sources', 'khayr' ),
                    'default' => 'testimonial',
                    'options' => [
                        'testimonial' => __( 'Testimonial CPT', 'khayr' ),
                        'custom' => __( 'Custom', 'khayr' )
                    ],
                    'state_emitter' => array(
                        'callback' => 'select',
                        'args' => array( 'sources' )
                    ),
                ],
                'custom'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Custom Item', 'khayr' ),
                    'item_name' => __( 'item', 'khayr' ),
                    'state_handler' => array(
                        'sources[custom]' => array('show'),
                        'sources[testimonial]' => array('hide'),
                    ),
                    'fields'    => [
                        'label'   => [
                            'type'      => 'text',
                            'label'     => __( 'Label', 'khayr' )
                        ],
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'description'   => [
                            'type'      => 'textarea',
                            'label'     => __( 'Description', 'khayr' )
                        ],
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                    ]
                ],
            ),
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_testimonial',
                'src'             => '/inc/widgets/kbw_testimonial/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_testimonial',
                'src'             => '/inc/widgets/kbw_testimonial/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_testimonial', __FILE__, 'kbw_testimonial_widget' );