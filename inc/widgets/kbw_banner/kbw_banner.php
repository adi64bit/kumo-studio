<?php
/*
Widget Name: Kumo - Banner
Description: kumo banner widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_banner_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_banner',
            __( 'Kumo - Banner', 'khayr' ),
            [
                'description' => __( 'Banner Widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'logo'      => [
                    'type'      => 'media',
                    'label'     => __( 'Logo Image', 'khayr' )
                ],
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Digital Creative Studio'
                ],
                'services'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Services', 'khayr' ),
                    'item_name' => __( 'Service item', 'khayr' ),
                    'fields'    => [
                        'label'   => [
                            'type'      => 'text',
                            'label'     => __( 'Label', 'khayr' ),
                            'default'   => '#{index}'
                        ],
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'description'   => [
                            'type'      => 'text',
                            'label'     => __( 'Description', 'khayr' )
                        ],
                    ]
                ]
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_banner',
                'src'             => '/inc/widgets/kbw_banner/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_banner',
                'src'             => '/inc/widgets/kbw_banner/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_banner', __FILE__, 'kbw_banner_widget' );