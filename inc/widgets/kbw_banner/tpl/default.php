<div class="module module-banner" data-khayr-grow="module-banner">
    <div class="container">
        <div class="module-banner__wrapper">
            <div class="module-banner__top">
                <div class="module-banner__image">
                    <?php 
                        if ($instance['logo']) {
                            $temp = wp_get_attachment_metadata($instance['logo']);
                            if (isset($temp['file']) && strpos($temp['file'], '.svg') !== false) {
                                $uploadDir = wp_get_upload_dir();
                                echo file_get_contents($uploadDir['basedir'].''.$temp['file']);
                            } else {
                                echo khayr_get_image($instance['logo'], 'full');
                            }
                        }
                    ?>
                </div>
                <?php 
                    if ($instance['heading']) {
                        echo '<h1 class="module-banner__heading">';
                        echo $instance['heading'];
                        echo '</h1>';
                    }
                ?>
            </div>
        </div>
        <?php 
            if ($instance['services']) {
                ?>
                <div class="module-banner__services">
                    <div class="module-banner__services-slider swiper" style="opacity:0;">
                        <div class="swiper-wrapper">
                            
                            <?php 
                                foreach ($instance['services'] as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-banner__services-slide">
                                            <div class="module-banner__services-slide--wrap">
                                                <?php 
                                                    if($value['label']){
                                                        echo '<label class="module-banner__services-slide--label">'.str_replace('{index}', ($key + 1), $value['label']).'</label>';
                                                    }
                                                    if($value['title']){
                                                        echo '<h3 class="module-banner__services-slide--title">'.$value['title'].'</h3>';
                                                    }
                                                    if($value['description']){
                                                        echo '<p class="module-banner__services-slide--description">'.$value['description'].'</p>';
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                            <div class="swiper-slide module-banner__services-slide offset"></div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <?php
            }
        ?>
        <div class="module-banner__anchor">
            <a href="#" class="module-banner__anchor-trigger cursor-link"><span class="icon icon-panah"></span></a>
        </div>
    </div>
</div>