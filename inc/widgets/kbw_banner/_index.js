(function (Khayr) {
    var eventLoad = new Event('khayr-website-load');
    var module_banner = function module_banner(element) {

        document.body.classList.add('banner-widget');

        this.swiper = this.swiper || [];
        let slider = element.querySelector('.module-banner__services-slider'),
            length = this.swiper.length,
            header = document.querySelector('header.header');

        if (header) {
            header.style.opacity = 0;
        }

        let swiperSlider = () => {
            if (slider) {
                let index = length++;
                slider.style.opacity = 1;
                this.swiper[index] = new Swiper(slider, {
                    slidesPerView: 1,
                    loop: false,
                    pagination: {
                        el: '.swiper-pagination'
                    },
                    mousewheel: {
                        invert: false,
                        releaseOnEdges: true
                    },
                    breakpoints: {
                        320: {
                            slidesPerView: 2,
                        },
                        600: {
                            slidesPerView: 3,
                        },
                        1280: {
                            slidesPerView: 4,
                        },
                    }
                });
            }
        };
        let textStagger = (element) => {
            let elm = element.querySelector('.module-banner__heading');

            if (!elm) {
                return;
            }

            let alpha = Splitting({
                target: elm,
                by: 'chars'
            });

            let animation = anime({
                targets: alpha[0].chars,
                translateY: ['110%', '0%'],
                opacity: [0, 1],
                duration: 600,
                delay: anime.stagger(50),
                begin: () => {
                    elm.style.opacity = 1;
                },
                complete: () => {
                    swiperSlider();
                    document.body.classList.add('banner-widget-loaded');
                    if (header) {
                        header.style.opacity = 1;
                    }
                }
            });
        };

        let svgElm = document.querySelector('.module-banner__image svg');
        if (svgElm) {
            let paths = svgElm.querySelectorAll('path');
            let fill = [];
            let tl = anime.timeline({
                easing: 'easeInOutSine',
                autoplay: false
            });

            tl.add({
                targets: paths,
                strokeDashoffset: [anime.setDashoffset, 0],
                duration: 2000,
                delay: function (el, i) {
                    return i * 250
                },
                direction: 'forwards',
                begin: function (anim) {
                    svgElm.classList.add('initialize');
                    if (anim.animatables) {
                        anim.animatables.forEach((element, index) => {
                            fill[index] = element.target.getAttribute('fill');
                            element.target.setAttribute('fill', 'rgba(0,0,0,0)');
                            element.target.setAttribute('stroke', fill[index]);
                            element.target.setAttribute('stroke-width', '2px');
                        });
                    }
                },
                complete: (anim) => {
                    if (anim.animatables) {
                        anim.animatables.forEach((element, index) => {
                            let childAnim = anime({
                                targets: element.target,
                                duration: 1000,
                                fill: ['rgba(0,0,0,0)', '' + fill[index] + ''],
                                strokeWidth: ['2px', '0px'],
                                complete: () => {
                                    element.target.setAttribute('stroke', 'rgba(0,0,0,0)');
                                }
                            });
                        });
                    }
                    svgElm.classList.add('complete');

                    textStagger(element);

                    document.dispatchEvent(eventLoad);
                },
            });

            setTimeout(() => {
                tl.play();
            }, 300);

            let buttonAnchor = element.querySelector('.module-banner__anchor-trigger');
            if (buttonAnchor) {
                buttonAnchor.addEventListener('click', (e) => {
                    e.preventDefault();
                    let height = element.getBoundingClientRect().height;
                    jQuery('html, body').animate({
                        scrollTop: height + 'px'
                    });
                    return false;
                });
            }
        }
    };

    Khayr.exec.add('module-banner', function (elm) {
        return new module_banner(elm);
    }, 'grow');
})(Khayr);