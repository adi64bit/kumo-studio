(function (Khayr) {
    var module_gallery = function module_gallery(element) {

        this.swiper = this.swiper || [];
        let slider = element.querySelector('.module-gallery__slides'),
            length = this.swiper.length;
        if (slider) {
            let index = length++;
            this.swiper[index] = new Swiper(slider, {
                slidesPerView: 1,
                spaceBetween: 10,
                loop: 'auto',
                autoHeight: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                }
            });
        }
    };

    Khayr.exec.add('module-gallery', function (elm) {
        return new module_gallery(elm);
    }, 'grow');
})(Khayr);