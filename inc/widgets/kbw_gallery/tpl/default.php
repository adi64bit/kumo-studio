<?php 
    if ($instance['gallery']) {
        ?>
            <div class="module module-gallery" data-khayr-grow="module-gallery">
                <div class="module-gallery__wrapper">
                    <div class="module-gallery__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                                foreach ($instance['gallery'] as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-gallery__slide">
                                            <div class="module module-image" data-khayr-grow="module-image">
                                                <div class="module-image__wrapper">
                                                <?php 
                                                    if ($value['image']) {
                                                        echo khayr_get_image_ratio($value['image'], $instance['ratio']);
                                                        if ($value['animation'] == 'no-animation') {
                                                            echo khayr_get_image($value['image'], 'full', array(
                                                                'alt'   => $value['alt_text'] !== '' ? $value['alt_text'] : false
                                                            ));
                                                        } else {
                                                            ?>
                                                                <div class="content-img__item">
                                                                    <div class="content-img__item-imgwrap">
                                                                        <?php echo khayr_get_image($value['image'], 'full', 
                                                                                array(
                                                                                    'alt'   => $value['alt_text'] !== '' ? $value['alt_text'] : false,
                                                                                    'class' => array('content-img__item-img')
                                                                                )
                                                                            ); ?>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                                </div>
                                                <?php 
                                                    if ($value['image'] && $value['caption'] !== 'hide') {
                                                        ?>
                                                            <div class="module-image__caption <?php echo $value['caption'];?>">
                                                                <?php 
                                                                    $caption = '';
                                                                    if ($value['caption_text'] !== '') {
                                                                        $caption = $value['caption_text'];
                                                                    } else {
                                                                        $caption = wp_get_attachment_caption($value['image']);
                                                                    }
                                                                ?>
                                                                <span class="module-image__caption-text"><?php echo $caption;?></span>
                                                            </div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        <?php
    }
?>