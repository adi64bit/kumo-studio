<?php
/*
Widget Name: Kumo - Gallery Image
Description: kumo gallery image widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_gallery_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_gallery',
            __( 'Kumo - Gallery Image', 'khayr' ),
            [
                'description' => __( 'kumo gallery image widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'gallery'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Gallery', 'khayr' ),
                    'item_name' => __( 'Gallery item', 'khayr' ),
                    'fields'    => [
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                        'caption' => [
                            'type' => 'select',
                            'label' => __( 'Image Caption', 'khayr' ),
                            'default' => 'hide',
                            'options' => [
                                'hide' => __( 'Hide', 'khayr' ),
                                'left' => __( 'Left', 'khayr' ),
                                'right' => __( 'Right', 'khayr' ),
                                'center' => __( 'Center', 'khayr' )
                            ]
                        ],
                        'caption_text'   => [
                            'type'      => 'text',
                            'label'     => __( 'Caption Text', 'khayr' ),
                            'description'     => __( 'leave it blank to get the caption from Media Library', 'khayr' ),
                            'default'   => ''
                        ],
                        'alt_text'   => [
                            'type'      => 'text',
                            'label'     => __( 'ALT Text', 'khayr' ),
                            'description'     => __( 'leave it blank to get the caption from Media Library', 'khayr' ),
                            'default'   => ''
                        ],
                        'animation' => [
                            'type' => 'select',
                            'label' => __( 'Image Animation', 'khayr' ),
                            'default' => 'zoom',
                            'options' => [
                                'no-animation' => __( 'No Animation', 'khayr' ),
                                'zoom' => __( 'Zoom on scroll', 'khayr' )
                            ]
                        ]
                    ]
                ],
                'ratio' => [
                    'type' => 'select',
                    'label' => __( 'Image Ratio', 'khayr' ),
                    'default' => 'landscape',
                    'options' => [
                        'square' => __( 'square ratio', 'khayr' ),
                        'landscape' => __( 'landscape ratio', 'khayr' ),
                        'portrait' => __( 'portrait ratio', 'khayr' )
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {

        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_gallery',
                'src'             => '/inc/widgets/kbw_gallery/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_gallery',
                'src'             => '/inc/widgets/kbw_gallery/_style.css',
                'footer'          => true
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_gallery', __FILE__, 'kbw_gallery_widget' );