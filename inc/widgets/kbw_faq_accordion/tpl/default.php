<div class="module module-faq-accordion 
    <?php echo $instance['always_open'] ? 'always-open' : ''; ?> 
    <?php echo $instance['always_open_mobile'] ? 'always-open-mobile' : ''; ?> 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " data-khayr-grow="module-faq-accordion" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-faq-accordion__wrapper">
            <?php 
                if ($instance['heading']) {
                    echo '<div class="module-faq-accordion__heading">';
                        echo '<h2 class="module-faq-accordion__heading-text" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    echo '</div>';
                }                
            ?>
            <div class="module-faq-accordion__faqs" itemscope itemtype="https://schema.org/FAQPage">
                <?php 
                    $delay = 0;
                    if ($instance['content']) {
                        foreach ($instance['content'] as $key => $value) {
                            ?>
                                <div class="module-faq-accordion__content-item" <?php echo khayr_scroll($delay.'ms', '0.6s', 'fade');?> itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
                                    <div class="module-faq-accordion__content-item--heading">
                                        <h3 class="module-faq-accordion__content-item--heading--title " itemprop="name"><?php echo $value['content_title']; ?></h3>
                                    </div>
                                    <div class="module-faq-accordion__content-item--content" itemprop="acceptedAnswer" itemscope itemtype="https://schema.org/Answer">
                                        <div class="module-faq-accordion__content-item--content--wrapper article-content" itemprop="text">
                                            <?php echo $value['content_description']; ?>
                                        </div>
                                    </div>
                                    <div class="module-faq-accordion__content-item--line" <?php echo khayr_scroll(($delay + 100).'ms', '0.6s', '');?>></div>
                                </div>
                            <?php
                            $delay += 200;
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</div>