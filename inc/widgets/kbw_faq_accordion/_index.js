(function (Khayr) {
    var module_faq = function module_faq(element) {
        let cursor = document.querySelector(".square-cursor");

        const closeAll = (base) => {
            selectCallback(base, '.module-faq-accordion__content-item', (item) => {
                item.classList.remove('im-faq-accordion--active');
                item.style.setProperty('--faq-height', '0px');
            }, true);
        };

        const toggle = (base, item) => {
            if (!item.classList.contains('im-faq-accordion--active')) {
                let wrap = selectChild(item, '.module-faq-accordion__content-item--content--wrapper'),
                    height = wrap.offsetHeight;
                item.style.setProperty('--faq-height', height + 'px');
                item.classList.add('im-faq-accordion--active');

            } else {
                item.classList.remove('im-faq-accordion--active');
                item.style.setProperty('--faq-height', '0px');
            }
        };

        selectCallback(element, '.module-faq-accordion__content-item', (item) => {
            let heading = selectChild(item, '.module-faq-accordion__content-item--heading');
            if (heading) {
                onKum('click', heading, (e) => {
                    e.preventDefault();
                    toggle(element, item);
                    return false;
                });
            }

            onKum('mouseover', item, () => {
                if (cursor) {
                    if (!item.classList.contains('im-faq-accordion--active')) {
                        cursor.classList.add('faq-open');
                        cursor.classList.remove('faq-close');
                    } else {
                        cursor.classList.remove('faq-open');
                        cursor.classList.add('faq-close');
                    }
                }
            });

            onKum('mouseleave', item, () => {
                if (cursor) {
                    cursor.classList.remove('faq-close');
                    cursor.classList.remove('faq-open');
                }
            });
        }, true);

        let setHeight = () => {
            selectCallback(element, '.module-faq-accordion__content-item.im-faq-accordion--active', (item) => {
                let wrap = selectChild(item, '.module-faq-accordion__content-item--content--wrapper'),
                    height = wrap.offsetHeight;
                item.style.setProperty('--faq-height', height + 'px');
            }, true);
        };

        if ((window.innerWidth > 991 && element.classList.contains('always-open')) || (window.innerWidth < 991 && element.classList.contains('always-open-mobile'))) {
            selectCallback(element, '.module-faq-accordion__content-item', (item) => {
                item.classList.add('im-faq-accordion--active');
            }, true);
            setHeight();
        } else {
            closeAll(element);
        }

        window.addEventListener('resize', () => {
            setHeight();
        });
    };

    Khayr.exec.add('module-faq-accordion', function (elm) {
        return new module_faq(elm);
    }, 'grow');
})(Khayr);