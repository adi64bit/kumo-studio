<?php
/*
Widget Name: Kumo - FAQ (SEO)
Description: Kumo FAQ widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_faq_accordion_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_faq_accordion',
            __( 'Kumo - FAQ (SEO)', 'khayr' ),
            [
                'description' => __( 'FAQ (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'FAQ Heading', 'khayr' ),
                    'default'   => 'FAQ'
                ],
                'content'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Contents', 'khayr' ),
                    'item_name' => __( 'Content item', 'khayr' ),
                    'fields'    => [
                        'content_title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Content Title', 'khayr' ),
                            'default'   => 'Lorem Ipsum Dolor'
                        ],
                        'content_description'   => [
                            'type'      => 'tinymce',
                            'label'     => __( 'Content Description', 'khayr' ),
                            'default'   => 'Lorem Ipsum Dolor'
                        ],
                    ]
                ],
                // option faq always open
                'always_open'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Always Open Desktop', 'khayr' ),
                    'default'   => false
                ],
                'always_open_mobile'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Always Open Mobile', 'khayr' ),
                    'default'   => false
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_faq_accordion',
                'src'             => '/inc/widgets/kbw_faq_accordion/_index.js',
                'footer'          => true,
                'deps'            => ['constants']
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_faq_accordion',
                'src'             => '/inc/widgets/kbw_faq_accordion/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_faq_accordion', __FILE__, 'kbw_faq_accordion_widget' );