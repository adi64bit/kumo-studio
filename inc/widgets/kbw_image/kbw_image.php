<?php
/*
Widget Name: Kumo - Image
Description: kumo image widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_image_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_image',
            __( 'Kumo - Image', 'khayr' ),
            [
                'description' => __( 'kumo image widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'image'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image', 'khayr' )
                ],
                'caption' => [
                    'type' => 'select',
                    'label' => __( 'Image Caption', 'khayr' ),
                    'default' => 'hide',
                    'options' => [
                        'hide' => __( 'Hide', 'khayr' ),
                        'left' => __( 'Left', 'khayr' ),
                        'right' => __( 'Right', 'khayr' ),
                        'center' => __( 'Center', 'khayr' )
                    ]
                ],
                'caption_text'   => [
                    'type'      => 'text',
                    'label'     => __( 'Caption Text', 'khayr' ),
                    'description'     => __( 'leave it blank to get the caption from Media Library', 'khayr' ),
                    'default'   => ''
                ],
                'alt_text'   => [
                    'type'      => 'text',
                    'label'     => __( 'ALT Text', 'khayr' ),
                    'description'     => __( 'leave it blank to get the caption from Media Library', 'khayr' ),
                    'default'   => ''
                ],
                'ratio' => [
                    'type' => 'select',
                    'label' => __( 'Image Ratio', 'khayr' ),
                    'default' => 'image',
                    'options' => [
                        'image' => __( 'original image ratio', 'khayr' ),
                        'square' => __( 'square ratio', 'khayr' ),
                        'landscape' => __( 'landscape ratio', 'khayr' ),
                        'portrait' => __( 'portrait ratio', 'khayr' )
                    ]
                ],
                'animation' => [
                    'type' => 'select',
                    'label' => __( 'Image Animation', 'khayr' ),
                    'default' => 'zoom',
                    'options' => [
                        'no-animation' => __( 'No Animation', 'khayr' ),
                        'zoom' => __( 'Zoom on scroll', 'khayr' )
                    ]
                ]
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_image',
                'src'             => '/inc/widgets/kbw_image/_style.css',
                'footer'          => true
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_image', __FILE__, 'kbw_image_widget' );