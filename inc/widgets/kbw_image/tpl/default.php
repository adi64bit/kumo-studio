<div class="module module-image" data-khayr-grow="module-image">
    <div class="module-image__wrapper">
    <?php 
        if ($instance['image']) {
            echo khayr_get_image_ratio($instance['image'], $instance['ratio']);
            if ($instance['animation'] == 'no-animation') {
                echo khayr_get_image($instance['image'], 'full', array(
                    'alt'   => $instance['alt_text'] !== '' ? $instance['alt_text'] : false
                ));
            } else {
                ?>
                    <div class="content-img__item">
                        <div class="content-img__item-imgwrap">
                            <?php echo khayr_get_image($instance['image'], 'full', 
                                    array(
                                        'alt'   => $instance['alt_text'] !== '' ? $instance['alt_text'] : false,
                                        'class' => array('content-img__item-img')
                                    )
                                ); ?>
                        </div>
                    </div>
                <?php
            }
        }
    ?>
    </div>
    <?php 
        if ($instance['image'] && $instance['caption'] !== 'hide') {
            ?>
                <div class="module-image__caption <?php echo $instance['caption'];?>">
                    <?php 
                        $caption = '';
                        if ($instance['caption_text'] !== '') {
                            $caption = $instance['caption_text'];
                        } else {
                            $caption = wp_get_attachment_caption($instance['image']);
                        }
                    ?>
                    <span class="module-image__caption-text"><?php echo $caption;?></span>
                </div>
            <?php
        }
    ?>
</div>