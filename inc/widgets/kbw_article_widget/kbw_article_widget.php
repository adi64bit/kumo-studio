<?php
/*
Widget Name: Kumo - Article Widget
Description: kumo article widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_article_widget_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_article_widget',
            __( 'Kumo - Article Widget', 'khayr' ),
            [
                'description' => __( 'kumo article widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                // select article from post
                'article'  => [
                    'type'      => 'select',
                    'label'     => __( 'Article', 'khayr' ),
                    'multiple'  => false,
                    'select2'   => true,
                    'options'   => $this->get_posts()
                ],
                // option to show or hide image
                'show_image'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Image', 'khayr' ),
                    'default'   => true
                ],
                'show_category'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Category', 'khayr' ),
                    'default'   => false
                ],
                'show_date'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Date', 'khayr' ),
                    'default'   => false
                ],
                'show_description'  => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Description', 'khayr' ),
                    'default'   => false
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_article_widget',
                'src'             => '/inc/widgets/kbw_article_widget/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_article_widget',
                'src'             => '/inc/widgets/kbw_article_widget/_style.css'
            ];
            return $css;
        });
    }

    function get_posts() {
        $posts = get_posts([
            'post_type' => 'post',
            'numberposts' => -1
        ]);

        $options = [];
        foreach ($posts as $post) {
            $options[$post->ID] = $post->post_title;
        }

        return $options;
    }
}
siteorigin_widget_register( 'kbw_article_widget', __FILE__, 'kbw_article_widget_widget' );