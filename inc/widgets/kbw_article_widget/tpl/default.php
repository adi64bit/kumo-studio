<div class="module module-article-widget" data-khayr-grow="module-article-widget">
    <div class="module-article-widget__wrapper khayr-ajax__list 
        <?php echo $instance['show_image'] ? 'module-article-widget__show-image' : '';?> 
        <?php echo $instance['show_category'] ? 'maw-show-category' : '';?> 
        <?php echo $instance['show_date'] ? 'maw-show-date' : '';?> 
        <?php echo $instance['show_description'] ? 'maw-show-description' : '';?> ">
    <?php 
        $article_id = $instance['article'];
        $article = get_post($article_id);
        if ($article) {
            $categories = wp_get_post_categories( $article_id, array( 'fields' => 'all' ) );
            $content = $article->post_excerpt;
            if ($content && $content != '') {
                # code...
            } else {
                ob_start();
                if ( class_exists( 'SiteOrigin_Panels' ) && get_post_meta( $article_id, 'panels_data', true ) ) {
                    echo SiteOrigin_Panels::renderer()->render( $article_id );
                } else {
                    $post_content = wpautop(get_post( $article_id )->post_content);
                    $modified_content = apply_filters( 'the_content', $post_content );
                    echo $modified_content;
                }
                $content = ob_get_clean();
                $content = wp_trim_words($content, 25, '...');
            }
            $items = array(
                'category' => $categories,
                'title' => get_the_title( $article_id ),
                'description'   => $content,
                'date'  => get_the_date('d M Y', $article_id),
                'link'  => get_the_permalink($article_id),
                'image' => has_post_thumbnail($article_id) ? khayr_get_image(get_post_thumbnail_id($article_id), 'full', array(
                    'class' => array('content-img__item-img')
                )) : false
            );
            ?>
            <div class="module-latest-blog__item" <?php echo khayr_scroll(); ?>>
                <div class="module-latest-blog__item--image">
                    <?php 
                        if ($items['image']) {
                            ?>
                                <div class="content-img__item">
                                    <div class="content-img__item-imgwrap">
                                        <?php echo $items['image']; ?>
                                    </div>
                                </div>
                            <?php
                        }
                    ?>
                </div>
                <div class="module-latest-blog__item--content">
                    <?php 
                        if ($items['category']) {
                            echo '<ul class="module-latest-blog__item-category">';
                            foreach ($items['category'] as $k => $v) {
                                echo '<li><a href="'.get_term_link($v->term_id).'" class="cursor-link">'.$v->name.'</a></li>';
                            }
                            echo '</ul>';
                        }
                        if($items['title']){
                            echo '<h3 class="module-latest-blog__item--title">'.$items['title'].'</h3>';
                        }
                        if($items['date']){
                            echo '<span class="module-latest-blog__item--date-inner">'.$items['date'].'</span>';
                        }
                        if($items['description']){
                            echo '<p class="module-latest-blog__item--description">'.$items['description'].'</p>';
                        }
                        if ($items['link']) {
                            ?>
                                <div class="module module-button" data-khayr-grow="module-button">
                                    <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                                        <a href="<?php echo $items['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text">Read Blog</span><span class="icon icon-panah"></span></a>
                                    </div>
                                </div>
                            <?php
                        }
                    ?>
                </div>
            </div>
            <?php
        }
    ?>
    </div>
</div>