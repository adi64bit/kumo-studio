<div class="module module-services-seo 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " data-khayr-grow="module-services-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-services-seo__wrapper">
            <div class="module-services-seo__left">
            <?php 
                if ($instance['heading']) {
                    echo '<div class="module-services-seo__heading">';
                        echo '<h2 class="module-services-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    echo '</div>';
                }
                if ($instance['lists']) {
                    ?>
                    <div class="module-services-seo__content">
                        <?php 
                            foreach ($instance['lists'] as $key => $value) {
                                ?>
                                    <div class="module-services-seo__content-item">
                                        <?php 
                                            if ($value['url']) {
                                                echo '<a href="'.$value['url'].'" class="module-services-seo__content-item-link cursor-link">';
                                            }
                                            if ($value['text']) {
                                                echo '<h4 class="module-services-seo__content-item-title" '.khayr_scroll().'>';
                                                echo $value['text'];
                                                echo '</h4>';
                                            }
                                            if ($value['url']) {
                                                echo '</a>';
                                            }
                                        ?>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                    <?php
                }
                
            ?>
            </div>
            <div class="module-services-seo__right">
                <div class="module-services-seo__image-wrapper lax-elm-down">
                    <?php 
                        if ($instance['image_1']) {
                            ?>
                            <div class="module-multi-image-seo__image-1">
                                <?php echo khayr_get_image_ratio($instance['image_1'], 120); ?>
                                <div class="content-img__item">
                                    <div class="content-img__item-imgwrap">
                                        <?php echo khayr_get_image($instance['image_1'], 'full', 
                                                array(
                                                    'class' => array('content-img__item-img')
                                                )
                                            ); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        if ($instance['image_2']) {
                            ?>
                            <div class="module-multi-image-seo__image-2">
                                <?php echo khayr_get_image_ratio($instance['image_2'], 120); ?>
                                <div class="content-img__item">
                                    <div class="content-img__item-imgwrap">
                                        <?php echo khayr_get_image($instance['image_2'], 'full', 
                                                array(
                                                    'class' => array('content-img__item-img')
                                                )
                                            ); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <div class="module-services-seo__image-wrapper lax-elm-up" >
                        <?php 
                            if ($instance['image_3']) {
                                ?>
                                <div class="module-multi-image-seo__image-3">
                                    <?php echo khayr_get_image_ratio($instance['image_3'], 120); ?>
                                    <div class="content-img__item">
                                        <div class="content-img__item-imgwrap">
                                            <?php echo khayr_get_image($instance['image_3'], 'full', 
                                                    array(
                                                        'class' => array('content-img__item-img')
                                                    )
                                                ); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                </div>
            </div>
        </div>
    </div>
    <div class="module-services-seo__right-mobile">
                <!-- slider swiper -->
                <div class="swiper module-services-seo__right-mobile-swiper">
                    <div class="swiper-wrapper">
                        <?php 
                            if ($instance['image_1']) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="module-services-seo__right-mobile-swiper-image">
                                        <div class="content-img__item">
                                            <div class="content-img__item-imgwrap">
                                                <?php echo khayr_get_image($instance['image_1'], 'full', 
                                                        array(
                                                            'class' => array('content-img__item-img')
                                                        )
                                                    ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            if ($instance['image_2']) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="module-services-seo__right-mobile-swiper-image">
                                        <div class="content-img__item">
                                            <div class="content-img__item-imgwrap">
                                                <?php echo khayr_get_image($instance['image_2'], 'full', 
                                                        array(
                                                            'class' => array('content-img__item-img')
                                                        )
                                                    ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            if ($instance['image_3']) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="module-services-seo__right-mobile-swiper-image">
                                        <div class="content-img__item">
                                            <div class="content-img__item-imgwrap">
                                                <?php echo khayr_get_image($instance['image_3'], 'full', 
                                                        array(
                                                            'class' => array('content-img__item-img')
                                                        )
                                                    ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
            </div>
</div>