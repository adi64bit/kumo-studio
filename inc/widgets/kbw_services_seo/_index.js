(function (Khayr) {

    var module_services_seo = function module_services_seo(element) {

        let runOnMobile = (elm) => {
            let slider = elm.querySelector('.module-services-seo__right-mobile-swiper');
            if (slider) {
                element.KSSwiper = new Swiper(slider, {
                    slidesPerView: 2.3,
                    spaceBetween: 30,
                    slidesOffsetBefore: 30,
                    slidesOffsetAfter: 30,
                    loop: false,
                    mousewheel: {
                        invert: false,
                        releaseOnEdges: true
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    },
                    breakpoints: {
                        320: {
                            slidesPerView: 1.3,
                        },
                        500: {
                            slidesPerView: 1.5,
                        },
                        1280: {
                            slidesPerView: 2.3,
                        },
                    }
                });
            }
        };

        let runOnMobileCheck = () => {
            if (window.innerWidth < 991) {
                if (!element.KSSwiper) {
                    runOnMobile(element);
                }
            }
        };

        runOnMobileCheck();

        window.addEventListener('resize', () => {
            runOnMobileCheck();
        });
    };

    Khayr.exec.add('module-services-seo', function (elm) {
        return new module_services_seo(elm);
    }, 'grow');
})(Khayr);