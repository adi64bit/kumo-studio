<?php
/*
Widget Name: Kumo - Services (SEO)
Description: kumo Services widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_services_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_services_seo',
            __( 'Kumo - Services (SEO)', 'khayr' ),
            [
                'description' => __( 'Services (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Services Heading', 'khayr' ),
                    'default'   => 'Services Offered'
                ],
                'lists'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'lists', 'khayr' ),
                    'item_name' => __( 'list item', 'khayr' ),
                    'fields'    => [
                        'text'   => [
                            'type'      => 'text',
                            'label'     => __( 'Text', 'khayr' ),
                            'default'   => ''
                        ],
                        'url'   => [
                            'type'      => 'link',
                            'label'     => __( 'URL', 'khayr' ),
                            'default'   => ''
                        ],
                    ]
                ],
                'image_1'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image 1', 'khayr' )
                ],
                'image_2'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image 2', 'khayr' )
                ],
                'image_3'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image 3', 'khayr' )
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_services_seo',
                'src'             => '/inc/widgets/kbw_services_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_services_seo',
                'src'             => '/inc/widgets/kbw_services_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_services_seo', __FILE__, 'kbw_services_seo_widget' );