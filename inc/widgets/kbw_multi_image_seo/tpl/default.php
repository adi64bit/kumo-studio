<div class="module module-multi-image-seo  
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?>" 
    data-khayr-grow="module-multi-image-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-multi-image-seo__wrapper">
            <div class="module-multi-image-seo__left">
                <?php 
                    if ($instance['heading']) {
                        echo '<div class="module-multi-image-seo__heading">';
                            echo '<h2 class="module-multi-image-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                            echo $instance['heading'];
                            echo '</h2>';
                        echo '</div>';
                    }
                    if ($instance['image_1']) {
                        ?>
                        <div class="module-multi-image-seo__left-image">
                            <?php echo khayr_get_image_ratio($instance['image_1'], 120); ?>
                            <div class="content-img__item">
                                <div class="content-img__item-imgwrap">
                                    <?php echo khayr_get_image($instance['image_1'], 'full', 
                                            array(
                                                'class' => array('content-img__item-img')
                                            )
                                        ); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                ?>
            </div>
            <div class="module-multi-image-seo__right lax-elm-up">
                <?php 
                    if ($instance['image_2']) {
                        ?>
                        <div class="module-multi-image-seo__right-image">
                            <?php echo khayr_get_image_ratio($instance['image_2'], 120); ?>
                            <div class="content-img__item">
                                <div class="content-img__item-imgwrap">
                                    <?php echo khayr_get_image($instance['image_2'], 'full', 
                                            array(
                                                'class' => array('content-img__item-img')
                                            )
                                        ); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($instance['text_below_image']) {
                        echo '<div class="module-multi-image-seo__right-text">';
                            echo '<h3 class="module-multi-image-seo__right-text-content">';
                            echo $instance['text_below_image'];
                            echo '</h3>';
                        echo '</div>';
                    }
                ?>
            </div>
        </div>
    </div>
</div>