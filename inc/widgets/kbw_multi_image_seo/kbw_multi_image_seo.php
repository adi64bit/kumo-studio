<?php
/*
Widget Name: Kumo - Multi Image (SEO)
Description: Kumo Multi Image widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_multi_image_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_multi_image_seo',
            __( 'Kumo - Multi Image (SEO)', 'khayr' ),
            [
                'description' => __( 'Multi Image (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Multi Image Heading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'image_1'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image 1', 'khayr' )
                ],
                'image_2'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image 2', 'khayr' )
                ],
                'text_below_image'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Below Image', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_multi_image_seo',
                'src'             => '/inc/widgets/kbw_multi_image_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_multi_image_seo',
                'src'             => '/inc/widgets/kbw_multi_image_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_multi_image_seo', __FILE__, 'kbw_multi_image_seo_widget' );