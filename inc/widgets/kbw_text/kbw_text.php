<?php
/*
Widget Name: Kumo - Text Content (WYSIWYG)
Description: kumo text content wysiwyg widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_text_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_text',
            __( 'Kumo - Text Content (WYSIWYG)', 'khayr' ),
            [
                'description' => __( 'kumo text content wysiwyg widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'text_content' => [
                    'type' => 'tinymce',
                    'label' => __( 'Text Content', 'khayr' ),
                    'rows' => 10,
                    'default_editor' => 'html',
                    'default' => '',
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
    }
}
siteorigin_widget_register( 'kbw_text', __FILE__, 'kbw_text_widget' );