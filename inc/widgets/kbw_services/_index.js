(function (Khayr) {

    var module_services = function module_services(element) {

        this.swiper = this.swiper || [];
        let container = element.querySelector('.container');
        let slider = element.querySelector('.module-services__slides'),
            length = this.swiper.length;
        if (slider) {
            let index = length++;
            this.swiper[index] = new Swiper(slider, {
                slidesPerView: 2.3,
                loop: false,
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                },
                breakpoints: {
                    320: {
                        slidesPerView: 1.1,
                    },
                    500: {
                        slidesPerView: 1.3,
                    },
                    1280: {
                        slidesPerView: 2.3,
                    },
                }
            });
        }

        let setPad = () => {
            if(container && slider){
                slider.style.paddingLeft = '';
                let left = container.getBoundingClientRect().left;
                slider.style.paddingLeft = left + 'px';
            }
        }

        setPad();

        window.addEventListener('resize', () => {
            setPad();
        });

        window.addEventListener('load', () => {
            setPad();
        });
    };

    Khayr.exec.add('module-services', function (elm) {
        return new module_services(elm);
    }, 'grow');
})(Khayr);