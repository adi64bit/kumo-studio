<div class="module module-services" data-khayr-grow="module-services">
    <div class="module-services__wrapper">
        <div class="container">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="module-services__heading" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
            ?>
        </div>
        <?php 
            $items = false;
            if ($instance['sources'] == 'services') {
                $argsWidget = array(
                    'post_type'     => 'service',
                    'post_status'   => 'publish',
                    'posts_per_page'=> -1
                );
                $query = new WP_Query($argsWidget);
                if($query->have_posts()){
                    $items = array();
                    foreach ($query->get_posts() as $key => $value) {
                        $link_text = get_post_meta( $value->ID, 'link_text', true );
                        $items[] = array(
                            'label' => get_post_meta( $value->ID, 'label', true ),
                            'title' => get_the_title( $value->ID ),
                            'description'   => get_post_meta( $value->ID, 'hashtag', true ),
                            'image' => has_post_thumbnail($value->ID) ? khayr_get_image(get_post_thumbnail_id($value->ID), 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => get_post_meta( $value->ID, 'link', true ),
                            'link_text' => $link_text && $link_text !== '' ? $link_text : 'Read More'
                        );
                    }
                }
            } elseif ($instance['sources'] == 'custom') {
                if($instance['custom']){
                    $items = array();
                    foreach ($instance['custom'] as $key => $value) {
                        $items[] = array(
                            'label' => $value['label'],
                            'title' => $value['title'],
                            'description'   => $value['description'],
                            'image' => $value['image'] ? khayr_get_image($value['image'], 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => $value['link'] ? sow_esc_url($value['link']) : false,
                            'link_text' => $value['link_text'] ? $value['link_text'] : 'Read More'
                        );
                    }
                }
            }

            if($items){
                ?>
                <div class="module-services__slides-wrapper">
                    <div class="module-services__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                                foreach ($items as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-services__slide">
                                            <div class="module-services__slide--wrap">
                                                <div class="module-services__slide--image">
                                                    <?php 
                                                        if ($value['image']) {
                                                            ?>
                                                                <?php echo $value['image']; ?>
                                                                <!-- <div class="content-img__item">
                                                                    <div class="content-img__item-imgwrap">
                                                                        <?php echo $value['image']; ?>
                                                                    </div>
                                                                </div> -->
                                                            <?php
                                                        }
                                                    ?>
                                                    
                                                </div>
                                                <?php 
                                                    if ($value['label']) {
                                                        echo '<label class="module-services__slide--label">'.$value['label'].'</label>';
                                                    }
                                                    if($value['title']){
                                                        echo '<h3 class="module-services__slide--title">'.$value['title'].'</h3>';
                                                    }
                                                    if($value['description']){
                                                        echo '<p class="module-services__slide--description">'.$value['description'].'</p>';
                                                    }
                                                    if ($value['link'] && $value['link'] != '') {
                                                        ?>
                                                            <div class="module module-button" data-khayr-grow="module-button">
                                                                <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                                                                    <a href="<?php echo $value['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text"><?php echo $value['link_text'];?></span><span class="icon icon-panah"></span></a>
                                                                </div>
                                                            </div>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>