<?php
/*
Widget Name: Kumo - Services
Description: kumo services widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_services_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_services',
            __( 'Kumo - Services', 'khayr' ),
            [
                'description' => __( 'kumo services widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'SERVICES'
                ],
                'sources' => [
                    'type' => 'select',
                    'label' => __( 'Select Sources', 'khayr' ),
                    'default' => 'services',
                    'options' => [
                        'services' => __( 'Service CPT', 'khayr' ),
                        'custom' => __( 'Custom', 'khayr' )
                    ],
                    'state_emitter' => array(
                        'callback' => 'select',
                        'args' => array( 'sources' )
                    ),
                ],
                'custom'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Custom Item', 'khayr' ),
                    'item_name' => __( 'item', 'khayr' ),
                    'state_handler' => array(
                        'sources[custom]' => array('show'),
                        'sources[services]' => array('hide'),
                    ),
                    'fields'    => [
                        'label'   => [
                            'type'      => 'text',
                            'label'     => __( 'Label', 'khayr' )
                        ],
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'description'   => [
                            'type'      => 'textarea',
                            'label'     => __( 'Description', 'khayr' )
                        ],
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                        'link'   => [
                            'type'      => 'link',
                            'label'     => __( 'Link', 'khayr' )
                        ],
                        'link_text'   => [
                            'type'      => 'text',
                            'label'     => __( 'Link Text', 'khayr' )
                        ],
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_services',
                'src'             => '/inc/widgets/kbw_services/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_services',
                'src'             => '/inc/widgets/kbw_services/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_services', __FILE__, 'kbw_services_widget' );