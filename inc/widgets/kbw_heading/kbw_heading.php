<?php
/*
Widget Name: Kumo - Heading
Description: kumo heading widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_heading_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_heading',
            __( 'Kumo - Heading', 'khayr' ),
            [
                'description' => __( 'kumo heading widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'tag' => [
                    'type' => 'select',
                    'label' => __( 'Tag', 'khayr' ),
                    'default' => 'h2',
                    'options' => [
                        'h1' => __( 'H1', 'khayr' ),
                        'h2' => __( 'H2', 'khayr' ),
                        'h3' => __( 'H3', 'khayr' ),
                        'h4' => __( 'H4', 'khayr' ),
                        'h5' => __( 'H5', 'khayr' ),
                        'h6' => __( 'H6', 'khayr' ),
                    ]
                ],
                'text' => [
                    'type' => 'text',
                    'label' => __( 'Text', 'khayr' ),
                    'default' => 'Heading',
                ],
                'align' => [
                    'type' => 'select',
                    'label' => __( 'Aligment', 'khayr' ),
                    'default' => 'left',
                    'options' => [
                        'left' => __( 'Left', 'khayr' ),
                        'center' => __( 'Center', 'khayr' ),
                        'right' => __( 'Right', 'khayr' ),
                    ]
                ],
                'color' => [
                    'type' => 'select',
                    'label' => __( 'Color', 'khayr' ),
                    'default' => 'white',
                    'options' => [
                        'white' => __( 'White', 'khayr' ),
                        'orange' => __( 'orange', 'khayr' )
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_heading',
                'src'             => '/inc/widgets/kbw_heading/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_heading',
                'src'             => '/inc/widgets/kbw_heading/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_heading', __FILE__, 'kbw_heading_widget' );