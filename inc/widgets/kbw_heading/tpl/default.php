<?php
    if ($instance['text'] !== '') {
        ?>
            <div class="module module-heading" data-khayr-grow="module-heading">
                <div class="module-heading__wrapper" >
                    <<?php echo $instance['tag'] ;?> 
                        class="module-heading__wrapper--text text-<?php echo $instance['align'] ;?> text-<?php echo $instance['color'] ;?>" data-text-stagger><?php echo $instance['text'] ;?></<?php echo $instance['tag'] ;?>>
                </div>
            </div>
        <?php
    }
?>