<div class="module module-image-text-seo 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " data-khayr-grow="module-image-text-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-image-text-seo__wrapper">
            <?php 
                if ($instance['heading']) {
                    echo '<div class="module-image-text-seo__heading">';
                        echo '<h2 class="module-image-text-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    echo '</div>';
                }                
            ?>
            <div class="module-image-text-seo__row">
                <?php 
                    if ($instance['image']) {
                        ?>
                        <div class="module-image-text-seo__left">
                            <div class="module-image-text-seo__image-wrapper ">
                                <?php echo khayr_get_image_ratio($instance['image'], 60); ?>
                                <div class="content-img__item">
                                    <div class="content-img__item-imgwrap">
                                        <?php echo khayr_get_image($instance['image'], 'full', 
                                                array(
                                                    'class' => array('content-img__item-img')
                                                )
                                            ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($instance['text']) {
                        ?>
                        <div class="module-image-text-seo__right">
                            <div class="module-image-text-seo__text article-content lax-elm-up">
                                <?php 
                                    echo $instance['text'];
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>