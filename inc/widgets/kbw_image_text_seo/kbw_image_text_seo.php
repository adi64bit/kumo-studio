<?php
/*
Widget Name: Kumo - Image Text (SEO)
Description: Kumo Image Text widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_image_text_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_image_text_seo',
            __( 'Kumo - Image Text (SEO)', 'khayr' ),
            [
                'description' => __( 'Image Text (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Image Text Heading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'image'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image', 'khayr' )
                ],
                'text'   => [
                    'type'      => 'tinymce',
                    'label'     => __( 'Text', 'khayr' ),
                    'default'   => '',
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_image_text_seo',
                'src'             => '/inc/widgets/kbw_image_text_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_image_text_seo',
                'src'             => '/inc/widgets/kbw_image_text_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_image_text_seo', __FILE__, 'kbw_image_text_seo_widget' );