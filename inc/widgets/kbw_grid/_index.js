(function (Khayr) {
    var module_banner = function module_banner(element) {

        document.body.classList.add('banner-widget');

        let swiper = null,
            slider = element.querySelector('.module-grid__items'),
            pagination = slider.querySelector('.swiper-pagination'),
            wrap = element.querySelector('.module-grid__items-wrap'),
            slide = element.querySelectorAll('.module-grid__item');

        let swiperSlider = () => {
            if (window.innerWidth < 991) {
                if (slider && slider.dataset.grid == 'x3' && slide.length > 1 && !slider.classList.contains('swiper-initialized')) {
                    wrap.classList.remove('row');
                    slider.classList.add('swiper');
                    pagination.style.display = 'block';
                    wrap.classList.add('swiper-wrapper');
                    slide.forEach(element => {
                        element.classList.remove('col-lg-4');
                        element.classList.remove('col-md-6');
                        element.classList.remove('col-sm-12');
                        element.classList.add('swiper-slide');
                    });
                    swiper = new Swiper(slider, {
                        slidesPerView: 1,
                        loop: true,
                        autoHeight: true,
                        pagination: {
                            el: '.swiper-pagination'
                        }
                    });
                }
            } else {
                pagination.style.display = 'none';
                if (slider && slider.dataset.grid == 'x3' && slider.classList.contains('swiper-initialized')) {
                    swiper.destroy();
                    wrap.classList.add('row');
                    slider.classList.remove('swiper');
                    wrap.classList.remove('swiper-wrapper');
                    slide.forEach(element => {
                        element.classList.add('col-lg-4');
                        element.classList.add('col-md-6');
                        element.classList.add('col-sm-12');
                        element.classList.remove('swiper-slide');
                    });
                }
            }
        };

        swiperSlider();

        window.addEventListener('load', () => {
            swiperSlider();
        });

        window.addEventListener('resize', () => {
            swiperSlider();
        });

    };

    Khayr.exec.add('module-grid', function (elm) {
        return new module_banner(elm);
    }, 'grow');
})(Khayr);