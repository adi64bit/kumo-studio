<?php
/*
Widget Name: Kumo - Grid
Description: kumo grid widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_grid_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_grid',
            __( 'Kumo - Grid', 'khayr' ),
            [
                'description' => __( 'kumo grid widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'WHY WORK WITH US?'
                ],
                'grid'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Grid', 'khayr' ),
                    'item_name' => __( 'Grid item', 'khayr' ),
                    'fields'    => [
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'description'   => [
                            'type'      => 'textarea',
                            'label'     => __( 'Description', 'khayr' )
                        ],
                    ]
                ],
                'grid_size' => [
                    'type'      => 'select',
                    'label'     => __( 'Select Grid Size', 'khayr' ),
                    'default' => 'x3',
                    'options' => array(
                        'x1' => __( '1 Column', 'khayr' ),
                        'x2' => __( '2 Column', 'khayr' ),
                        'x3' => __( '3 Column', 'khayr' ),
                    )
                ],
                'align' => array(
                    'type' => 'select',
                    'label' => __( 'Grid Aligment', 'khayr' ),
                    'default' => 'center',
                    'options' => array(
                        'left' => __( 'Left', 'khayr' ),
                        'center' => __( 'Center', 'khayr' ),
                        'right' => __( 'Right', 'khayr' ),
                    )
                ),
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_grid',
                'src'             => '/inc/widgets/kbw_grid/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_grid',
                'src'             => '/inc/widgets/kbw_grid/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_grid', __FILE__, 'kbw_grid_widget' );