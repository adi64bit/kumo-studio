<?php
    $colClass = 'col-lg-4 col-md-6 col-sm-12';
    switch ($instance['grid_size']) {
        case 'x1':
            $colClass = 'col-lg-12';
            break;
        case 'x2':
            $colClass = 'col-lg-6 col-md-12';
            break;
    }
?>
<div class="module module-grid" data-khayr-grow="module-grid" >
    <div class="module-grid__wrapper">
        <?php 
            if ($instance['heading']) {
                echo '<h2 class="module-grid__heading" '.khayr_scroll().' data-text-stagger>';
                echo $instance['heading'];
                echo '</h2>';
            }
        ?>
        <?php 
            if ($instance['grid']) {
                ?>
                <div class="module-grid__items" data-grid="<?php echo $instance['grid_size'];?>">
                    <div class="module-grid__items-wrap row">
                    <?php 
                        $delay = 0;
                        foreach ($instance['grid'] as $key => $value) {
                            ?>
                                <div class="module-grid__item <?php echo $colClass;?> text-<?php echo $instance['align']; ?>" <?php echo khayr_scroll($delay.'s');?>>
                                    <?php 
                                        if($value['title']){
                                            echo '<h3 class="module-grid__item--title">'.$value['title'].'</h3>';
                                        }
                                        if($value['description']){
                                            echo '<p class="module-grid__item--description">'.$value['description'].'</p>';
                                        }
                                    ?>
                                </div>
                            <?php
                            $delay = $delay + 0.2;
                        }
                    ?>
                    </div>
                    <div class="swiper-pagination" style="display:none"></div> 
                </div>
                <?php
            }
        ?>
    </div>
</div>