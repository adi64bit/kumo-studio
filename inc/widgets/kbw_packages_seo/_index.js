(function (Khayr) {

    var module_packages_seo = function module_packages_seo(element) {

        let data = {
            root : element.querySelector('.module-package-seo__content'),
            wrapper : element.querySelector('.module-package-seo__content-wrapper'),
            slides : element.querySelectorAll('.module-package-seo__content-item'),
            swiper : null
        };

        let runOnMobile = (elm) => {
            if (window.innerWidth < 991) {
                if (data.root.classList.contains('swiper')) {
                    return;
                }
                data.root.classList.add('swiper');
                data.wrapper.classList.add('swiper-wrapper');
                data.slides.forEach((slide, index) => {
                    slide.classList.add('swiper-slide');
                });
                data.swiper = new Swiper(data.root, {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    loop: false,
                    mousewheel: {
                        invert: false,
                        releaseOnEdges: true
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    }
                });
            } else {
                if (data.root.classList.contains('swiper')) {
                    data.root.classList.remove('swiper');
                    data.wrapper.classList.remove('swiper-wrapper');
                    data.slides.forEach((slide, index) => {
                        slide.classList.remove('swiper-slide');
                    });
                    data.swiper.destroy();
                }
            }
        };

        runOnMobile();

        window.addEventListener('resize', () => {
            runOnMobile();
        });
    };

    Khayr.exec.add('module-package-seo', function (elm) {
        return new module_packages_seo(elm);
    }, 'grow');
})(Khayr);