<?php
/*
Widget Name: Kumo - Packages (SEO)
Description: Kumo Packages widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_package_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_package_seo',
            __( 'Kumo - Packages (SEO)', 'khayr' ),
            [
                'description' => __( 'Packages (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Packages Heading', 'khayr' ),
                    'default'   => 'Our Packages'
                ],
                'subheading'  => [
                    'type'      => 'text',
                    'label'     => __( 'Packages Subheading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'content'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Contents', 'khayr' ),
                    'item_name' => __( 'Content item', 'khayr' ),
                    'fields'    => [
                        'content_title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Content Title', 'khayr' ),
                            'default'   => 'Lorem Ipsum Dolor'
                        ],
                        'content_description'   => [
                            'type'      => 'tinymce',
                            'label'     => __( 'Content Description', 'khayr' ),
                            'default'   => 'Lorem Ipsum Dolor'
                        ],
                    ]
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_package_seo',
                'src'             => '/inc/widgets/kbw_packages_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_package_seo',
                'src'             => '/inc/widgets/kbw_packages_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_package_seo', __FILE__, 'kbw_package_seo_widget' );