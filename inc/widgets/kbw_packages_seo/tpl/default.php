<div class="module module-package-seo 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " data-khayr-grow="module-package-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-package-seo__wrapper">
            <?php 
                if ($instance['heading']) {
                    echo '<div class="module-package-seo__heading">';
                        echo '<h2 class="module-package-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    echo '</div>';
                }
                if ($instance['subheading']) {
                    echo '<div class="module-package-seo__subheading">';
                        echo '<h4 class="module-package-seo__subheading-text" '.khayr_scroll().'>';
                        echo $instance['subheading'];
                        echo '</h4>';
                    echo '</div>';
                }
                
            ?>
            <div class="module-package-seo__content">
                <div class="module-package-seo__content-wrapper">
                <?php 
                    if ($instance['content']) {
                        foreach ($instance['content'] as $key => $value) {
                            ?>
                                <div class="module-package-seo__content-item">
                                    <?php 
                                        if ($value['content_title']) {
                                            echo '<h3 class="module-package-seo__content-item-title" '.khayr_scroll().'>';
                                            echo $value['content_title'];
                                            echo '</h3>';
                                        }
                                        if ($value['content_description']) {
                                            echo '<div class="module-package-seo__content-item-description article-content" '.khayr_scroll().'>';
                                            echo $value['content_description'];
                                            echo '</div>';
                                        }
                                    ?>
                                </div>
                            <?php
                        }
                    }
                ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</div>