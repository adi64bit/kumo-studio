(function (Khayr) {
    var eventLoad = new Event('khayr-website-load');
    var module_banner_seo = function module_banner_seo(element) {

        document.body.classList.add('banner-seo-widget');

        let header = document.querySelector('header.header');

        if (header) {
            let setPaddingTop = () => {
                let height = header.clientHeight;
                element.style.paddingTop = height + 'px';
            };

            setPaddingTop();
            window.addEventListener('resize', setPaddingTop);
            // on load
            window.addEventListener('load', setPaddingTop);
        }

        let buttonAnchor = element.querySelector('.module-banner__anchor-trigger');
        if (buttonAnchor) {
            buttonAnchor.addEventListener('click', (e) => {
                e.preventDefault();
                let height = element.getBoundingClientRect().height;
                jQuery('html, body').animate({
                    scrollTop: height + 'px'
                });
                return false;
            });
        }
    };

    Khayr.exec.add('module-banner-seo', function (elm) {
        return new module_banner_seo(elm);
    }, 'grow');
})(Khayr);