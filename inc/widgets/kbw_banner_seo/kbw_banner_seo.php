<?php
/*
Widget Name: Kumo - Banner
Description: kumo banner widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_banner_seo_widget extends SiteOrigin_Widget {

    function __construct() {
        parent::__construct(
            'kbw_banner_seo',
            __( 'Kumo - Banner (SEO)', 'khayr' ),
            [
                'description' => __( 'Banner (SEO) Widget.', 'khayr' ),
                'panels_groups' => array('seo-theme-widget')
            ],
            [],
            [
                'image'      => [
                    'type'      => 'media',
                    'label'     => __( 'Banner Image', 'khayr' )
                ],
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Banner Heading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'subheading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Banner Sub Heading', 'khayr' ),
                    'default'   => 'Lorem Ipsum Dolor'
                ],
                'links'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Links', 'khayr' ),
                    'item_name' => __( 'Link item', 'khayr' ),
                    'fields'    => [
                        'text'   => [
                            'type'      => 'text',
                            'label'     => __( 'Text', 'khayr' ),
                            'default'   => ''
                        ],
                        'url'   => [
                            'type'      => 'link',
                            'label'     => __( 'URL', 'khayr' )
                        ]
                    ]
                ],
                // option image left text right
                'image_position'   => [
                    'type'      => 'select',
                    'label'     => __( 'Image Position', 'khayr' ),
                    'default'   => 'right',
                    'options'   => [
                        'left' => 'Left',
                        'right' => 'Right'
                    ]
                ],
                'background_color'   => [
                    'type'      => 'color',
                    'label'     => __( 'Background Color', 'khayr' ),
                    'default'   => '#002e3f'
                ],
                // text color white or black
                'text_color'   => [
                    'type'      => 'select',
                    'label'     => __( 'Text Color', 'khayr' ),
                    'default'   => 'white',
                    'options'   => [
                        'white' => 'White',
                        'black' => 'Black'
                    ]
                ],
                // show anchor arow
                'show_arrow'   => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Arrow', 'khayr' ),
                    'default'   => true
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_banner_seo',
                'src'             => '/inc/widgets/kbw_banner_seo/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_banner_seo',
                'src'             => '/inc/widgets/kbw_banner_seo/_style.css'
            ];
            return $css;
        });

    }
}

siteorigin_widget_register( 'kbw_banner_seo', __FILE__, 'kbw_banner_seo_widget' );