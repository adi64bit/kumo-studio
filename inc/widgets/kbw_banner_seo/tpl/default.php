<div class="module module-banner-seo 
    <?php echo isset($instance['show_arrow']) && $instance['show_arrow'] ? 'show-arrow' : ''; ?> 
    <?php echo isset($instance['image_position']) && $instance['image_position'] ? 'image-'.$instance['image_position'] : 'image-right'; ?> 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?>" data-khayr-grow="module-banner-seo" style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="container">
        <div class="module-banner-seo__wrapper">
            <div class="module-banner-seo__top">
                <?php 
                    if ($instance['heading'] && $instance['heading'] != '') {
                        echo '<h1 class="module-banner-seo__heading h2" data-text-stagger '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h1>';
                    }

                    if ($instance['subheading'] && $instance['subheading'] != '') {
                        echo '<p class="module-banner-seo__subheading h3" '.khayr_scroll().'>';
                        echo $instance['subheading'];
                        echo '</p>';
                    }
                ?>
                <div class="module-banner-seo__links">
                    <?php 
                        if ($instance['links']) {
                            foreach ($instance['links'] as $link) {
                                // item wrapper
                                echo '<div class="module-banner-seo__link-item">';
                                ?>
                                <div class="module module-button" data-khayr-grow="module-button">
                                    <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                                        <a href="<?php echo sow_esc_url($link['url']);?>" class="module-button__wrapper--link cursor-link"><span class="text"><?php echo $link['text'];?></span><span class="icon icon-panah"></span></a>
                                    </div>
                                </div>
                                <?php
                                // end item wrapper
                                echo '</div>';
                            }
                        }
                    ?>
                </div>
            </div>
            <!-- image -->
            <?php 
                if ($instance['image']) {
                    echo '<div class="module-banner-seo__image lax-elm-up-0">';
                    ?>
                    <div class="content-img__item">
                        <div class="content-img__item-imgwrap">
                            <?php echo khayr_get_image($instance['image'], 'full', 
                                    array(
                                        'alt'   => $instance['alt_text'] !== '' ? $instance['alt_text'] : false,
                                        'class' => array('content-img__item-img')
                                    )
                                ); ?>
                        </div>
                    </div>
                    <?php
                    echo '</div>';
                }
            ?>
        </div>

        <?php 
            if ($instance['show_arrow']) {
                ?>
                <div class="module-banner__anchor" style="opacity:1;">
                    <a href="#" class="module-banner__anchor-trigger cursor-link"><span class="icon icon-panah"></span></a>
                </div>
                <?php
            }
        ?>
    </div>
</div>