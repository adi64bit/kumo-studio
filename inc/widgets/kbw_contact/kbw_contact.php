<?php
/*
Widget Name: Kumo - Contact
Description: kumo contact widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_contact_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_contact',
            __( 'Kumo - Contact', 'khayr' ),
            [
                'description' => __( 'kumo contact widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'ONE STEP CLOSER'
                ],
                'subheading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Sub Heading', 'khayr' ),
                    'default'   => 'You\'re almost there'
                ],
                'contact'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Contact', 'khayr' ),
                    'item_name' => __( 'Contact item', 'khayr' ),
                    'fields'    => [
                        'icon' => [
                            'type' => 'select',
                            'label' => __( 'Icon', 'khayr' ),
                            'default' => 'none',
                            'options' => [
                                'none' => __( 'None', 'khayr' ),
                                'wa' => __( 'Whatsapp', 'khayr' ),
                                'mail' => __( 'Email', 'khayr' ),
                                'facebook' => __( 'Facebook', 'khayr' ),
                                'instagram' => __( 'Instagram', 'khayr' )
                            ]
                        ],
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'link'   => [
                            'type'      => 'link',
                            'label'     => __( 'Link', 'khayr' )
                        ],
                    ]
                ],
                // select font size normal & small
                'font_size' => [
                    'type'      => 'select',
                    'label'     => __( 'Font Size', 'khayr' ),
                    'default'   => 'normal',
                    'options'   => [
                        'normal' => __( 'Normal', 'khayr' ),
                        'small' => __( 'Small', 'khayr' )
                    ]
                ],
                // show or hide contact form
                'show_contact_form' => [
                    'type'      => 'checkbox',
                    'label'     => __( 'Show Contact Form', 'khayr' ),
                    'default'   => false
                ],
                'contact_form'  => [
                    'type'      => 'text',
                    'label'     => __( 'Contact Form (shortcode)', 'khayr' ),
                    'default'   => ''
                ],
                'contact_image'      => [
                    'type'      => 'media',
                    'label'     => __( 'Image Background', 'khayr' )
                ],
                'contact_map'      => [
                    'type' => 'code',
                    'label'     => __( 'Contact Map', 'khayr' ),
                    'description'   => 'if this field filled, the image will not used'
                ],
                // contact form position left or center
                'contact_form_position' => [
                    'type'      => 'select',
                    'label'     => __( 'Contact Form Position', 'khayr' ),
                    'default'   => 'left',
                    'options'   => [
                        'left' => __( 'Left', 'khayr' ),
                        'center' => __( 'Center', 'khayr' )
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_contact',
                'src'             => '/inc/widgets/kbw_contact/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_contact',
                'src'             => '/inc/widgets/kbw_contact/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_contact', __FILE__, 'kbw_contact_widget' );