<div class="module module-contact <?php echo isset($instance['font_size']) && $instance['font_size'] ? 'mc-font-'.$instance['font_size'] : 'mc-font-normal'; ?> " data-khayr-grow="module-contact">
    <div class="module-contact__wrapper">
        <div class="module-contact__top">
            <div class="module-contact__heading">
                <?php 
                    if ($instance['heading']) {
                        echo '<h2 class="" '.khayr_scroll().' data-text-stagger>';
                        echo $instance['heading'];
                        echo '</h2>';
                    }
                    if ($instance['subheading']) {
                        echo '<p class="" '.khayr_scroll().'>';
                        echo $instance['subheading'];
                        echo '</p>';
                    }
                ?>
            </div>
            <div class="module-contact__lists">
                <?php 
                    if ($instance['contact']) {
                        foreach ($instance['contact'] as $key => $value) {
                            if ($value['link'] && $value['title']) {
                                echo '<a href="'.sow_esc_url($value['link']).'" class="cursor-link">';
                                if ($value['icon'] && $value['icon'] !== 'none') {
                                    echo '<i class="icon icon-'.$value['icon'].'"></i>';
                                }
                                echo '<span class="title">'.$value['title'].'</span>';
                                echo '</a>';
                            }
                        }
                    }
                ?>
            </div>
        </div>
        <?php 
            if ($instance['show_contact_form']) {
                ?>
                <div class="module-contact__bottom <?php echo $instance['contact_form_position'] == 'center' ? 'form-center' : '';?> ">
                    <div class="module-contact__bottom-image <?php echo isset($instance['contact_map']) && $instance['contact_map'] !== '' ? 'map' : '';?>">
                        <?php 
                            if(isset($instance['contact_map']) && $instance['contact_map'] !== ''){
                                echo $instance['contact_map'];
                            } else {
                                ?>
                                <div class="content-img__item">
                                    <div class="content-img__item-imgwrap">
                                    <?php 
                                        if ($instance['contact_image']) {
                                            echo khayr_get_image($instance['contact_image'], 'full', array(
                                                'class' => array('content-img__item-img')
                                            ));
                                        }
                                    ?>
                                    </div>
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="module-contact__bottom-form">
                        <?php 
                            if ($instance['contact_form']) {
                                echo do_shortcode($instance['contact_form']);
                            }
                        ?>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>