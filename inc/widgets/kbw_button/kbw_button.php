<?php
/*
Widget Name: Kumo - Button
Description: kumo button widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_button_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_button',
            __( 'Kumo - Button', 'khayr' ),
            array(
                'description' => __( 'kumo button widget.', 'khayr' )
            ),
            array(
            ),
            array(
                'text' => array(
                    'type' => 'text',
                    'label' => __( 'Button Text', 'khayr' ),
                    'default' => 'Button',
                ),
                'link' => array(
                    'type' => 'link',
                    'label' => __( 'Button Link', 'khayr' ),
                    'default' => home_url(),
                ),
                'target' => array(
                    'type' => 'checkbox',
                    'label' => __( 'Open in new window? (target _blank)', 'khayr' ),
                    'default' => false,
                ),
                'align' => array(
                    'type' => 'select',
                    'label' => __( 'Button Aligment', 'khayr' ),
                    'default' => 'center',
                    'options' => array(
                        'left' => __( 'Left', 'khayr' ),
                        'center' => __( 'Center', 'khayr' ),
                        'right' => __( 'Right', 'khayr' ),
                    )
                ),
            ),
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_button',
                'src'             => '/inc/widgets/kbw_button/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_button',
                'src'             => '/inc/widgets/kbw_button/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_button', __FILE__, 'kbw_button_widget' );