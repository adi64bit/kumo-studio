<?php
    if ($instance['text'] !== '' && $instance['link']) {
        ?>
            <div class="module module-button" data-khayr-grow="module-button">
                <div class="module-button__wrapper <?php echo $instance['align']; ?>" <?php echo khayr_scroll();?>>
                    <a href="<?php echo sow_esc_url($instance['link']);?>" class="module-button__wrapper--link cursor-link"><span class="text"><?php echo $instance['text']; ?></span><span class="icon icon-panah"></span></a>
                </div>
            </div>
        <?php
    }
?>