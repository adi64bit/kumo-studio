<?php
/*
Widget Name: Kumo - Services V2
Description: kumo services v2 widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_services_v2_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_services_v2',
            __( 'Kumo - Services V2', 'khayr' ),
            [
                'description' => __( 'kumo services v2 widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'SERVICES'
                ],
                'sources' => [
                    'type' => 'select',
                    'label' => __( 'Select Sources', 'khayr' ),
                    'default' => 'services',
                    'options' => [
                        'services' => __( 'Service CPT', 'khayr' ),
                        'custom' => __( 'Custom', 'khayr' )
                    ],
                    'state_emitter' => array(
                        'callback' => 'select',
                        'args' => array( 'sources' )
                    ),
                ],
                'custom'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Custom Item', 'khayr' ),
                    'item_name' => __( 'item', 'khayr' ),
                    'state_handler' => array(
                        'sources[custom]' => array('show'),
                        'sources[services]' => array('hide'),
                    ),
                    'fields'    => [
                        'title'   => [
                            'type'      => 'text',
                            'label'     => __( 'Title', 'khayr' )
                        ],
                        'description'   => [
                            'type'      => 'textarea',
                            'label'     => __( 'Description', 'khayr' )
                        ],
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                        'link'   => [
                            'type'      => 'link',
                            'label'     => __( 'Link', 'khayr' )
                        ],
                    ]
                ],
                // column type 3 or 4
                'column' => [
                    'type' => 'select',
                    'label' => __( 'Column', 'khayr' ),
                    'default' => '4',
                    'options' => [
                        '3' => __( '3 Column', 'khayr' ),
                        '4' => __( '4 Column', 'khayr' )
                    ]
                ],
                // link type button or whole card
                'link_type' => [
                    'type' => 'select',
                    'label' => __( 'Link Type', 'khayr' ),
                    'default' => 'card',
                    'options' => [
                        'button' => __( 'Button', 'khayr' ),
                        'card' => __( 'Whole Card', 'khayr' )
                    ]
                ],
                // card alignment center or left 
                'card_alignment' => [
                    'type' => 'select',
                    'label' => __( 'Card Alignment', 'khayr' ),
                    'default' => 'center',
                    'options' => [
                        'center' => __( 'Center', 'khayr' ),
                        'left' => __( 'Left', 'khayr' )
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_services_v2',
                'src'             => '/inc/widgets/kbw_services_v2/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_services_v2',
                'src'             => '/inc/widgets/kbw_services_v2/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_services_v2', __FILE__, 'kbw_services_v2_widget' );