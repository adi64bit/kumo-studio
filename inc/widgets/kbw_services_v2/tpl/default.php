<div class="module module-services-v2 
    <?php 
        // column
        echo isset($instance['column']) && $instance['column'] ? 'column-'.$instance['column'].' ' : 'column-4 ';
        // link type
        echo isset($instance['link_type']) && $instance['link_type'] ? 'link-type-'.$instance['link_type'].' ' : 'link-type-card ';
        // card alignment
        echo isset($instance['card_alignment']) && $instance['card_alignment'] ? 'card-alignment-'.$instance['card_alignment'].' ' : 'card-alignment-center ';
    ?>" data-khayr-grow="module-services-v2">
    <div class="module-services-v2__wrapper">
        <div class="container">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="module-services-v2__heading" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
            ?>
        </div>
        <?php 
            $items = false;
            if ($instance['sources'] == 'services') {
                $argsWidget = array(
                    'post_type'     => 'service',
                    'post_status'   => 'publish',
                    'posts_per_page'=> -1
                );
                $query = new WP_Query($argsWidget);
                if($query->have_posts()){
                    $items = array();
                    foreach ($query->get_posts() as $key => $value) {
                        $items[] = array(
                            'label' => get_post_meta( $value->ID, 'label', true ),
                            'title' => get_the_title( $value->ID ),
                            'description'   => get_post_meta( $value->ID, 'hashtag', true ),
                            'image' => has_post_thumbnail($value->ID) ? khayr_get_image(get_post_thumbnail_id($value->ID), 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => '#'
                        );
                    }
                }
            } elseif ($instance['sources'] == 'custom') {
                if($instance['custom']){
                    $items = array();
                    foreach ($instance['custom'] as $key => $value) {
                        $items[] = array(
                            'label' => $value['label'],
                            'title' => $value['title'],
                            'description'   => $value['description'],
                            'image' => $value['image'] ? khayr_get_image($value['image'], 'full', array(
                                'class' => array('content-img__item-img')
                            )) : false,
                            'link'  => $value['link'] ? sow_esc_url($value['link']) : false
                        );
                    }
                }
            }

            if($items){
                ?>
                <div class="module-services-v2__row">
                    <?php 
                        foreach ($items as $key => $value) {
                            ?>
                                <div class="module-services-v2__col">
                                    <?php 
                                        if ($value['link'] && $instance['link_type'] == 'card') {
                                            ?>
                                            <a href="<?php echo $value['link'];?>" class="module-services-v2__col--link cursor-link">
                                            <?php
                                        }
                                    ?>
                                    <div class="module-services-v2__col--image">
                                        <?php 
                                            if ($value['image']) {
                                                ?>
                                                    <?php echo $value['image']; ?>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                    <?php 
                                        if($value['title']){
                                            echo '<h3 class="module-services-v2__col--title">'.$value['title'].'</h3>';
                                        }
                                        if($value['description']){
                                            echo '<p class="module-services-v2__col--description">'.$value['description'].'</p>';
                                        }
                                        if ($value['link'] && $instance['link_type'] == 'card') {
                                            ?>
                                            </a>
                                            <?php
                                        }
                                        if ($value['link'] && $instance['link_type'] == 'button') {
                                            ?>
                                            <div class="module module-button" data-khayr-grow="module-button">
                                                <div class="module-button__wrapper <?php echo isset($instance['card_alignment']) && $instance['card_alignment'] ? $instance['card_alignment'] : 'center'; ?>" <?php echo khayr_scroll();?>>
                                                    <a href="<?php echo $value['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text">Read More</span><span class="icon icon-panah"></span></a>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            <?php
                        }
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>