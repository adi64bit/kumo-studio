(function (Khayr) {

    var module_partners = function module_partners(element) {

        this.swiper = this.swiper || [];
        let slider = element.querySelector('.module-partners__slides'),
            length = this.swiper.length;
        if (slider) {
            let index = length++;
            this.swiper[index] = new Swiper(slider, {
                slidesPerView: 6,
                loop: false,
                mousewheel: {
                    invert: false,
                    releaseOnEdges: true
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true
                },
                breakpoints: {
                    320: {
                        slidesPerView: 3,
                    },
                    500: {
                        slidesPerView: 3,
                    },
                    1280: {
                        slidesPerView: 6,
                    },
                }
            });
        }
    };

    Khayr.exec.add('module-partners', function (elm) {
        return new module_partners(elm);
    }, 'grow');
})(Khayr);