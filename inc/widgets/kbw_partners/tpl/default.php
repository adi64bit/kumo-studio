<div class="module module-partners" data-khayr-grow="module-partners">
    <div class="module-partners__wrapper">
        <div class="container">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="module-partners__heading" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
            ?>
        </div>
        <?php 
            $items = array();
            foreach ($instance['custom'] as $key => $value) {
                $items[] = array(
                    'image' => $value['image'] ? khayr_get_image($value['image'], 'full', array(
                        'class' => array('content-img__item-img')
                    )) : false,
                    'link'  => $value['link'] ? sow_esc_url($value['link']) : false
                );
            }

            if($items){
                ?>
                <div class="module-partners__slides-wrapper">
                    <div class="module-partners__slides swiper">
                        <div class="swiper-wrapper">
                            <?php 
                                foreach ($items as $key => $value) {
                                    ?>
                                        <div class="swiper-slide module-partners__slide">
                                            <div class="module-partners__slide--wrap">
                                                <?php 
                                                    if ($value['link']) {
                                                        ?>
                                                            <a href="<?php echo $value['link'];?>" class="module-partners__slide--link">
                                                        <?php
                                                    }
                                                ?>
                                                <div class="module-partners__slide--image">
                                                    <?php 
                                                        if ($value['image']) {
                                                            ?>
                                                                <?php echo $value['image']; ?>
                                                            <?php
                                                        }
                                                    ?>
                                                </div>
                                                <?php 
                                                    if ($value['link']) {
                                                        ?>
                                                            </a>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                <?php
            }
        ?>
    </div>
</div>