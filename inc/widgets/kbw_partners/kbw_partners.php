<?php
/*
Widget Name: Kumo - Partners
Description: kumo partners widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_partners_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_partners',
            __( 'Kumo - Partners', 'khayr' ),
            [
                'description' => __( 'kumo partners widget.', 'khayr' ),
                'panels_groups' => array('theme-widget')
            ],
            [],
            [
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Partners'
                ],
                'custom'  => [
                    'type'      => 'repeater',
                    'label'     => __( 'Custom Item', 'khayr' ),
                    'item_name' => __( 'item', 'khayr' ),
                    'fields'    => [
                        'image'      => [
                            'type'      => 'media',
                            'label'     => __( 'Image', 'khayr' )
                        ],
                        'link'   => [
                            'type'      => 'link',
                            'label'     => __( 'Link', 'khayr' )
                        ],
                    ]
                ],
            ],
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_partners',
                'src'             => '/inc/widgets/kbw_partners/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_partners',
                'src'             => '/inc/widgets/kbw_partners/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_partners', __FILE__, 'kbw_partners_widget' );