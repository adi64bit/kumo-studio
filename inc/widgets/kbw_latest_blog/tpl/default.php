<div class="module module-latest-blog" data-khayr-grow="module-latest-blog">
    <div class="module-latest-blog__wrapper">
        <div class="module-latest-blog__heading">
            <?php 
                if ($instance['heading']) {
                    echo '<h2 class="" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                }
                if ($instance['text'] && $instance['link']) {
                    echo '<a href="'.sow_esc_url($instance['link']).'" class="cursor-link">'.$instance['text'].'</a>';
                }
            ?>
        </div>
        <?php 
            $items = false;
            $argsWidget = array(
                'post_type'     => 'post',
                'post_status'   => 'publish',
                'posts_per_page'=> $instance['ppp'] ? $instance['ppp'] : 2
            );

            $query = new WP_Query($argsWidget);
            if($query->have_posts()){
                $items = array();
                foreach ($query->get_posts() as $key => $value) {
                    $categories = wp_get_post_categories( $value->ID, array( 'fields' => 'all' ) );
                    $content = $value->post_excerpt;
                    if ($content && $content != '') {
                        # code...
                    } else {
                        ob_start();
                        if ( class_exists( 'SiteOrigin_Panels' ) && get_post_meta( $value->ID, 'panels_data', true ) ) {
                            //echo SiteOrigin_Panels::renderer()->render( $value->ID );
                        } else {
                            // $post_content = wpautop(get_post( $value->ID )->post_content);
                            // $modified_content = apply_filters( 'the_content', $post_content );
                            // echo $modified_content;
                        }
                        $content = ob_get_clean();
                        $content = wp_trim_words($content, 25, '...');
                    }
                    $items[] = array(
                        'category' => $categories,
                        'title' => get_the_title( $value->ID ),
                        'description'   => $content,
                        'date'  => get_the_date('d M Y', $value->ID),
                        'link'  => get_the_permalink($value->ID)
                    );
                }
            }

            if($items){
                ?>
                <div class="module-latest-blog__items">
                    <?php 
                        foreach ($items as $key => $value) {
                            ?>
                                <div class="module-latest-blog__item" <?php echo khayr_scroll(); ?>>
                                    <div class="module-latest-blog__item--date">
                                        <?php 
                                            if($value['date']){
                                                echo '<span>'.$value['date'].'</span>';
                                            }
                                        ?>
                                    </div>
                                    <div class="module-latest-blog__item--content">
                                        <?php 
                                            if ($value['category']) {
                                                echo '<ul class="module-latest-blog__item-category">';
                                                foreach ($value['category'] as $k => $v) {
                                                    echo '<li><a href="'.get_term_link($v->term_id).'">'.$v->name.'</a></li>';
                                                }
                                                echo '</ul>';
                                            }
                                            if($value['title']){
                                                echo '<h3 class="module-latest-blog__item--title">'.$value['title'].'</h3>';
                                            }
                                            if($value['description']){
                                                echo '<p class="module-latest-blog__item--description">'.$value['description'].'</p>';
                                            }
                                            if ($value['link']) {
                                                ?>
                                                    <div class="module module-button" data-khayr-grow="module-button">
                                                        <div class="module-button__wrapper" <?php echo khayr_scroll();?>>
                                                            <a href="<?php echo $value['link'];?>" class="module-button__wrapper--link cursor-link"><span class="text">Read Blog</span><span class="icon icon-panah"></span></a>
                                                        </div>
                                                    </div>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            <?php
                        }
                    ?>
                            
                </div>
                <?php
            }
        ?>
    </div>
</div>