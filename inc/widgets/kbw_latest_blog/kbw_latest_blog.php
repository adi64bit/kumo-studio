<?php
/*
Widget Name: Kumo - Latest Blog
Description: kumo latest blog widget.
Author: Adi64bit
Author URI: adi64bit@gmail.com
Widget URI: adi64bit@gmail.com
*/

class kbw_latest_blog_widget extends SiteOrigin_Widget {

    function __construct() {

        parent::__construct(
            'kbw_latest_blog',
            __( 'Kumo - Latest Blog', 'khayr' ),
            array(
                'description' => __( 'kumo latest blog widget.', 'khayr' )
            ),
            array(
            ),
            array(
                'heading'   => [
                    'type'      => 'text',
                    'label'     => __( 'Text Heading', 'khayr' ),
                    'default'   => 'Latest Blogs'
                ],
                'text'   => [
                    'type'      => 'text',
                    'label'     => __( 'Button Text', 'khayr' ),
                    'default'   => 'See all blog'
                ],
                'link'   => [
                    'type'      => 'link',
                    'label'     => __( 'Button link', 'khayr' )
                ],
                'ppp'   => [
                    'type'      => 'text',
                    'label'     => __( 'Number of Posts', 'khayr' ),
                    'default'   => 2
                ],
            ),
            plugin_dir_path( __FILE__ )
        );
    }

    function initialize() {
        add_filter( 'khayr_script_loader', function($js){
            $js[] = [
                'handle'          => 'kbw_latest_blog',
                'src'             => '/inc/widgets/kbw_latest_blog/_index.js',
                'footer'          => true
            ];
            return $js;
        });

        add_filter( 'khayr_style_loader', function($css){
            $css[] = [
                'handle'          => 'kbw_latest_blog',
                'src'             => '/inc/widgets/kbw_latest_blog/_style.css'
            ];
            return $css;
        });
    }
}
siteorigin_widget_register( 'kbw_latest_blog', __FILE__, 'kbw_latest_blog_widget' );