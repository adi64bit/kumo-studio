<div class="module module-text-grid-seo 
    <?php echo isset($instance['text_color']) && $instance['text_color'] ? 'text-color-'.$instance['text_color'] : 'text-color-normal'; ?> " data-khayr-grow="module-text-grid-seo" 
    style="background-color:<?php echo isset($instance['background_color']) && $instance['background_color'] ? $instance['background_color'] : ''; ?>;">
    <div class="module-text-grid-seo__wrapper">
        <?php 
            if ($instance['heading']) {
                echo '<div class="module-text-grid-seo__heading">';
                    echo '<h2 class="module-text-grid-seo__heading-text" '.khayr_scroll().' data-text-stagger>';
                    echo $instance['heading'];
                    echo '</h2>';
                echo '</div>';
            }
            
        ?>
        <div class="module-text-grid-seo__content">
            <?php 
                if ($instance['content']) {
                    foreach ($instance['content'] as $key => $value) {
                        ?>
                            <div class="module-text-grid-seo__content-item">
                                <?php 
                                    if ($value['content_title']) {
                                        echo '<h4 class="module-text-grid-seo__content-item-title" '.khayr_scroll().'>';
                                        echo $value['content_title'];
                                        echo '</h4>';
                                    }
                                    if ($value['content_description']) {
                                        echo '<p class="module-text-grid-seo__content-item-description" '.khayr_scroll().'>';
                                        echo $value['content_description'];
                                        echo '</p>';
                                    }
                                ?>
                            </div>
                        <?php
                    }
                }
            ?>
        </div>
    </div>
</div>