<?php
if (function_exists('khayr_register_cpt')) {
    khayr_register_cpt(array(
        'postType' => 'testimonial',
        'postName' => 'Testimonial',
        'singularName' => 'Testimonial',
        'pluralName' => 'Testimonials',
        'active'    => true,
        'supports' => array(
            'title',
            'thumbnail',
            'revisions'
        )
    ));
}
