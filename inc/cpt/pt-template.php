<?php
if (function_exists('khayr_register_cpt')) {
    khayr_register_cpt(array(
        'postType' => 'template',
        'postName' => 'Template',
        'singularName' => 'Template',
        'pluralName' => 'Templates',
        'active'    => true,
        'supports' => array(
            'title',
            'content',
            'revisions'
        )
    ));
}
