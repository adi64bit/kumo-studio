<?php
if (function_exists('khayr_register_cpt')) {
    khayr_register_cpt(array(
        'postType' => 'service',
        'postName' => 'Service',
        'singularName' => 'Service',
        'pluralName' => 'Services',
        'active'    => true,
        'supports' => array(
            'title',
            'thumbnail',
            'revisions'
        )
    ));
}
