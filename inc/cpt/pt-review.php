<?php
if (function_exists('khayr_register_cpt')) {
    khayr_register_cpt(array(
        'postType' => 'review',
        'postName' => 'Review',
        'singularName' => 'Review',
        'pluralName' => 'Reviews',
        'active'    => true,
        'supports' => array(
            'title',
            'revisions'
        )
    ));
}

// register taxonomy using wordpress function
function khayr_register_taxonomy() {
    register_taxonomy('review_category', 'review', array(
        'label' => 'Review Category',
        'hierarchical' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'review-category'),
    ));
}
add_action('init', 'khayr_register_taxonomy', 11);
