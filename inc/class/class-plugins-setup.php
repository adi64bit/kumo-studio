<?php
if (!class_exists('KhayrBase_RegisterPlugins')) {
    class KhayrBase_RegisterPlugins{
    
        public function __construct(){
            require_once KHAYR_BASE_LIBS . 'class-tgm-plugin-activation.php';
    
            add_action( 'tgmpa_register', [$this, 'khayr_register_required_plugins']);
            add_filter( 'post_class',[$this,'themes_remove_hentry'] );
        }

        public function themes_remove_hentry( $classes ) {
            if ( is_page() ) {
                $classes = array_diff( $classes, array( 'hentry' ) );
            }
            return $classes;
        }
    
        public function khayr_register_required_plugins(){
            /*
            * Array of plugin arrays. Required keys are name and slug.
            * If the source is NOT from the .org repo, then source is also required.
            */
            $plugins = array(
    
                array(
                    'name'     				=> 'Elementor Page Builder', // The plugin name
                    'slug'     				=> 'elementor', // The plugin slug (typically the folder name)
                    'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
                    'force_activation' 		=> false
                ),
    
                array(
                    'name'     				=> 'Redux Framework', // The plugin name
                    'slug'     				=> 'redux-framework', // The plugin slug (typically the folder name)
                    'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
                ),
                // One Click Demo Import.
                array(
                    'name'      => 'One Click Demo Import',
                    'slug'      => 'one-click-demo-import', // The plugin slug (typically the folder name).
                    'required'  => false, // If false, the plugin is only 'recommended' instead of required.
                ),
                
                array(
                    'name'     				=> 'MailChimp For WP', // The plugin name
                    'slug'     				=> 'mailchimp-for-wp', // The plugin slug (typically the folder name)
                    'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
                )
            );
    
            /*
            * Array of configuration settings. Amend each line as needed.
            *
            * TGMPA will start providing localized text strings soon. If you already have translations of our standard
            * strings available, please help us make TGMPA even better by giving us access to these translations or by
            * sending in a pull-request with .po file(s) with the translations.
            *
            * Only uncomment the strings in the config array if you want to customize the strings.
            */
            $config = array(
                'id'           => 'kumo',                 // Unique ID for hashing notices for multiple instances of TGMPA.
                'default_path' => '',                      // Default absolute path to bundled plugins.
                'menu'         => 'tgmpa-install-plugins', // Menu slug.
                'has_notices'  => true,                    // Show admin notices or not.
                'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => false,                   // Automatically activate plugins after installation or not.
                'message'      => '',                      // Message to output right before the plugins table.
            );
    
            tgmpa( $plugins, $config );
        }
    }
}

new KhayrBase_RegisterPlugins();