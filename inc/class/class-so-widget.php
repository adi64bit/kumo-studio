<?php 
class KhayrBase_SoWidgets
{
 

    public function __construct(){
            add_filter('siteorigin_widgets_widget_folders', [$this, 'so_widget_dir']);
            add_filter('siteorigin_panels_widget_dialog_tabs', [$this, 'so_widget_tab']);
            add_filter('siteorigin_widgets_active_widgets', [$this, 'so_widget_active']);
    }

    public function so_widget_dir( $folders ){
        $folders[] = KHAYR_BASE_WIDGET;
        return $folders;
    }

    public function so_widget_tab( $tabs ){
        $tabs[] = array(
            'title'     => 'Kumo Widget',
            'filter'    => array(
                'groups'    => array('theme-widget')
            )
        );
        $tabs[] = array(
            'title'     => 'SEO Kumo Widget',
            'filter'    => array(
                'groups'    => array('seo-theme-widget')
            )
        );
        return $tabs;
    }

    public function so_widget_active( $widgets ){
        $widget_folder = KHAYR_BASE_WIDGET;
        $dir = new \DirectoryIterator($widget_folder);
        foreach ($dir as $dirinfo) {
            if (!$dirinfo->isDot() && $dirinfo->isDir()) {
                $filename = $dirinfo->getFilename();
                $filename = str_replace('.php', '', $filename);
                $widgets[$filename] = true;
            }
        }
        return $widgets;
    }

}

new KhayrBase_SoWidgets();