<?php
if (!class_exists('KhayrBase_Multisite')) {
    class KhayrBase_Multisite{
    
        public function __construct(){
            $this->meta_setup();
            add_action( 'wp_head', [$this, 'add_canonical_link'] );
            add_shortcode( 'khayr_multisite', [$this, 'shortcode'] );
        }
        
        public function meta_setup(){
            if (function_exists('khayr_register_metabox')) {
                khayr_register_metabox(
                    'khayr_multisite',
                    array(
                        'title'         => esc_html__( 'Multisite Options', 'kumo' ),
                        'object_types'  => array('post', 'page'),
                        'show_on_cb'    => 'khayr_show_if_front_page',
                        'context'       => 'normal',
                        'priority'      => 'high',
                    ),
                    array(
                        array(
                            'name'             => __( 'Select Canonical', 'cmb2' ),
                            'desc'             => __( 'Select post', 'cmb2' ),
                            'id'               => 'select_canonical',
                            'type'             => 'select',
                            'show_option_none' => true, // Show a "Select an option" default option
                            'options'          => $this->select_post_option(),
                        ),
                    )
                );
            }
        }

        public function select_post_option(){
            $return = [];
            
            if ( is_multisite() ) {
                $site_id = get_current_blog_id();
                $alternate_site_id = $site_id == 1 ? 2 : 1;
                switch_to_blog( $alternate_site_id );
                $posts = get_posts( array(
                    'post_type' => ['post', 'page'],
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                ) );
                foreach ( $posts as $post ) {
                    $return[get_permalink($post->ID)] = get_the_title($post->ID).' ('.get_post_type($post->ID).')';
                }
                restore_current_blog();
            }
            return $return;
        }

        public function add_canonical_link() {
            if ( is_page() || is_single() || is_singular() ) {
                global $post;
                if ($post) {
                    $canonical_url = get_permalink( $post->ID );
                    $alternate_url = get_post_meta( $post->ID, 'select_canonical', true );
                    if ( ! empty( $alternate_url ) ) {
                        $canonical_url = $alternate_url;
                    }
                    echo '<link rel="canonical" href="' . esc_url( $canonical_url ) . '" />' . "\n";
                }
            }
        }

        public function get_canonical_link($post = false) {
            $return = false;
            if (!$post) {
                if ( is_page() || is_single() || is_singular() ) {
                    global $post;
                }
            }

            if ($post) {
                $alternate_url = get_post_meta( $post->ID, 'select_canonical', true );
                if ( ! empty( $alternate_url ) ) {
                    $return = $alternate_url;
                }
            }

            return $return;
        }

        public function shortcode(){
            $lang = [
                1 => [
                    'name' => 'Indonesia',
                    'code' => 'ind',
                    'url'  => '/id/',
                    'blog_id'   => 2,
                ],
                2 => [
                    'name' => 'English',
                    'code' => 'eng',
                    'url'  => '/',
                    'blog_id'   => 1,
                ],
            ];
            $link = [];
            foreach ($lang as $key => $value) {
                $site_url = $value['url'];
                if ( get_current_blog_id() == $value['blog_id'] ) {
                    $class = 'active';
                } else {
                    $class = '';

                    $canonical_url = $this->get_canonical_link();
                    if ($canonical_url) {
                        $site_url = $canonical_url;
                    }
                }

                $links[] = sprintf( '<a href="%s" class="%s">%s</a>', $site_url, $class, $value['code'] );
            }
            ob_start();
            ?>
            <div class="khayr__language-swithcer">
                <ul>
                    <?php foreach ( $links as $link ) : ?>
                        <li><?php echo $link; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php
            return ob_get_clean();
        }
    }
}

new KhayrBase_Multisite();