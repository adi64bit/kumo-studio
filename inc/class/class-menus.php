<?php 
class KhayrBase_Menus
{
    public static $menus = array(
        'main_menu' => 'The Main Menu',
        'footer_menu' => 'The Footer Menu'
    );

    public function __construct(){
        $this->register();
    }

    public static function register($action = 'init', $priority = 10)
    {
        // Register the action
        add_action($action, function () {
            register_nav_menus(static::$menus);
        }, $priority);
    }
}

new KhayrBase_Menus();