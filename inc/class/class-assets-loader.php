<?php
class KhayrBase_AssetsLoader{
    public static $css = [];

    public static $js = [];

    public static $addCss = [];

    public static $addJs = [];

    public static $adminCSS = [];

    public static $adminJs = [];

    public static $addAdminCSS = [];

    public static $addAdminJs = [];

    public static $async = [];

    /**
     * En-queue required assets
     *
     * @param  string  $filter   The name of the filter to hook into
     * @param  integer $priority The priority to attach the filter with
     */
    public static function load()
    {
        // vendor
        static::add_css([
            'handle'          => 'bootstrap-reboot',
            'src'             => '/assets/libs/bootstrap/css/bootstrap-reboot.min.css',
        ]);

        static::add_css([
            'handle'          => 'bootstrap-grid',
            'src'             => '/assets/libs/bootstrap/css/bootstrap-grid.min.css',
        ]);

        static::add_js([
            'handle'          => 'anime',
            'src'             => '/assets/libs/anime/anime.min.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_css([
            'handle'          => 'swiper',
            'src'             => '/assets/libs/swiper/swiper-bundle.min.css',
        ]);

        static::add_js([
            'handle'          => 'swiper',
            'src'             => '/assets/libs/swiper/swiper-bundle.min.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_css([
            'handle'          => 'sal',
            'src'             => '/assets/libs/sal/sal.css',
        ]);

        static::add_js([
            'handle'          => 'sal',
            'src'             => '/assets/libs/sal/sal.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_js([
            'handle'          => 'lax',
            'src'             => '/assets/libs/lax/lax.min.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_css([
            'handle'          => 'splitting',
            'src'             => '/assets/libs/splitting/splitting.css',
        ]);
        static::add_css([
            'handle'          => 'splitting-cells',
            'src'             => '/assets/libs/splitting/splitting-cells.css',
        ]);

        static::add_js([
            'handle'          => 'splitting',
            'src'             => '/assets/libs/splitting/splitting.min.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_js([
            'handle'          => 'lazysizes',
            'src'             => '/assets/libs/lazysizes/lazysizes.min.js',
            'footer'          => true,
            'deps'            => []
        ]);

        // Font Load
        static::add_css([
            'handle'          => 'khayr_futura',
            'src'             => '/assets/font/Futura/stylesheet.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_opensans',
            'src'             => '/assets/font/OpenSans/stylesheet.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_icon',
            'src'             => '/assets/icon/style.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_base',
            'src'             => '/assets/css/base.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_content',
            'src'             => '/assets/css/content.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_form',
            'src'             => '/assets/css/form.css',
        ]);

        static::add_css([
            'handle'          => 'khayr_cursor',
            'src'             => '/assets/css/cursor.css',
        ]);

        static::add_js([
            'handle'          => 'khayr_global_script',
            'src'             => '/assets/js/global.js',
            'footer'          => true,
            'deps'            => [],
            'data'            => [
                'name'        => 'Khayr',
                'config'      => [
                    'ajax_url'      => admin_url( 'admin-ajax.php' ),
                    'childThemeDir' => get_stylesheet_directory_uri(),
                    'grow'          => array(),
                    'diet'          => array(),
                    'evt'           => array()
                ]
            ]
        ]);

        static::add_js([
            'handle'          => 'constants',
            'src'             => '/assets/js/constants.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_js([
            'handle'          => 'image-anim',
            'src'             => '/assets/js/image-anim.js',
            'footer'          => true,
            'deps'            => []
        ]);

        static::add_js([
            'handle'          => 'form',
            'src'             => '/assets/js/form.js',
            'footer'          => true,
            'deps'            => ['khayr_global_script', 'choices']
        ]);

        static::add_js([
            'handle'          => 'animation',
            'src'             => '/assets/js/animation.js',
            'footer'          => true,
            'deps'            => ['khayr_global_script', 'lax']
        ]);
        
        static::add_js([
            'handle'          => 'cursor',
            'src'             => '/assets/js/cursor.js',
            'footer'          => true,
            'deps'            => ['khayr_global_script', 'jquery']
        ]);

        //add choice js cdn
        static::add_js([
            'handle'          => 'choices',
            'src'             => 'https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js',
            'footer'          => true,
            'deps'            => []
        ]);
        // add choice css cdn
        static::add_css([
            'handle'          => 'choices',
            'src'             => 'https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css',
        ]);

        static::component_assets();

        add_action('wp_enqueue_scripts', __CLASS__.'::register_files', 12);
        add_action('admin_enqueue_scripts', __CLASS__.'::admin_register_files', 12);
    }

    public static function component_assets(){
        $component = KHAYR_BASE_COMPONENT;
        $dir = new \DirectoryIterator($component);
        foreach ($dir as $dirinfo) {
            if (!$dirinfo->isDot() && $dirinfo->isDir()) {
                $filename = $dirinfo->getFilename();
                $filename = str_replace('.php', '', $filename);
                static::add_css([
                    'handle'          => 'component-'.$filename,
                    'src'             => '/component/'.$filename.'/_style.css',
                ]);
                static::add_js([
                    'handle'          => 'component-'.$filename,
                    'src'             => '/component/'.$filename.'/_index.js',
                    'footer'          => true,
                    'deps'            => ['khayr_global_script']
                ]);
            }
        }
    }

    /**
     * Register CSS
     *
     * @param [array] $config
     * @return void
     */
    public static function add_css( $config, $type = '' )
    {
        $defaults = [
            'handle'    => null,
            'src'       => null,
            'plugin'    => false,
            'deps'      => [],
            'ver'       => false,
            'media'     => 'all'
        ];

        $config = array_merge( $defaults, $config );

        if ( !khayr_is_external_content($config['src']) && !$config['plugin'] ) {
            // Set location
            $config['src'] = get_stylesheet_directory_uri() . $config['src'];
        }

        // If version hasn't been passed in and the src isn't external(google fonts)
        if ( !khayr_is_external_content($config['src']) && !$config['plugin'] && !$config['ver'] ) {
            $config['ver'] = filemtime( get_stylesheet_directory() . "/{$defaults['src']}" );
        }

        if ($type == 'admin') {
            static::$adminCSS[] = $config;
        } else {
            static::$css[] = $config;
        }
    }

    /**
     * Register JS
     *
     * @param [arrat] $config
     * @return void
     */
    public static function add_js( $config , $type = '')
    {
        $defaults = [
            'handle'    => null,
            'src'       => null,
            'deps'      => [],
            'plugin'    => false,
            'ver'       => false,
            'footer'    => true,
            'data'      => false,
            'async'     => false
        ];

        $config = array_merge( $defaults, $config );

        // Store async handles for later on
        if ( $config['async'] ) {
            static::$async[] = $config['handle'];
        }

        if ( !khayr_is_external_content($config['src']) && !$config['plugin']) {
            // Set location
            $config['src'] = get_stylesheet_directory_uri() . $config['src'];
        }

        // If version hasn't been passed in and the src isn't external(google fonts)
        if ( !$config['ver'] && !khayr_is_external_content($defaults['src']) && !$config['plugin']) {
            $config['ver'] = filemtime( get_stylesheet_directory() . "/{$defaults['src']}" );
        }

        if ($type == 'admin') {
            static::$adminJs[] = $config;
        } else {
            static::$js[] = $config;
        }
    }

    public static function register_files()
    {
        static::$addJs = apply_filters( 'khayr_script_loader', array());
        static::$addCss = apply_filters( 'khayr_style_loader', array());

        if (!empty(static::$addJs) && is_array(static::$addJs)) {
            foreach (static::$addJs as $key => $value) {
                static::add_js($value);
            }
        }

        if (!empty(static::$addCss) && is_array(static::$addCss)) {
            foreach (static::$addCss as $key => $value) {
                static::add_css($value);
            }
        }

        // CSS
        if ( static::$css !== [] ) {
            foreach ( static::$css as $css ) {
                wp_enqueue_style( $css['handle'] , $css['src'], $css['deps'], $css['ver'], $css['media']);
            }
        }

        // JS
        if ( static::$js !== array() ) {
            foreach ( static::$js as $js ) {
                if ( isset($js['test']) && call_user_func($js['test']) === false ) {
                    continue;
                }

                wp_enqueue_script( $js['handle'] , $js['src'], $js['deps'], $js['ver'], $js['footer']);

                if ( $js['data'] ) {
                    wp_localize_script($js['handle'], $js['data']['name'], $js['data']['config']);

                    wp_enqueue_script($js['handle']);
                }
            }
        }
    }

    public static function admin_register_files()
    {
        static::$addAdminJs = apply_filters( 'khayr_admin_script_loader', array());
        static::$addAdminCSS = apply_filters( 'khayr_admin_style_loader', array());

        if (!empty(static::$addAdminJs) && is_array(static::$addAdminJs)) {
            foreach (static::$addAdminJs as $key => $value) {
                static::add_js($value, 'admin');
            }
        }

        if (!empty(static::$addAdminCSS) && is_array(static::$addAdminCSS)) {
            foreach (static::$addAdminCSS as $key => $value) {
                static::add_css($value, 'admin');
            }
        }

        // CSS
        if ( static::$adminCSS !== [] ) {
            foreach ( static::$adminCSS as $css ) {
                wp_enqueue_style( $css['handle'] , $css['src'], $css['deps'], $css['ver'], $css['media']);
            }
        }

        // JS
        if ( static::$adminJs !== array() ) {
            foreach ( static::$adminJs as $js ) {
                if ( isset($js['test']) && call_user_func($js['test']) === false ) {
                    continue;
                }

                wp_enqueue_script( $js['handle'] , $js['src'], $js['deps'], $js['ver'], $js['footer']);

                if ( $js['data'] ) {
                    wp_localize_script($js['handle'], $js['data']['name'], $js['data']['config']);

                    wp_enqueue_script($js['handle']);
                }
            }
        }
    }
}

KhayrBase_AssetsLoader::load();